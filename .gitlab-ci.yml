# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Flutter.gitlab-ci.yml

### CONFIGURATION ###

# First runs tests and if successful builds the app
# https://docs.gitlab.com/ee/ci/yaml/#stages
stages:
  - test
  - build

# Sets environment variables
variables:
  # Sets the cache location for Dart packages
  # https://dart.dev/tools/pub/environment-variables
  # Stolen from https://stackoverflow.com/a/66254487/4106848
  PUB_CACHE: $CI_PROJECT_DIR\.pub-cache

# Runs all checks as a merge request pipeline if a merge request is open on a specified branch
# https://docs.gitlab.com/ee/ci/yaml/workflow.html#switch-between-branch-pipelines-and-merge-request-pipelines
workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

### JOBS ###

# Checks if all files are properly formatted
code_format:
  stage: test
  image: cirrusci/flutter:latest
  script:
    - dart format -o show --set-exit-if-changed .

# Generates a code quality report
code_quality:
  stage: test
  image: cirrusci/flutter:latest
  before_script:
    - set "PATH=%PATH%;%CI_PROJECT_DIR%\.pub-cache\bin"
    - flutter pub global activate dart_code_metrics 4.20.0
  script:
    - flutter pub global run dart_code_metrics:metrics analyze lib -r gitlab > gl-code-quality-report.json --verbose
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
  cache:
    key: code_quality_cache
    paths:
      - .pub-cache

# Analyzes the Dart code
flutter_analyze:
  stage: test
  image: cirrusci/flutter:latest
  script:
    - flutter analyze --no-fatal-infos --no-fatal-warnings
  cache:
    key: '${CI_COMMIT_REF_SLUG}_analyze_cache'
    paths:
      - .pub-cache

# Builds the Android app if it's an merge request on the master branch
flutter_build:
  stage: build
  image: cirrusci/flutter:latest
  script:
    - flutter build apk
  artifacts:
    paths:
      - build/app/outputs/flutter-apk/app-release.apk
    expire_in: 1 week
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master"'
