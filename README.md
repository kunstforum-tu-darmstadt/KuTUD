# Kunstforum Frontend (App)

This repository contains the mobile application for the "Kunstforum" of the TU Darmstadt. It is also referred to as frontend.
The application is built using the [Flutter](https://flutter.dev/) framework. Hence, it can be used for Android and iOS devices.
It connects to the corresponding [backend](https://git.rwth-aachen.de/kunstforum-tu-darmstadt/kutud-backend).

Currently the App is not available on the Google Play Store and the Apple App Store.

## Development

The app was developed on Flutter version 3.10.1 and currently only works for this version.
For further development you need the following requirements:

- [Android Studio](https://developer.android.com/studio?hl=de&gclsrc=ds&gclsrc=ds)
  - Plugins: Dart & Flutter
- [Flutter 3.10.1](https://docs.flutter.dev/get-started/install)

To run the project follow the [official Tutorial](https://flutter.dev/docs/get-started/test-drive?tab=terminal).


## Overview

<p>
<details>
<summary>Home Screen</summary>
The home screen is divided into two categories presenting an overview of the art on campus and the exhibitions.
<br/>
<img src="./docs/images/home_screen.png" height="500">

</details>
</p>

<p>
<details>
<summary>Map Screen</summary>
The map screen displays the campus art locations on a 2D map and can be used for navigating to the desired artwork.
<br/>
<img src="./docs/images/map_screen.png" height="500">

</details>
</p>

<p>
<details>
<summary>Search Screen</summary>
The search screen helps users to find artworks and exhibitions. 
You can search by artwork identifiers, text or QR codes.
<br/>
<img src="./docs/images/search_screen.png" height="500">

</details>
</p>

<p>
<details>
<summary>Exhibitions Screen</summary>
The exhibition screen provides an overview over all current and past exhibitions of the Kunstforum.
<br/>
<img src="./docs/images/exhibitions_screen.png" height="500">

</details>
</p>

<p>
<details>
<summary>Settings Screen</summary>
The settings screen allows users to customize various aspects of the app.
<br/>
<img src="./docs/images/settings_screen.png" height="500">

</details>
</p>

<p>
<details>
<summary>Artwork Screen</summary>
The artwork screen provides detailed information for a single artwork and its creator. 
It includes a textual description, an audio guide and a video.
<br/>
<img src="./docs/images/artwork_screen.png" height="500">

</details>
</p>


## Contributors

This project was created as part of a Projekt-Praktikum at the TU Darmstadt in the summer term 2021 by the team of:
* Aria Jamili
* Betty Ballin
* Mohamad Kattaa
* Muhamad Azem
* Anissa Manai

It was further improved as part of the
[Bachelor-Praktikum](https://www.informatik.tu-darmstadt.de/studium_fb20/im_studium/studiengaenge_liste/bachelor_praktikum.de.jsp)
at the TU Darmstadt in the winter term 2021/22 by the team of:
* Louis Rethfeld
* Lukas Arnold
* Paul Jonas Kurz
* Philipp Hempel
* Timo Imhof