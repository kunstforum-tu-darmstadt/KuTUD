import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kunstforum_tu_darmstadt/api/api_connection.dart';
import 'package:kunstforum_tu_darmstadt/api/api_util.dart';
import 'package:kunstforum_tu_darmstadt/models/app_information.dart';
import 'package:kunstforum_tu_darmstadt/models/artist.dart';
import 'package:kunstforum_tu_darmstadt/models/artwork.dart';
import 'package:kunstforum_tu_darmstadt/models/collection.dart';
import 'package:kunstforum_tu_darmstadt/models/copyright.dart';
import 'package:kunstforum_tu_darmstadt/models/exhibition.dart';
import 'package:kunstforum_tu_darmstadt/models/identifiable.dart';
import 'package:kunstforum_tu_darmstadt/models/media.dart';

/// A [ChangeNotifier] providing global access to the API.
class ModelCache extends ChangeNotifier {
  /// ----------------------- Attributes -------------------------

  /// The list of [Artwork]s
  final Map<int, Artwork> _artworkList = {};

  /// The list of [Artist]s
  final Map<int, Artist> _artistList = {};

  /// The list of [Exhibition]s
  final Map<int, Exhibition> _exhibitionList = {};

  /// The campus exhibition
  late Exhibition _campusExhibition;

  /// The Campus [Exhibition]'s name
  final String _campusExhibitionName = "Campus-Kunst";

  /// The list of [int]s that mark the favorite [Artwork]s as indices in [_artworkList]
  final List<int> _favoriteArtworkList = [];

  final Map<String, Media> _mediaList = {};

  /// The list of [Copyright]s
  late Map<String, Copyright> _copyrightList = {};

  /// The [AppInfo]
  AppInformation _appInformation = AppInformation();

  /// ----------------------- Class Getters -------------------------

  /// To only get the properties and not set them from outside this class

  /// The list of [Artwork]s
  Map<int, Artwork> get artworkList => _artworkList;

  /// The list of [Exhibition]s
  Map<int, Exhibition> get exhibitionList => _exhibitionList;

  /// The list of [Artist]s
  Map<int, Artist> get artistList => _artistList;

  /// The campus exhibition
  Exhibition get campusExhibition => _campusExhibition;

  /// The list of [int]s that are ID's of favorite artworks
  List<int> get favoriteArtworkList => _favoriteArtworkList;

  /// The list of [Media]
  Map<String, Media> get mediaList => _mediaList;

  /// The list of [Copyright]
  Map<String, Copyright> get copyrightList => _copyrightList;

  /// Get app info
  AppInformation get appInfo => _appInformation;

  /// ----------------------- Downloads to the Cache in the beginning -------------------------

  /// Downloads all data from the Backend asynchronously,
  /// that should be downloaded as the app is started
  Future<void> getAll(BuildContext context) async {
    var futures = <Future<void>>[];
    futures.add(downloadExhibitions(context));
    futures.add(downloadArtworks(context));
    futures.add(downloadArtists(context));
    futures.add(downloadAppInformation(context));
    futures.add(downloadCopyrights(context));
    await Future.wait(futures);
  }

  /// Extracts the [Exhibition] that is marked as the Campus Exhibition
  void extractCampusArt(BuildContext context) {
    int campusArtID = _exhibitionList.values
        .firstWhere((element) =>
            element.getFieldValue("title", 'de') == _campusExhibitionName)
        .id;
    _campusExhibition = _exhibitionList.remove(campusArtID)!;
  }

  /// Downloads a collection using the function [getAll]
  /// and all its items using the function [getSingle].
  /// Adds all downloaded items with their id as key to the [targetMap].
  /// This is used for Map<int, T> with T extends Identifiable
  Future<void> _downloadCollection<T extends Identifiable>(
      Future<Collection> Function(BuildContext context) getAll,
      T Function(Map<String, dynamic> json) getSingle,
      Map<int, T> targetMap,
      BuildContext context) async {
    Collection collection = await getAll(context);

    for (var object in collection.results) {
      targetMap.addAll({object["id"]: getSingle(object)});
    }

    print("Got ${targetMap.length} ${T.toString()}s");
  }

  /// Downloads a collection using the function [getAll]
  /// and all its items using the function [getSingle].
  /// Adds all downloaded items with their id as key to the [targetMap].
  /// This is used for Map<String, T>
  Future<void> _downloadStaticCollection<T>(
      Future<Collection> Function(BuildContext context) getAll,
      T Function(Map<String, dynamic> json) getSingle,
      Map<String, T> targetMap,
      BuildContext context) async {
    Collection collection = await getAll(context);
    int itemCounter = 0;
    for (var object in collection.results) {
      targetMap.addAll({itemCounter.toString(): getSingle(object)});
      itemCounter++;
    }

    print("Got ${targetMap.length} ${T.toString()}s");
  }

  /// Downloads all [Exhibition] JSON files, creates [Exhibition] objects
  /// and adds them to the [_exhibitionList]
  /// Also downloads the [Media] JSON files from the exhibitions coverImage
  Future<void> downloadExhibitions(BuildContext context) async {
    await _downloadCollection<Exhibition>(ApiConnection.getAllExhibitions,
        ApiUtil.getExhibition, _exhibitionList, context);
    await downloadExhibitionCoverImageUrls(context);
    extractCampusArt(context);
  }

  /// Downloads all [Artwork] JSON files, creates [Artwork] objects
  /// and adds them to the [_artworkList]
  Future<void> downloadArtworks(BuildContext context) async {
    await _downloadCollection<Artwork>(ApiConnection.getAllArtworks,
        ApiUtil.getArtwork, _artworkList, context);
  }

  /// Downloads all [Artist] JSON files, creates [Artist] objects
  /// and adds them to the [_artistList]
  Future<void> downloadArtists(BuildContext context) async {
    await _downloadCollection<Artist>(
        ApiConnection.getAllArtists, ApiUtil.getArtist, _artistList, context);
  }

  /// Downloads all [Copyright] JSON files, creates [Copyright] objects
  /// and adds them to the [_copyrightList]
  Future<void> downloadCopyrights(BuildContext context) async {
    await _downloadStaticCollection(ApiConnection.getAllCopyright,
        ApiUtil.getCopyright, _copyrightList, context);
  }

  /// Downloads the [AppInformation] JSON file, creates an [AppInformation] object
  /// and saves it in [_appInformation]
  Future<void> downloadAppInformation(BuildContext context) async {
    _appInformation = await ApiConnection.getInfo(context);
    print("Got AppInformation");
  }

  /// Downloads the [Media] JSON files from the exhibitions only,
  /// creates [Media] objects of them
  /// and adds them to the [_mediaList]
  /// So they must not be downloaded when opening the main or exhibitions screen
  Future<void> downloadExhibitionCoverImageUrls(BuildContext context) async {
    var futures = <Future<Media>>[];
    for (var exhibition in exhibitionList.values) {
      futures.add(ApiConnection.getMedia(exhibition.coverImage, context));
    }
    var medias = await Future.wait(futures);
    for (var media in medias) {
      _mediaList.addAll({media.uuid: media});
    }
  }

  /// ----------------------- Helpful Methods -------------------------

  /// Checks if the [Media] with the given [uuid] is cached in the [_mediaList]
  /// If so it returns the [Media] otherwise it downloads it,
  /// saves it to the [_mediaList] and then returns it
  Future<Media> getMediaCached(String uuid, BuildContext context) async {
    Media result;
    var mediaListResult = _mediaList[uuid];
    if (mediaListResult != null) {
      result = mediaListResult;
    } else {
      result = await ApiConnection.getMedia(uuid, context);
      _mediaList.addAll({uuid: result});
    }
    return result;
  }

  /// Returns the [Exhibition] of the passed [Artwork]
  Exhibition getExhibitionOf(int artworkID) {
    Map<int, Exhibition> allExhibitions = exhibitionList;
    allExhibitions.addAll({campusExhibition.id: campusExhibition});
    return allExhibitions.values
        .toList()
        .firstWhere((exhibition) => exhibition.artworkList.contains(artworkID));
  }

  /// Downloads image links for a given [artwork]
  Future<List<Media>> getImagesFromArtwork(
      Artwork artwork, BuildContext context) async {
    List<Future<Media>> futures = [];
    for (var element in artwork.images) {
      futures.add(getMediaCached(element, context));
    }
    return await Future.wait(futures);
  }
}
