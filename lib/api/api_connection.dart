import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:kunstforum_tu_darmstadt/config/route_generator.dart';
import 'package:kunstforum_tu_darmstadt/models/app_information.dart';
import 'package:kunstforum_tu_darmstadt/models/artist.dart';
import 'package:kunstforum_tu_darmstadt/models/artwork.dart';
import 'package:kunstforum_tu_darmstadt/models/collection.dart';
import 'package:kunstforum_tu_darmstadt/models/copyright.dart';
import 'package:kunstforum_tu_darmstadt/models/exhibition.dart';
import 'package:kunstforum_tu_darmstadt/models/media.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/display_refresh_screen.dart';

/// Manages the downloads from the api of the backend
class ApiConnection {
  static final String apiLink =
      'https://vm11.rbg.informatik.tu-darmstadt.de/api';

  /// Downloads all available items from the given [Collection] type from the api
  static Future<Collection> _getAllOfCollectionType(
      Future<Collection> Function(int, BuildContext) getSingle,
      BuildContext context) async {
    Collection accCollection = await getSingle(1, context);
    final int numberOfPages =
        (accCollection.count / accCollection.results.length).ceil();
    var futures = <Future<Collection>>[];
    for (int i = 2; i <= numberOfPages; i++) {
      futures.add(getSingle(i, context));
    }
    List<Collection> collections = await Future.wait(futures);
    return collections.fold<Collection>(accCollection,
        (previousValue, element) => previousValue.combine(element));
  }

  /// Downloads all available [Exhibition]s from the api
  static Future<Collection> getAllExhibitions(BuildContext context) async {
    return await _getAllOfCollectionType(getExhibitionsPage, context);
  }

  /// Downloads all available [Artwork]s from the api
  static Future<Collection> getAllArtworks(BuildContext context) async {
    return await _getAllOfCollectionType(getArtworksPage, context);
  }

  /// Downloads all available [Artist]s from the api
  static Future<Collection> getAllArtists(BuildContext context) async {
    return await _getAllOfCollectionType(getArtistsPage, context);
  }

  /// Downloads the [AppInformation] from the api
  static Future<Collection> getAllCopyright(BuildContext context) async {
    return await _getAllOfCollectionType(getCopyrightPage, context);
  }

  /// ----------------------- Download all items of a page -------------------------

  /// Downloads a specific page of [Exhibition]s from the api
  static Future<Collection> getExhibitionsPage(
      int page, BuildContext context) async {
    Uri url = Uri.parse(apiLink + '/exhibitions/?page=' + page.toString());
    return Collection.fromJson(await getData(url, context));
  }

  /// Downloads a specific page of [Artist]s from the api
  static Future<Collection> getArtistsPage(
      int page, BuildContext context) async {
    Uri url = Uri.parse(apiLink + '/artists/?page=' + page.toString());
    return Collection.fromJson(await getData(url, context));
  }

  /// Downloads a specific page of [Artwork]s from the api
  static Future<Collection> getArtworksPage(
      int page, BuildContext context) async {
    Uri url = Uri.parse(apiLink + '/artworks/?page=' + page.toString());
    return Collection.fromJson(await getData(url, context));
  }

  /// Downloads a specific page of [Copyright]s from the api
  static Future<Collection> getCopyrightPage(
      int page, BuildContext context) async {
    Uri url = Uri.parse(apiLink + '/copyright/?page=' + page.toString());
    return Collection.fromJson(await getData(url, context));
  }

  /// Downloads a specific [Media] identified by its uuid
  static Future<Media> getMedia(String uuid, BuildContext context) async {
    Uri url = Uri.parse(apiLink + '/media/' + uuid + '/');
    return Media.fromJson(await getData(url, context));
  }

  /// Downloads the [AppInformation] from the api
  static Future<AppInformation> getInfo(BuildContext context) async {
    Uri url = Uri.parse(apiLink + "/app_information/");
    return AppInformation.fromJsonArray(
        (await getData(url, context)).cast<Map<String, dynamic>>());
  }

  /// Downloads a json file from an url
  static Future<dynamic> getData(Uri url, BuildContext context) async {
    try {
      http.Response response =
          await http.get(url).timeout(Duration(seconds: 20));
      if (response.body.isEmpty) {
        throw Exception("getData() failed, no data found on " + url.toString());
      }

      return jsonDecode(utf8.decode(response.bodyBytes));
    } on Exception catch (e) {
      if (!DisplayRefreshScreen.opened) {
        DisplayRefreshScreen.opened = true;
        Navigator.of(context).pushNamed(RouteGenerator.displayRefreshScreen);
      }
      throw Exception(
        "getData() failed in getting a valid response from " +
            url.toString() +
            " with Exception: " +
            e.toString(),
      );
    }
  }
}
