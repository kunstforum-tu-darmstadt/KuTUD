import 'package:kunstforum_tu_darmstadt/models/copyright.dart';
import 'package:url_launcher/url_launcher.dart';

import '../models/artist.dart';
import '../models/artwork.dart';
import '../models/exhibition.dart';

/// ----------------------- API helper class -------------------------

class ApiUtil {
  /// Initializes an [Exhibition] object from the given json element
  static Exhibition getExhibition(Map<String, dynamic> json) {
    return Exhibition.fromJson(json);
  }

  /// Initializes an [Artwork] object from the given json element
  static Artwork getArtwork(Map<String, dynamic> json) {
    return Artwork.fromJson(json);
  }

  /// Initializes an [Artist] object from the given json element
  static Artist getArtist(Map<String, dynamic> json) {
    return Artist.fromJson(json);
  }

  /// Initializes an [Copyright] object from the given json element
  static Copyright getCopyright(Map<String, dynamic> json) {
    return Copyright.fromJson(json);
  }

  /// Launches a link in the devices browser
  static launchURL(urlString) async {
    final Uri url = Uri.parse(urlString);
    if (!await launchUrl(url)) {
      throw Exception('Could not launch $url');
    }
  }
}
