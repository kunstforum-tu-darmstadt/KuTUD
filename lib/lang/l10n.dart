import 'package:flutter/material.dart';

class L10n {
  static final all = [
    const Locale('en'),
    const Locale('de'),
  ];

  static final allLangString = [
    'English',
    'Deutsch',
  ];

  static final defaultLang = 'de';

  static final allLangCode = [
    'en',
    'de',
  ];

  static Locale getLocal(String code) {
    switch (code) {
      case 'Deutsch':
        return Locale('de');
      case 'English':
      default:
        return Locale('en');
    }
  }
}
