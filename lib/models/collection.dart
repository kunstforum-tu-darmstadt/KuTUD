/// Represents a collection of objects of a class that extends [Identifiable]
class Collection {
  /// the number of elements on all pages
  final int count;

  /// the url to the next page
  final String? next;

  /// the url to the previous page
  final String? previous;

  /// A Map mapping entries to the associated elements on the current page
  final List<Map<String, dynamic>> results;

  Collection(
      {required this.count, this.next, this.previous, required this.results});

  /// Creates the [Collection] object from the given map which is extracted from a JSON file
  factory Collection.fromJson(Map<String, dynamic> json) {
    return Collection(
        count: json["count"],
        next: json["next"],
        previous: json["previous"],
        results: json["results"].cast<Map<String, dynamic>>());
  }

  /// This method adds all entries in List [result] of the given [Collection] to this object.
  /// Entry [previous] is determined by the entry of this object.
  /// Entry [next] is determined by the entry of the parameter object.
  /// This method should be used to combine consecutive objects
  Collection combine(Collection other) {
    return Collection(
        count: count,
        previous: previous,
        next: other.next,
        results: results + other.results);
  }
}
