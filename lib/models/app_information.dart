import 'package:kunstforum_tu_darmstadt/models/translation_util.dart';

enum AppInformationType {
  group,
  artAtUniversity,
  imprint,
  website,
  dataProtectionDeclaration
}

/// Represents the collection of the three texts of the group, the art at the university and the imprint
class AppInformation {
  /// Info entry for the art group
  final AppInformationEntry? group;

  /// Info entry for the art at the university
  final AppInformationEntry? artAtUniversity;

  /// Info entry for the app imprint
  final AppInformationEntry? imprint;

  // Data Protection Clause
  final AppInformationEntry? dataProtection;

  AppInformation(
      {this.group, this.artAtUniversity, this.imprint, this.dataProtection});

  /// Creates the [AppInformation] object from the given map which is extracted from a JSON file
  factory AppInformation.fromJsonArray(List<Map<String, dynamic>> jsonList) {
    // creates a new [AppInformationEntry] Object for every item in the list
    final entries =
        jsonList.map((entry) => AppInformationEntry.fromJson(entry));
    // sorts the entries alphabetically
    final map = {for (var entry in entries) entry.key: entry};

    return AppInformation(
      group: map[AppInformationType.group],
      artAtUniversity: map[AppInformationType.artAtUniversity],
      imprint: map[AppInformationType.imprint],
      dataProtection: map[AppInformationType.dataProtectionDeclaration],
    );
  }
}

/// A class wrapping the information for [SettingScreen]
class AppInformationEntry with TranslationUtil {
  /// list of localized fields
  static final List<String> localizedFields = ['text'];

  /// about us section in the app
  final AppInformationType key;

  /// the date when the AppInformation was created as an entry in the backend
  final DateTime createdAt;

  /// the date when the AppInformation was updated last as an entry in the backend
  final DateTime updatedAt;

  /// information about the app
  final Map<String, String> text;

  AppInformationEntry(
      {required this.key,
      required this.createdAt,
      required this.updatedAt,
      required this.text});

  /// Creates the [AppInformationEntry] object from the given map which is extracted from a JSON file
  factory AppInformationEntry.fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> localizedValues =
        TranslationUtil.getLocalizedFields(localizedFields, json);
    AppInformationType appInformationType;
    String appInformationTypeString = json["key"];
    switch (appInformationTypeString) {
      case "group":
        appInformationType = AppInformationType.group;
        break;
      case "art_at_university":
        appInformationType = AppInformationType.artAtUniversity;
        break;
      case "imprint":
        appInformationType = AppInformationType.imprint;
        break;
      case "app_website":
        // this information is unused
        appInformationType = AppInformationType.website;
        break;
      case "data_protection_declaration":
        appInformationType = AppInformationType.dataProtectionDeclaration;
        break;
      default:
        throw Exception("while reading the app information json data: " +
            appInformationTypeString +
            " is not an expected part of the app information data");
    }

    return AppInformationEntry(
      key: appInformationType,
      createdAt: DateTime.fromMillisecondsSinceEpoch(json["created_at"] * 1000),
      updatedAt: DateTime.fromMillisecondsSinceEpoch(json["updated_at"] * 1000),
      text: localizedValues['text'],
    );
  }

  /// returns the field value for a given String
  @override
  Map<String, String> getFieldByName(String fieldName) {
    switch (fieldName) {
      case 'text':
        return text;
      default:
        throw ('Unsupported!');
    }
  }
}
