import 'package:kunstforum_tu_darmstadt/models/translation_util.dart';

import 'identifiable.dart';

/// An abstract display of an artist
class Artist with TranslationUtil implements Identifiable {
  /// list of localized fields
  static final List<String> localizedFields = ['info_text'];

  /// the unique id of a Artist
  @override
  final int id;

  /// the date when the Artist was created as an entry in the backend
  final DateTime createdAt;

  /// the date when the Artist was updated last as an entry in the backend
  final DateTime updatedAt;

  /// the name of the Artist
  final String name;

  /// the year the Artist was born
  final int? yearBirth;

  /// the year the Artist died
  final int? yearDeath;

  /// key data of the Artist
  final Map<String, String>? infoText;

  Artist({
    required this.id,
    required this.createdAt,
    required this.updatedAt,
    required this.name,
    this.yearBirth,
    this.yearDeath,
    this.infoText,
  });

  /// Creates the [Artist] object from the given map which is extracted from a JSON file
  factory Artist.fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> localizedValues =
        TranslationUtil.getLocalizedFields(localizedFields, json);
    return Artist(
      id: json["id"],
      createdAt: DateTime.fromMillisecondsSinceEpoch(json["created_at"] * 1000),
      updatedAt: DateTime.fromMillisecondsSinceEpoch(json["updated_at"] * 1000),
      name: json["name"],
      yearBirth: json["year_birth"],
      yearDeath: json["year_death"],
      infoText: localizedValues["info_text"],
    );
  }

  /// returns the field value for a given String
  @override
  Map<String, String> getFieldByName(String fieldName) {
    switch (fieldName) {
      case 'info_text':
        return infoText!;
      default:
        throw ('Unsupported!');
    }
  }
}
