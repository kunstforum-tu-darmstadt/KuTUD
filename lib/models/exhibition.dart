import 'package:kunstforum_tu_darmstadt/models/identifiable.dart';
import 'package:kunstforum_tu_darmstadt/models/translation_util.dart';

/// An abstract display of an exhibition, that contains a list of Artworks
class Exhibition with TranslationUtil implements Identifiable {
  /// list of localized fields
  static final List<String> localizedFields = ['title'];

  /// Exhibition's id
  @override
  final int id;

  /// the date when the Exhibition was created as an entry in the backend
  final DateTime createdAt;

  /// the date when the Exhibition was updated last as an entry in the backend
  final DateTime updatedAt;

  /// Exhibition's title
  final Map<String, String> title;

  /// the date when the Exhibition starts
  final DateTime startDate;

  /// the date when the Exhibition ends
  final DateTime endDate;

  /// the uuid of the image that should be the cover image of the exhibition
  final String coverImage;

  /// a list that contains the ids of the artworks
  final List<int> artworkList;

  /// the number of Artworks
  final int artworkCount;

  Exhibition(
      {required this.id,
      required this.createdAt,
      required this.updatedAt,
      required this.title,
      required this.startDate,
      required this.endDate,
      required this.coverImage,
      required this.artworkList,
      required this.artworkCount});

  /// Creates the [Exhibition] object from the given map which is extracted from a JSON file
  factory Exhibition.fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> localizedValues =
        TranslationUtil.getLocalizedFields(localizedFields, json);
    return Exhibition(
        id: json["id"],
        createdAt:
            DateTime.fromMillisecondsSinceEpoch(json["created_at"] * 1000),
        updatedAt:
            DateTime.fromMillisecondsSinceEpoch(json["updated_at"] * 1000),
        title: localizedValues["title"],
        startDate:
            DateTime.fromMillisecondsSinceEpoch(json["start_date"] * 1000),
        endDate: DateTime.fromMillisecondsSinceEpoch(json["end_date"] * 1000),
        coverImage: json["cover_image"],
        artworkList: json["artworks"].cast<int>(),
        artworkCount: json["artwork_count"]);
  }

  /// returns the field value for a given String
  @override
  Map<String, String> getFieldByName(String fieldName) {
    switch (fieldName) {
      case 'title':
        return title;
      default:
        throw ('Unsupported!');
    }
  }
}
