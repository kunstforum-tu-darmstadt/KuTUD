import '../lang/l10n.dart';

mixin TranslationUtil {
  /// retrieves all localized values for the localized fields
  static Map<String, dynamic> getLocalizedFields(
      List<String> localizedFields, Map<String, dynamic> json) {
    Map<String, dynamic> allFields = <String, Map<String, String>>{};
    for (var element in localizedFields) {
      Map<String, String> allLang = <String, String>{};
      for (var lang in L10n.allLangCode) {
        String languageField = element + '_' + lang;
        json[languageField] == null
            ? allLang[lang] = ''
            : allLang[lang] = json[languageField];
      }
      allFields[element] = allLang;
    }
    return allFields;
  }

  /// returns the Value to be displayed for a given language
  /// null already checked in getLocalizedFields
  String getFieldValue(String fieldName, String languageCode) {
    if (getFieldByName(fieldName)[languageCode]! == '') {
      return getFieldByName(fieldName)[L10n.defaultLang]!;
    }
    return getFieldByName(fieldName)[languageCode]!;
  }

  /// abstract, returns the fieldValues for an object
  getFieldByName(String fieldName) {}
}
