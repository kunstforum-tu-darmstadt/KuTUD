import 'package:kunstforum_tu_darmstadt/models/exhibition.dart';
import 'package:kunstforum_tu_darmstadt/models/translation_util.dart';

import 'identifiable.dart';

/// An abstract display of an Artwork, that contains a list of Images
class Artwork with TranslationUtil implements Identifiable {
  /// list of localized fields
  static final List<String> localizedFields = [
    'name',
    'type',
    'canvas',
    'acquired',
    'info_text',
    'location',
    'linked_artwork_description',
    "audio_guide"
  ];

  /// id of the Artwork
  @override
  final int id;

  /// the date when the [Artwork] was created as an entry in the backend
  final DateTime createdAt;

  /// the date when the [Artwork] was updated last as an entry in the backend
  final DateTime updatedAt;

  /// the identifier used for searching
  final String identifier;

  /// the name of the [Artwork]
  final Map<String, String> name;

  /// year in which the [Artwork] was created
  final String? year;

  /// the type of the [Artwork]
  final Map<String, String>? type;

  /// what kind of canvas was used for the [Artwork]
  final Map<String, String>? canvas;

  /// id of the artist
  final int? artistId;

  /// name of the owner
  final String? owner;

  /// how the [Artwork] was acquired
  final Map<String, String>? howAcquired;

  /// a informational text about the [Artwork]
  final Map<String, String> infoText;

  /// a reference to a [Artwork] from another [Exhibition]
  final int? linkedArtwork;

  /// information about the linked [Artwork]
  final Map<String, String>? linkedArtworkDescription;

  /// uuids of all associated images
  final List<String> images;

  /// number of images of the [Artwork]
  final int imageCount;

  /// uuid of image that is shown in the exhibition screen or in the campus art slider if part of campus art
  final String coverImage;

  /// uuid of the associated audio media
  final Map<String, String>? audioGuide;

  /// url of the associated video
  final String? videoLink;

  /// latitude of the location of the [Artwork]
  final double? latitude;

  /// longitude of the location of the [Artwork]
  final double? longitude;

  /// location of the [Artwork]
  final Map<String, String>? location;

  /// address of the [Artwork]
  final String? address;

  /// name of the campus where the [Artwork] is located
  final String? campus;

  /// defines if the [Artwork] is new, updated or old
  final String? createdStatus;

  Artwork({
    required this.id,
    required this.createdAt,
    required this.updatedAt,
    required this.identifier,
    required this.name,
    this.year,
    this.type,
    this.canvas,
    this.artistId,
    this.owner,
    this.howAcquired,
    required this.infoText,
    this.linkedArtwork,
    this.linkedArtworkDescription,
    required this.images,
    required this.imageCount,
    required this.coverImage,
    this.audioGuide,
    this.videoLink,
    this.latitude,
    this.longitude,
    this.location,
    this.address,
    this.campus,
    this.createdStatus,
  });

  /// Creates the [Artwork] object from the given map which is extracted from a JSON file
  factory Artwork.fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> localizedValues =
        TranslationUtil.getLocalizedFields(localizedFields, json);
    return Artwork(
      id: json["id"],
      createdAt: DateTime.fromMillisecondsSinceEpoch(json["created_at"] * 1000),
      updatedAt: DateTime.fromMillisecondsSinceEpoch(json["updated_at"] * 1000),
      identifier: json["identifier"],
      name: localizedValues["name"],
      year: json["year"],
      type: localizedValues["type"],
      canvas: localizedValues["canvas"],
      artistId: json["artist"],
      owner: json["owner"],
      howAcquired: localizedValues["acquired"],
      infoText: localizedValues["info_text"],
      linkedArtwork: json["linked_artwork"],
      linkedArtworkDescription: localizedValues["linked_artwork_description"],
      images: json["images"].cast<String>(),
      imageCount: json["image_count"],
      coverImage: json["cover_image"],
      audioGuide: localizedValues["audio_guide"],
      videoLink: json["video_link"],
      latitude: double.tryParse(json["latitude"] ?? ""),
      longitude: double.tryParse(json["longitude"] ?? ""),
      location: localizedValues["location"],
      address: json["address"],
      campus: json["campus"],
      createdStatus: json["change_status"],
    );
  }

  /// returns the field value for a given String
  @override
  Map<String, String> getFieldByName(String fieldName) {
    switch (fieldName) {
      case 'name':
        return name;
      case 'type':
        return type!;
      case 'canvas':
        return canvas!;
      case 'acquired':
        return howAcquired!;
      case 'info_text':
        return infoText;
      case 'location':
        return location!;
      case 'linked_artwork_description':
        return linkedArtworkDescription!;
      case 'audio_guide':
        return audioGuide!;
      default:
        throw ('Unsupported!');
    }
  }
}
