/// The types of a medium. Either it represents an image or an audio file
enum MediaCategory { image, audio }

/// An abstract display of an medium, which is either an image or an audio
class Media {
  /// the unique id of a Media
  final String uuid;

  /// the date when the Media was created as an entry in the backend
  final DateTime createdAt;

  /// the date when the Media was updated last as an entry in the backend
  final DateTime updatedAt;

  /// the description text of the Media
  final String description;

  /// the copyright of the media file
  final String copyright;

  /// the type of Media (either image or audio)
  final MediaCategory category;

  /// the specific data type of the associated media file
  final String type;

  /// the url with which the associated media file can be downloaded
  final String url;

  Media(
      {required this.uuid,
      required this.createdAt,
      required this.updatedAt,
      required this.description,
      required this.copyright,
      required this.category,
      required this.type,
      required this.url});

  /// Creates the [Media] object from the given map which is extracted from a JSON file
  factory Media.fromJson(Map<String, dynamic> json) {
    MediaCategory mediaType;
    String category = json["category"];
    switch (category) {
      case "image":
        mediaType = MediaCategory.image;
        break;
      case "audio-guide":
        mediaType = MediaCategory.audio;
        break;
      default:
        throw Exception(category + " is neither an image or audio");
    }

    return Media(
      uuid: json["uuid"],
      createdAt: DateTime.fromMillisecondsSinceEpoch(json["created_at"] * 1000),
      updatedAt: DateTime.fromMillisecondsSinceEpoch(json["updated_at"] * 1000),
      description: json["description"],
      copyright: json["copyright"],
      category: mediaType,
      type: json["type"],
      url: json["url"],
    );
  }
}
