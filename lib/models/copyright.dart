/// Display of the copyright aka owners of the Pictures used in the App
class Copyright {
  /// the copyright of the media file
  final String copyright;

  /// the copyright Url of the media file
  final String? copyrightUrl;

  Copyright({required this.copyright, this.copyrightUrl});

  /// Creates the [Media] object from the given map which is extracted from a JSON file
  factory Copyright.fromJson(Map<String, dynamic> json) {
    return Copyright(
        copyright: json["copyright"], copyrightUrl: json["copyright_url"]);
  }
}
