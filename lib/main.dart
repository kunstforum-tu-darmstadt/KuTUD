import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:kunstforum_tu_darmstadt/config/themes.dart';

import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'config/route_generator.dart';
import 'lang/l10n.dart';
import 'dart:ui' as ui;
import 'lang/locale_provider.dart';

import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/loading_screen.dart';
import 'package:kunstforum_tu_darmstadt/api/model_cache.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/introduction_screen.dart';
import 'package:lehttp_overrides/lehttp_overrides.dart';

/// A flag indicating whether to show the [IntroScreen]
bool? showIntroScreen;
bool? agreementRequired;

/// Runs the app by building the widget [KuTUD]
void main() async {
  if (Platform.isAndroid) {
    HttpOverrides.global = LEHttpOverrides();
  }
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  final prefs = await SharedPreferences.getInstance();
  showIntroScreen = prefs.getBool("showIntroScreen") ?? true;
  agreementRequired = prefs.getBool("agreementRequired") ?? true;
  // uncomment the next two lines and the imports for release version before merging in master
  RenderErrorBox.backgroundColor = Colors.transparent;
  RenderErrorBox.textStyle = ui.TextStyle(color: Colors.transparent);
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider<LocaleProvider>(create: (_) => LocaleProvider()),
    ChangeNotifierProvider<AppSetting>(create: (_) => AppSetting()),
    ChangeNotifierProvider<ModelCache>(create: (_) => ModelCache()),
  ], child: KuTUD()));
}

///
/// A [StatefulWidget] building a [MaterialApp] which is this app's core.
///
class KuTUD extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AppSetting appSetting = Provider.of<AppSetting>(context);
    // make statusBar transparent for light design
    final localProvider = Provider.of<LocaleProvider>(context);
    return ChangeNotifierProvider(
        create: (_) => ThemeNotifier(),
        child: Consumer<ThemeNotifier>(
            builder: (_, ThemeNotifier themeNotifier, __) {
          SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
              statusBarColor: Colors.transparent,
              statusBarIconBrightness:
                  themeNotifier.themeMode == ThemeMode.light
                      ? Brightness.dark
                      : Brightness.light));
          return MaterialApp(
            // uncomment the next line for release version before merging in master
            debugShowCheckedModeBanner: false,
            theme: lightTheme(appSetting.sizeSetting),
            darkTheme: darkTheme(appSetting.sizeSetting),
            themeMode: themeNotifier.themeMode,
            title: 'Kunstforum',
            home: (showIntroScreen! || agreementRequired!)
                ? IntroScreen()
                : LoadingScreen(),
            onGenerateRoute: RouteGenerator.generateRoute,
            locale: appSetting.language != null
                ? Locale(appSetting.language!)
                : localProvider.locale,
            supportedLocales: L10n.all,
            localizationsDelegates: [
              AppLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
            ],
          );
        }));
  }
}
