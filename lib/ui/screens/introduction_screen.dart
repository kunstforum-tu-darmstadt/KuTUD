import 'dart:convert';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/settings/favorite_settings.dart';
import 'package:provider/provider.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/loading_screen.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';

import '../../api/model_cache.dart';
import '../../lang/l10n.dart';
import 'display_refresh_screen.dart';

/// A [StatefulWidget] that displays an Onboarding Screen for the App.
class IntroScreen extends StatefulWidget {
  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  /// An instance of [AppSetting]
  late AppSetting _appSetting;

  bool _hasAccepted = false;

  /// A  flag indicated whether the app has internet access
  bool _isConnected = false;

  /// A [GlobalKey] for tracing changes.
  final _introKey = GlobalKey<IntroductionScreenState>();

  /// A  flag indicated whether the [AppSetting] is initialized
  bool _initSetting = true;

  /// An instance of [SharedPreferences]
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  late List<String> introductionScreenTitles;
  late List<String> introductionScreenSubtitles;
  late List<List<String>> introductionImages;

  /// sets all intro screen texts and image filenames
  void initIntroductionTexts(BuildContext context) {
    final modelCache = Provider.of<ModelCache>(context);
    AppSetting appSetting = Provider.of<AppSetting>(context);
    String languageCode = appSetting.language ?? L10n.defaultLang;

    introductionScreenTitles = [
      AppLocalizations.of(context)!.introScreenTitle1,
      AppLocalizations.of(context)!.introScreenTitle2,
      AppLocalizations.of(context)!.introScreenTitle3,
      AppLocalizations.of(context)!.introScreenTitle4,
      AppLocalizations.of(context)!.introScreenTitle5,
      AppLocalizations.of(context)!.introScreenTitle6,
      AppLocalizations.of(context)!.introScreenTitle7,
      AppLocalizations.of(context)!.dataProtectionAgreement.toUpperCase()
    ];
    introductionScreenSubtitles = [
      AppLocalizations.of(context)!.introScreenSubTitle1,
      AppLocalizations.of(context)!.introScreenSubTitle2,
      AppLocalizations.of(context)!.introScreenSubTitle3,
      AppLocalizations.of(context)!.introScreenSubTitle4,
      AppLocalizations.of(context)!.introScreenSubTitle5,
      AppLocalizations.of(context)!.introScreenSubTitle6,
      AppLocalizations.of(context)!.introScreenSubTitle7,
      modelCache.appInfo?.dataProtection?.getFieldValue("text", languageCode) ??
          ""
    ];
    introductionImages = [
      ['kutud.jpg', 'kutud_white.jpg'],
      ['2_light.png', '2_dark.png'],
      ['3_light.png', '3_dark.png'],
      ['4_light.png', '4_dark.png'],
      ['5_light.png', '5_dark.png'],
      ['6_light.png', '6_dark.png'],
      ['7_light.png', '7_dark.png'],
      ['kutud.jpg', 'kutud_white.jpg'],
    ];
  }

  /// creates all Page widgets for the introduction
  List<PageViewModel> getIntroductionPages() {
    List<PageViewModel> pages = [];
    int currentBrightness =
        Theme.of(context).brightness == Brightness.light ? 0 : 1;
    final PageDecoration pageDecoration = PageDecoration(
        descriptionPadding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 16.0),
        pageColor: currentBrightness == 0 ? Colors.white : Colors.black,
        imagePadding: EdgeInsets.all(5),
        imageFlex: 2);
    for (int i = 0; i < introductionScreenTitles.length; i++) {
      i < introductionScreenTitles.length - 1
          ? pages.add(PageViewModel(
              titleWidget: AutoSizeText(
                introductionScreenTitles[i],
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline3,
              ),
              bodyWidget: AutoSizeText(introductionScreenSubtitles[i],
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyText1),
              image: Image.asset(
                  'assets/images/introduction/${introductionImages[i][currentBrightness]}',
                  width: 350),
              decoration: pageDecoration))
          : pages.add(PageViewModel(
              titleWidget: AutoSizeText(
                introductionScreenTitles[i],
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline3,
              ),
              bodyWidget: Container(
                child: Column(
                  children: [
                    AutoSizeText(
                        AppLocalizations.of(context)!.dataProtectionRequest,
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.headline2),
                    SizedBox(
                      height: 20,
                    ),
                    AutoSizeText(introductionScreenSubtitles[i],
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.bodyText1),
                  ],
                ),
              ),
              footer: StatefulBuilder(
                  builder: (context, setState) => CheckboxListTile(
                      title: AutoSizeText(
                          AppLocalizations.of(context)!.dataProtectionAccept,
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.bodyText1),
                      value: _hasAccepted,
                      activeColor: _appSetting.primaryColor,
                      controlAffinity: ListTileControlAffinity.leading,
                      onChanged: (bool? isChecked) {
                        setState(() {
                          _hasAccepted = isChecked!;
                        });
                      })),
              decoration: pageDecoration));
    }
    return pages;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: checkConnectivity().then((e) => _runInitTasks()),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Container(
              color: Theme.of(context).brightness != Brightness.dark
                  ? Colors.white
                  : Colors.black);
        }
        initIntroductionTexts(context);
        _appSetting = Provider.of<AppSetting>(context);
        if (_initSetting) {
          _prefs.then((SharedPreferences prefs) {
            bool settingSaved = prefs.getBool("settingSaved") ?? false;
            if (settingSaved) {
              _appSetting.fromJson(jsonDecode(prefs.getString("appSetting")!));
            } else {
              _appSetting.initAppSetting(context);
            }
            FavoritesHandler.loadFavorites(context);
          });
          _initSetting = false;
        }
        return _isConnected
            ? Container(
                color:
                    Theme.of(context).bottomNavigationBarTheme.backgroundColor,
                child: SafeArea(
                  child: IntroductionScreen(
                    key: _introKey,
                    globalBackgroundColor: Colors.black12,
                    pages: getIntroductionPages(),
                    onDone: () => _onIntroEnd(context),
                    showSkipButton: true,
                    skipFlex: 0,
                    nextFlex: 0,
                    skip: AutoSizeText(AppLocalizations.of(context)!.skip,
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: _appSetting.primaryColor,
                            fontWeight: FontWeight.w700)),
                    next: AutoSizeText(AppLocalizations.of(context)!.next,
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: _appSetting.primaryColor,
                            fontWeight: FontWeight.w700)),
                    done: AutoSizeText(AppLocalizations.of(context)!.done,
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: _appSetting.primaryColor,
                            fontWeight: FontWeight.w700)),
                    curve: Curves.easeIn,
                    controlsMargin: const EdgeInsets.all(6),
                    controlsPadding: kIsWeb
                        ? const EdgeInsets.all(12.0)
                        : const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
                    color: _appSetting.primaryColor,
                    dotsDecorator: DotsDecorator(
                      size: _appSetting.dotSize,
                      activeColor: _appSetting.primaryColor.shade300,
                      color: _appSetting.primaryColor,
                      activeSize: Size(_appSetting.dotSize.width * 2,
                          _appSetting.dotSize.height),
                      activeShape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(25.0)),
                      ),
                    ),
                  ),
                ),
              )
            : DisplayRefreshScreen(onReload: () {
                setState(() {});
              });
      },
    );
  }

  /// ----------------------- Assisting functions -------------------------

  /// This method checks wether the device is connected with the internet or not
  Future<void> checkConnectivity() async {
    _isConnected =
        (await Connectivity().checkConnectivity()) != ConnectivityResult.none;
  }

  /// Downloads the AppInformation which is needed to display the data protection clause
  Future<void> _runInitTasks() async {
    bool success = false;

    /// Run each initializer method sequentially
    await Future.microtask(() async {
      final modelCache = Provider.of<ModelCache>(context, listen: false);
      if (_isConnected) {
        try {
          await modelCache.downloadAppInformation(context).then((value) {
            success = true;
          });
        } catch (e) {
          if (!success) setState(() {});
        }
      }
    }).whenComplete(() {
      // When all the initializers has been called and terminated their
      // execution. The screen is navigated to the home screen
      if (_isConnected && success) {}
    });
  }

  /// Navigates to the [LoadingScreen].
  void _onIntroEnd(context) {
    if (_hasAccepted) {
      _hasAccepted = false;
      final prefs = SharedPreferences.getInstance();
      prefs.then((_prefs) => _prefs.setBool("agreementRequired", false));
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
            builder: (_) => LoadingScreen(
                  initSettings: false,
                )),
      );
    } else {
      showDialog(
          context: context,
          builder: (context) => AlertDialog(
                content: Text(
                    AppLocalizations.of(context)!.dataProtectionRequest,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline1),
                actionsAlignment: MainAxisAlignment.center,
                actionsOverflowButtonSpacing: 2.0,
                actions: [
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    style: ButtonStyle(
                        overlayColor: MaterialStateColor.resolveWith(
                            (states) => _appSetting.primaryColor.shade100)),
                    child: Text("OK",
                        style: Theme.of(context).textTheme.headline3!.copyWith(
                            color: _appSetting.primaryColor,
                            fontWeight: FontWeight.w700)),
                  ),
                ],
              ));
    }
  }

  @override
  void initState() {
    final prefs = SharedPreferences.getInstance();
    prefs.then((_prefs) => _prefs.setBool("showIntroScreen", false));
    super.initState();
  }
}
