import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/settings/data_protection_settings.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/settings/legal_notice_settings.dart';
import 'package:provider/provider.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/settings/favorite_settings.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/settings/language_settings.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/settings/size_settings.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/settings/about_settings.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/settings/color_settings.dart';
import 'package:kunstforum_tu_darmstadt/api/model_cache.dart';

import '../../config/app_setting.dart';
import '../../lang/l10n.dart';

/// A [StatefulWidget] that displays the settings screen
/// with 7 buttons stored in a StaggeredGridView
class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

/// A State for [SettingsScreen] that computes the box containers
/// for the StaggeredGridView
class _SettingsScreenState extends State<SettingsScreen> {
  /// Class attributes

  /// Colors for the visible Buttons
  final brightButtonColor = Color(0xFF818284);
  final normalButtonColor = Color(0xFF6D6E71);
  final darkButtonColor = Color(0xFF58595B);

  @override
  Widget build(BuildContext context) {
    final modelCache = Provider.of<ModelCache>(context);
    AppSetting appSetting = Provider.of<AppSetting>(context);
    String languageCode = appSetting.language ?? L10n.defaultLang;
    return Column(
      children: [
        Flexible(
          child: Row(
            children: [
              Flexible(
                child: createButtons(
                    AppLocalizations.of(context)!.aboutUs.toUpperCase(),
                    modelCache.appInfo.group
                            ?.getFieldValue("text", languageCode) ??
                        "",
                    normalButtonColor),
              ),
              Flexible(
                child: createButtons(
                    AppLocalizations.of(context)!.aboutArt.toUpperCase(),
                    modelCache.appInfo.artAtUniversity
                            ?.getFieldValue("text", languageCode) ??
                        "",
                    brightButtonColor),
              ),
            ],
          ),
        ),
        Flexible(
          child: Row(
            children: [
              Flexible(
                child: createButtons(
                    AppLocalizations.of(context)!.colors.toUpperCase(),
                    "",
                    brightButtonColor),
              ),
              Flexible(
                child: createButtons(
                    AppLocalizations.of(context)!.sizes.toUpperCase(),
                    "",
                    normalButtonColor),
              ),
            ],
          ),
        ),
        Flexible(
          child: Row(
            children: [
              Flexible(
                child: createButtons(
                    AppLocalizations.of(context)!.favorites.toUpperCase(),
                    "",
                    normalButtonColor),
              ),
              Flexible(
                child: createButtons(
                    AppLocalizations.of(context)!.lang.toUpperCase(),
                    "",
                    brightButtonColor),
              ),
            ],
          ),
        ),
        Flexible(
          child: Row(
            children: [
              Flexible(
                child: createButtons(
                    AppLocalizations.of(context)!.dataProtection.toUpperCase(),
                    modelCache.appInfo.dataProtection
                            ?.getFieldValue("text", languageCode) ??
                        "",
                    brightButtonColor),
              ),
              Flexible(
                child: createButtons(
                    AppLocalizations.of(context)!.about.toUpperCase(),
                    modelCache.appInfo.imprint
                            ?.getFieldValue("text", languageCode) ??
                        "",
                    normalButtonColor),
              ),
            ],
          ),
        ),
      ],
    );
  }

  /// ----------------------- Assisting Widgets -------------------------

  /// Creates a [ElevatedButton] with the background-color [color] and
  /// the [text] as label
  Widget createButtons(String text, String infoText, Color color,
      {void Function()? onPressed}) {
    return TextButton(
      onPressed: () {
        showOptionDialog(text, infoText);
      },
      style: ButtonStyle(
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(borderRadius: BorderRadius.zero)),
          backgroundColor: MaterialStateProperty.all<Color>(color),
          splashFactory: NoSplash.splashFactory),
      child: Align(
        alignment: Alignment(-1, -0.75),
        child: Padding(
          padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
          child: AutoSizeText(text,
              style: Theme.of(context)
                  .textTheme
                  .button!
                  .copyWith(fontWeight: FontWeight.bold, color: Colors.white),
              textAlign: TextAlign.left),
        ),
      ),
    );
  }

  /// ----------------------- Assisting Functions -------------------------

  /// Display the option dialogs depending on pressed button text
  void showOptionDialog(String text, String infoText) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          if (text == AppLocalizations.of(context)!.colors.toUpperCase()) {
            return StatefulBuilder(builder: (context, setState) {
              return ColorSettings();
            });
          } else if (text == AppLocalizations.of(context)!.lang.toUpperCase()) {
            return LanguageSettings();
          } else if (text ==
              AppLocalizations.of(context)!.sizes.toUpperCase()) {
            return SizeSettings();
          } else if (text ==
              AppLocalizations.of(context)!.favorites.toUpperCase()) {
            return FavoriteSettings();
          } else if (text ==
              AppLocalizations.of(context)!.about.toUpperCase()) {
            return LegalNoticeSettings(about: text, text: infoText);
          } else if (text ==
              AppLocalizations.of(context)!.dataProtection.toUpperCase()) {
            return DataProtectionSettings(about: text, text: infoText);
          } else {
            return AboutSettings(about: text, text: infoText);
          }
        });
  }
}
