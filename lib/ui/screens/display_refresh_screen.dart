import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kunstforum_tu_darmstadt/config/kunstforum_icons.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

import '../../config/app_setting.dart';
import '../widgets/custom_app_bar.dart';

/// Builds a Image and a warning Text with [IconButton] to refresh screen
class DisplayRefreshScreen extends StatelessWidget {
  const DisplayRefreshScreen({Key? key, required this.onReload})
      : super(key: key);
  final Function() onReload;
  static bool opened = false;

  @override
  Widget build(BuildContext context) {
    final appSetting = Provider.of<AppSetting>(context);
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: Theme.of(context).cardColor,
        appBar: CustomAppBar(
          showBackButton: false,
          appSetting: appSetting,
        ),
        body: Container(
          color: Color.fromRGBO(88, 89, 91, 1.0),
          child: Column(
            children: [
              Expanded(
                flex: 2,
                child: Padding(
                    padding: const EdgeInsets.all(50),
                    child: Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: 50,
                      child: Image.asset(
                        'assets/images/Connection.png',
                        fit: BoxFit.scaleDown,
                        color: Color.fromRGBO(167, 169, 171, 1.0),
                      ),
                    )),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.fromLTRB(80, 15, 80, 5),
                  child: AutoSizeText(
                    AppLocalizations.of(context)!.alertNoWifi,
                    style: Theme.of(context).textTheme.headline2!.copyWith(
                        fontWeight: FontWeight.w900, color: Colors.white),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.fromLTRB(80, 5, 80, 15),
                  child: AutoSizeText(
                    AppLocalizations.of(context)!.alertPressReconnect,
                    style: Theme.of(context).textTheme.headline2!.copyWith(
                        fontWeight: FontWeight.w900, color: Colors.white),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: Center(
                  child: IconButton(
                    icon: Icon(Kunstforum.restart),
                    color: Colors.white,
                    onPressed: onReload,
                    iconSize: 50,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
