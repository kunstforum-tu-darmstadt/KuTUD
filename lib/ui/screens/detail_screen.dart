import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:kunstforum_tu_darmstadt/lang/l10n.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/settings/favorite_settings.dart';
import 'package:rxdart/rxdart.dart';
import 'package:provider/provider.dart';
import 'package:just_audio/just_audio.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:kunstforum_tu_darmstadt/api/model_cache.dart';
import 'package:kunstforum_tu_darmstadt/models/artist.dart';
import 'package:kunstforum_tu_darmstadt/models/artwork.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/custom_app_bar.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/map_screen_popup.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/artwork_card.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/carousel_slider_artwork.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/audio_guide_widgets.dart';
import 'package:kunstforum_tu_darmstadt/config/kunstforum_icons.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../config/route_generator.dart';
import '../../lang/locale_provider.dart';

/// A [StatefulWidget] that displays an audio player UI.
class AudioGuideScreen extends StatelessWidget {
  /// The [Artwork] whose media are going to be displayed
  final Artwork artwork;

  /// shows if an audio guide is available for [artwork]
  final bool hasAudio;

  /// shows if a video is available for [artwork]
  final bool hasVideo;

  /// signals the VideoImageBox if a video(false) or image(true) should be displayed
  final ValueNotifier<bool> valueVideoImageNotifier = ValueNotifier(true);

  /// signals the AudioTextBox if a text(0) or the audio guide(1) or nothing(2) should be displayed
  final ValueNotifier<int> valueAudioTextNotifier = ValueNotifier(0);

  AudioGuideScreen._(this.artwork, this.hasAudio, this.hasVideo);

  ///factory is used to access [artwork] to set hasAudio and hasVideo
  factory AudioGuideScreen({required Artwork artwork}) {
    return AudioGuideScreen._(
        artwork,
        artwork.audioGuide!.values.any((element) => element != ''),
        artwork.videoLink != null);
  }

  @override
  Widget build(BuildContext context) {
    final appSetting = Provider.of<AppSetting>(context);
    var localization = AppLocalizations.of(context)!;
    return Scaffold(
      backgroundColor: Theme.of(context).cardColor,
      appBar: CustomAppBar(
        appSetting: appSetting,
        appBarTitle: localization.artwork,
      ),
      body: SafeArea(
        child: LayoutBuilder(
          builder: (context, constraints) => CustomScrollView(
            slivers: [
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    _VideoImageBox(
                        valueVideoImageNotifier, artwork, constraints),
                    _ControlBox(
                        hasAudio,
                        hasVideo,
                        artwork,
                        valueVideoImageNotifier,
                        valueAudioTextNotifier,
                        constraints)
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

/// Box that displays either the images or the video of [artwork]
class _VideoImageBox extends StatefulWidget {
  /// Tells this box which screen is to be shown
  final ValueNotifier<bool> valueVideoImageNotifier;

  /// The displayed artwork of the audio guide screen
  final Artwork artwork;

  /// The constraints of the screen space
  final BoxConstraints constraints;

  _VideoImageBox(this.valueVideoImageNotifier, this.artwork, this.constraints);

  @override
  _VideoImageBoxState createState() => _VideoImageBoxState();
}

class _VideoImageBoxState extends State<_VideoImageBox>
    with AutomaticKeepAliveClientMixin {
  /// Video Player that controls the display of the video from the artwork
  YoutubePlayerController? videoPlayer;

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SizedBox(
      width: widget.constraints.maxWidth,

      /// This calculation comes from the mockup, were this widget should be 760 pixels high on a screen that is 1920 pixels high
      height: widget.constraints.maxHeight * 760 / 1920,
      child: ValueListenableBuilder<bool>(
          valueListenable: widget.valueVideoImageNotifier,
          builder: (context, value, child) {
            if (value) {
              _pauseVideo();
              return _imageDisplay();
            } else {
              _initVideo();
              return _videoDisplay();
            }
          }),
    );
  }

  /// Displays the video from [Artwork}
  Widget _videoDisplay() {
    final appSetting = Provider.of<AppSetting>(context);
    return YoutubePlayer(
      onEnded: (metaData) {
        videoPlayer!
          ..seekTo(Duration.zero)
          ..pause();
      },
      progressIndicatorColor: appSetting.primaryColor,
      progressColors: ProgressBarColors(
          backgroundColor: Colors.grey,
          playedColor: appSetting.primaryColor,
          bufferedColor: appSetting.primaryColor.withOpacity(0.5),
          handleColor: appSetting.primaryColor),
      controller: videoPlayer!,
      showVideoProgressIndicator: true,
      bottomActions: [
        CurrentPosition(),
        ProgressBar(
          isExpanded: true,
          colors: ProgressBarColors(
              playedColor: appSetting.primaryColor,
              bufferedColor: appSetting.primaryColor.withOpacity(0.5),
              handleColor: appSetting.primaryColor),
        ),
      ],
    );
  }

  /// Displays the images from [Artwork}
  Widget _imageDisplay() {
    return CarouselSliderArtwork(
      mediaUUIDs: widget.artwork.images,

      /// This calculation comes from the mockup, were this widget should be 760 pixels high on a screen that is 1920 pixels high
      boxSize: Size(widget.constraints.maxWidth,
          widget.constraints.maxHeight * 760 / 1920),
    );
  }

  /// Initializes the [YoutubePlayerController] if not initialized
  void _initVideo() {
    videoPlayer ??= YoutubePlayerController(
        initialVideoId:
            YoutubePlayer.convertUrlToId(widget.artwork.videoLink!)!,
        flags: YoutubePlayerFlags(autoPlay: false));
  }

  /// Pauses the video, if a [YoutubePlayerController] is initialized
  void _pauseVideo() {
    if (videoPlayer != null) videoPlayer!.pause();
  }

  @override
  void dispose() {
    if (videoPlayer != null) videoPlayer!.dispose();
    super.dispose();
  }
}

/// Box that controls both other Boxes and displays basic information of [artwork]
class _ControlBox extends StatefulWidget {
  /// The [Artwork] whose media are going to be displayed
  final Artwork artwork;

  /// shows if an audio guide is available for [artwork]
  final bool hasAudio;

  /// shows if a video is available for [artwork]
  final bool hasVideo;

  /// signals the VideoImageBox if a video(false) or image(true) should be displayed
  final ValueNotifier<bool> valueVideoImageNotifier;

  /// signals the AudioTextBox if a text(0) or the audio guide(1) or nothing(2) should be displayed
  final ValueNotifier<int> valueAudioTextNotifier;

  /// The constraints of the screen space
  final BoxConstraints constraints;

  _ControlBox(
      this.hasAudio,
      this.hasVideo,
      this.artwork,
      this.valueVideoImageNotifier,
      this.valueAudioTextNotifier,
      this.constraints);
  @override
  _ControlBoxState createState() => _ControlBoxState();
}

enum View { text, audio, video }

class _ControlBoxState extends State<_ControlBox> {
  /// Shows which of the three right navigation buttons is pressed
  View view = View.text;

  /// Size of the icons derived from the app settings
  late double iconSize;

  /// Audio Player that controls the audio guide from the artwork
  AudioPlayer? audioPlayer;

  /// The available height for this area
  double? height;

  @override
  Widget build(BuildContext context) {
    height = widget.constraints.maxHeight * (1 - 760 / 1920);
    final appSetting = Provider.of<AppSetting>(context);
    final modelCache = Provider.of<ModelCache>(context);
    final locale = Provider.of<LocaleProvider>(context);
    if (widget.hasAudio) _initAudio(modelCache, locale, appSetting);
    if (view != View.audio) _pauseAudio();
    iconSize = appSetting.iconSize;
    return Container(
      height: view == View.audio ? height : null,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _controllerBar(height!),
          if (view == View.text)
            _TextBox(widget.artwork, widget.valueAudioTextNotifier)
        ],
      ),
    );
  }

  /// Bar for controlling the views of all boxes in the audio guide
  Widget _controllerBar(double height) {
    final appSetting = Provider.of<AppSetting>(context);
    iconSize = appSetting.iconSize * 1.4;
    final screenSize = MediaQuery.of(context).size;
    return Container(
      height: view != View.text
          ? widget.constraints.maxHeight * (1 - 760 / 1920)
          : null,
      color: Theme.of(context).cardColor,
      child: Padding(
        padding: EdgeInsets.fromLTRB(0, screenSize.width * 0.037, 0, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: iconSize * 1.2,
              child: Padding(
                padding: EdgeInsets.fromLTRB(
                    screenSize.width * 0.037, 0, screenSize.width * 0.037, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        _favoriteButton(),
                        widget.artwork.longitude != null &&
                                widget.artwork.latitude != null
                            ? _mapButton()
                            : SizedBox.shrink(),
                      ],
                    ),
                    widget.hasAudio || widget.hasVideo
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              _textButton(),
                              widget.hasAudio
                                  ? _audioButton()
                                  : SizedBox.shrink(),
                              widget.hasVideo
                                  ? _videoButton()
                                  : SizedBox.shrink()
                            ],
                          )
                        : SizedBox.shrink()
                  ],
                ),
              ),
            ),
            Container(
                height: view == View.audio
                    ? height - iconSize * 1.2 - 120
                    : view == View.video
                        ? height - iconSize * 1.2 - screenSize.width * 0.037
                        : null,
                child: _shortInfo(view)),
            if (view != View.text) Spacer(),
            if (view == View.audio) _audioDisplay()
          ],
        ),
      ),
    );
  }

  /// Displays the audio guide and its control buttons
  Widget _audioDisplay() {
    return Column(
      children: [
        StreamBuilder<Duration?>(
          stream: audioPlayer!.durationStream,
          builder: (context, snapshot) {
            final duration = snapshot.data ?? Duration.zero;
            return StreamBuilder<PositionData>(
              stream: Rx.combineLatest2<Duration, Duration, PositionData>(
                  audioPlayer!.positionStream,
                  audioPlayer!.bufferedPositionStream,
                  (position, bufferedPosition) =>
                      PositionData(position, bufferedPosition)),
              builder: (context, snapshot) {
                final positionData =
                    snapshot.data ?? PositionData(Duration.zero, Duration.zero);
                var position = positionData.position;
                if (position > duration) {
                  position = duration;
                }
                var bufferedPosition = positionData.bufferedPosition;
                if (bufferedPosition > duration) {
                  bufferedPosition = duration;
                }
                return SeekBar(
                  duration: duration,
                  position: position,
                  bufferedPosition: bufferedPosition,
                  onChangeEnd: (newPosition) {
                    audioPlayer!.seek(newPosition);
                  },
                );
              },
            );
          },
        ),
        ControlButtons(player: audioPlayer!, displayPausePlay: true)
      ],
    );
  }

  /// pauses audio if the player is initialized
  void _pauseAudio() {
    if (audioPlayer != null) audioPlayer!.pause();
  }

  /// initializes the audio player with the url from artwork
  void _initAudio(ModelCache modelCache, LocaleProvider locale,
      AppSetting appSetting) async {
    String languageCode = appSetting.language ?? L10n.defaultLang;
    if (audioPlayer == null) {
      audioPlayer = AudioPlayer();
      audioPlayer!.setSpeed(1.0);
      try {
        modelCache
            .getMediaCached(
                widget.artwork.getFieldValue("audio_guide", languageCode),
                context)
            .then((media) => _setUrlAudio(media.url));
      } catch (e) {}
    }
  }

  /// sets the url of an audio player
  void _setUrlAudio(url) async {
    try {
      await audioPlayer!.setUrl(url, preload: true);
    } catch (e) {
      print("Could not get Audio file from $url: $e");
    }
  }

  /// removes the audio player from the build tree
  @override
  void dispose() {
    if (audioPlayer != null) audioPlayer!.dispose();
    super.dispose();
  }

  /// Displays basic information about the artwork
  Widget _shortInfo(View view) {
    final modelCache = Provider.of<ModelCache>(context);
    final screenSize = MediaQuery.of(context).size;
    Artist? artist = modelCache.artistList[widget.artwork.artistId];

    String birthDeathText = "";
    if (artist != null) {
      birthDeathText += artist.name;
      if (artist.yearBirth != null) {
        if (artist.yearDeath != null) {
          birthDeathText += " (* ${artist.yearBirth} – † ${artist.yearDeath})";
        } else {
          birthDeathText += " (* ${artist.yearBirth})";
        }
      } else if (artist.yearDeath != null) {
        birthDeathText += " († ${artist.yearDeath})";
      }
    }
    AppSetting appSetting = Provider.of<AppSetting>(context);
    String languageCode = appSetting.language ?? L10n.defaultLang;
    String artworkData = "";
    if (widget.artwork.getFieldValue("location", languageCode) != "") {
      artworkData +=
          "\n" + widget.artwork.getFieldValue("location", languageCode);
    }
    if (widget.artwork.address != null && widget.artwork.address != "") {
      artworkData += "\n" + widget.artwork.address!;
    }
    if (artworkData != "") artworkData += "\n";
    if (widget.artwork.getFieldValue("type", languageCode) != "") {
      artworkData += "\n" + widget.artwork.getFieldValue("type", languageCode);
    }
    if (widget.artwork.getFieldValue("canvas", languageCode) != "") {
      artworkData +=
          "\n" + widget.artwork.getFieldValue("canvas", languageCode);
    }
    if (widget.artwork.getFieldValue("acquired", languageCode) != "") {
      artworkData +=
          "\n" + widget.artwork.getFieldValue("acquired", languageCode);
    }

    List<Widget> widgets = [
      /// Artwork Title
      Align(
          child: Text(
            widget.artwork.getFieldValue("name", languageCode) +
                (widget.artwork.year != null && widget.artwork.year != ""
                    ? " (${widget.artwork.year})"
                    : ""),
            textAlign: TextAlign.left,
            style: Theme.of(context).textTheme.headline1!.copyWith(
                  fontWeight: FontWeight.bold,
                ),
          ),
          alignment: Alignment.topLeft),

      /// Artist title
      Align(
        child: Text(
          birthDeathText,
          textAlign: TextAlign.left,
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                fontWeight: FontWeight.w600,
              ),
        ),
        alignment: Alignment.topLeft,
      ),

      /// Artwork data
      Align(
        child: Text(
          artworkData,
          maxLines: view == View.audio ? 7 : null,
          textAlign: TextAlign.left,
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                fontWeight: FontWeight.normal,
              ),
        ),
        alignment: Alignment.topLeft,
      ),
    ];

    if (view != View.text) {
      return ListView(
          padding: EdgeInsets.fromLTRB(screenSize.width * 0.037, 0,
              screenSize.width * 0.037, screenSize.height * 0.037 / 4),
          shrinkWrap: true,
          children: widgets);
    } else {
      return Container(
          child: Padding(
              padding: EdgeInsets.fromLTRB(screenSize.width * 0.037, 0,
                  screenSize.width * 0.037, screenSize.height * 0.037 / 4),
              child: Column(children: widgets)));
    }
  }

  /// Buttons
  /// Responsible for handling the adding and removing of the artwork from the favorite list
  IconButton _favoriteButton() {
    final modelCache = Provider.of<ModelCache>(context);
    final appSetting = Provider.of<AppSetting>(context);
    return IconButton(
      padding: EdgeInsets.all(0),
      alignment: Alignment.centerLeft,
      onPressed: () {
        setState(() {
          if (modelCache.favoriteArtworkList.contains(widget.artwork.id)) {
            FavoritesHandler.removeFavorite(widget.artwork.id, context);
          } else {
            FavoritesHandler.addFavorite(widget.artwork.id, context);
          }
        });
      },
      icon: Icon(Kunstforum.circleFavorite),
      color: modelCache.favoriteArtworkList.contains(widget.artwork.id)
          ? appSetting.primaryColor
          : null,
      iconSize: iconSize * 1.1,
    );
  }

  /// Opens a map screen with the artworks placed at their locations as a pin
  IconButton _mapButton() {
    // also returns to text view to close all audio/video

    return IconButton(
      padding: EdgeInsets.all(0),
      alignment: Alignment.centerLeft,
      onPressed: () {
        if (view != View.text) {
          if (view == View.video) {
            //notifies the VideoImageBox
            widget.valueVideoImageNotifier.value = true;
          }
          //notifies the AudioTextBox
          widget.valueAudioTextNotifier.value = 0;
          setState(() {
            view = View.text;
          });
        }
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => MapPopUpScreen(initArtwork: widget.artwork)));
      },
      icon: Icon(Kunstforum.circlePin),
      iconSize: iconSize * 1.1,
    );
  }

  /// Button to switch the view to text view (image above, long description below)
  IconButton _textButton() {
    final appSetting = Provider.of<AppSetting>(context);
    return IconButton(
      padding: EdgeInsets.all(0),
      alignment: Alignment.centerRight,
      onPressed: () {
        // changing state is only relevant if states differ
        if (view != View.text) {
          if (view == View.video) {
            //notifies the VideoImageBox
            widget.valueVideoImageNotifier.value = true;
          }
          //notifies the AudioTextBox
          widget.valueAudioTextNotifier.value = 0;
          setState(() {
            view = View.text;
          });
        }
      },
      icon: Icon(Kunstforum.circleText),
      iconSize: iconSize * 1.1,
      color: view == View.text ? appSetting.primaryColor : null,
    );
  }

  /// Button to switch the view to audio view (image above, audio controller below)
  IconButton _audioButton() {
    final appSetting = Provider.of<AppSetting>(context);
    return IconButton(
      padding: EdgeInsets.all(0),
      alignment: Alignment.centerRight,
      onPressed: () {
        // changing state is only relevant if states differ
        if (view != View.audio) {
          if (view == View.video) {
            //notifies the VideoImageBox
            widget.valueVideoImageNotifier.value = true;
          }
          //notifies the AudioTextBox
          widget.valueAudioTextNotifier.value = 1;
          setState(() {
            view = View.audio;
          });
        }
      },
      icon: Icon(Kunstforum.circleAudio),
      iconSize: iconSize * 1.1,
      color: view == View.audio ? appSetting.primaryColor : null,
    );
  }

  /// Button to switch the view to video view (video above, nothing below)
  IconButton _videoButton() {
    final appSetting = Provider.of<AppSetting>(context);
    return IconButton(
      padding: EdgeInsets.all(0),
      alignment: Alignment.centerRight,
      onPressed: () {
        // changing state is only relevant if states differ
        if (view != View.video) {
          widget.valueVideoImageNotifier.value = false;
          //notifies the AudioTextBox
          widget.valueAudioTextNotifier.value = 2;
          setState(() {
            view = View.video;
          });
        }
      },
      icon: Icon(Kunstforum.circleVideo),
      iconSize: iconSize * 1.1,
      color: view == View.video ? appSetting.primaryColor : null,
    );
  }
}

/// Box that displays either the description or the audio controller of [artwork] or nothing
class _TextBox extends StatefulWidget {
  /// Tells this box which screen is to be shown
  final ValueNotifier<int> valueAudioTextNotifier;

  /// The displayed artwork of the audio guide screen
  final Artwork artwork;

  _TextBox(this.artwork, this.valueAudioTextNotifier);
  @override
  _TextBoxState createState() => _TextBoxState();
}

class _TextBoxState extends State<_TextBox> {
  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final modelCache = Provider.of<ModelCache>(context);
    Artist? artist = modelCache.artistList[widget.artwork.artistId];
    AppSetting appSetting = Provider.of<AppSetting>(context);
    String languageCode = appSetting.language ?? L10n.defaultLang;
    AppLocalizations localizations = AppLocalizations.of(context)!;
    EdgeInsets padding = EdgeInsets.fromLTRB(
        screenSize.width * 0.037,
        screenSize.height * 0.0335 / 4,
        screenSize.width * 0.037,
        screenSize.height * 0.0335 / 4);
    return Column(
      children: [
        ColoredBox(
          color: Theme.of(context).brightness == Brightness.dark
              ? Color(0xFF818284)
              : Color(0xFFF7F7F7),
          child: Padding(
            padding: padding,
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                widget.artwork.getFieldValue("info_text", languageCode),
                textAlign: TextAlign.left,
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      fontWeight: FontWeight.normal,
                    ),
              ),
            ),
          ),
        ),
        artist != null &&
                artist.getFieldValue("info_text", languageCode) != null &&
                artist.getFieldValue("info_text", languageCode) != ""
            ? ColoredBox(
                color: Theme.of(context).cardColor,
                child: Padding(
                  padding: padding,
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      artist.getFieldValue("info_text", languageCode),
                      textAlign: TextAlign.left,
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontWeight: FontWeight.normal,
                          ),
                    ),
                  ),
                ),
              )
            : SizedBox.shrink(),
        widget.artwork.linkedArtwork != null
            ? AppBar(
                toolbarHeight: 100,
                foregroundColor: Colors.white,
                backgroundColor: appSetting.primaryColor,
                leading: IconButton(
                  padding: EdgeInsets.fromLTRB(15, 10, 15, 15),
                  iconSize: appSetting.iconSize * 2,
                  icon: modelCache.campusExhibition.artworkList
                          .contains(widget.artwork.linkedArtwork)
                      ? Icon(Kunstforum.circleMap)
                      : Icon(Kunstforum.circleAthene),
                  disabledColor: Colors.white,
                  onPressed: null,
                ),
                flexibleSpace: Padding(
                  padding: EdgeInsets.fromLTRB(80, 15, 15, 15),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: RichText(
                        text: TextSpan(
                            children: widget.artwork.getFieldValue(
                                        "linked_artwork_description",
                                        languageCode) !=
                                    null
                                ? [
                                    TextSpan(
                                      text: widget.artwork.getFieldValue(
                                          "linked_artwork_description",
                                          languageCode),
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1!
                                          .copyWith(
                                            fontWeight: FontWeight.normal,
                                            color: Colors.white,
                                          ),
                                    ),
                                    TextSpan(
                                        text: localizations.linkToArtwork,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1!
                                            .copyWith(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                            ),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () {
                                            Navigator.of(context).pushNamed(
                                              RouteGenerator
                                                  .audioGuideScreenRouteName,
                                              arguments: {
                                                "artwork":
                                                    modelCache.artworkList[
                                                        widget.artwork
                                                            .linkedArtwork!]
                                              },
                                            );
                                          }),
                                  ]
                                : [
                                    TextSpan(
                                        text: localizations.linkToArtwork,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1!
                                            .copyWith(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                            ),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () {
                                            Navigator.of(context).pushNamed(
                                              RouteGenerator
                                                  .audioGuideScreenRouteName,
                                              arguments: {
                                                "artwork":
                                                    modelCache.artworkList[
                                                        widget.artwork
                                                            .linkedArtwork!]
                                              },
                                            );
                                          }),
                                  ])),
                  ),
                ),
              )
            : SizedBox.shrink(),
      ],
    );
  }
}
