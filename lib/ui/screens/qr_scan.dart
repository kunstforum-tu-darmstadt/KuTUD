import 'dart:io';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:collection/collection.dart';
import 'package:flutter/services.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/artwork_card.dart';
import 'package:kunstforum_tu_darmstadt/api/model_cache.dart';
import 'package:kunstforum_tu_darmstadt/config/route_generator.dart';
import 'package:kunstforum_tu_darmstadt/models/artwork.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/keyboard_visibility_builder.dart';

/// A [StatefulWidget] handling a QR Code Scan
/// and displaying an [ArtworkCard] as scan result
class QRScan extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => QRScanState();
}

/// A State for [QRScan] changing the info container based on
/// the fetched scan result
class QRScanState extends State<QRScan> {
  /// ----------------------- Class Attributes -------------------------

  /// An instance of [QRViewController] to control the QR-Code scanner
  QRViewController? controller;

  /// A key to for [QRView]
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  /// Is the SnackBar visible?
  bool _snackBarVisible = false;

  /// Is the QR-Scan active?
  bool _qrScanActive = false;

  /// ----------------------- Main Widget -------------------------

  @override
  Widget build(BuildContext context) {
    final appSetting = Provider.of<AppSetting>(context);

    return KeyboardVisibilityBuilder(
        builder: (context, child, isKeyboardVisible) {
      //makes sure that the keyboard from the search_screen does not interfere with the camera display.
      if (isKeyboardVisible) {
        SystemChannels.textInput.invokeMethod('TextInput.hide');
        return SizedBox.shrink();
      }
      if (!_qrScanActive) {
        return Center(
          child: ElevatedButton.icon(
            style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(appSetting.primaryColor)),
            onPressed: () {
              setState(
                () {
                  _qrScanActive = true;
                },
              );
            },
            icon: Icon(
              Icons.qr_code_scanner_sharp,
              color: appSetting.primaryColor.computeLuminance() < 0.6
                  ? Colors.white
                  : Colors.black,
            ),
            label: AutoSizeText(AppLocalizations.of(context)!.pressToScan,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline2!.copyWith(
                      color: appSetting.primaryColor.computeLuminance() < 0.6
                          ? Colors.white
                          : Colors.black,
                    )),
          ),
        );
      }
      // For this example we check how wide the device is and change the scanArea and overlay accordingly.
      var scanArea = MediaQuery.of(context).size.width / 6 * 5;
      return Column(
        children: <Widget>[
          Expanded(
              flex: 6,
              child: QRView(
                key: qrKey,
                onQRViewCreated: _onQRViewCreated,
                overlay: QrScannerOverlayShape(
                    borderColor: Colors.red,
                    borderRadius: 10,
                    borderLength: 30,
                    borderWidth: 10,
                    cutOutSize: scanArea),
              ))
        ],
      );
    });
  }

  /// ----------------------- Assisting Functions -------------------------

  /// Read the scannedDataStream into the result attribute
  void _onQRViewCreated(QRViewController controller) {
    ModelCache modelCache = Provider.of<ModelCache>(context, listen: false);
    setState(() {
      this.controller = controller;
    });
    Barcode? result;
    controller.scannedDataStream.listen((scanData) async {
      // prevent the scanner from pushing the detail screen multiple times
      if (result == scanData) return;
      setState(() {
        result = scanData;
      });
      final query =
          Uri.decodeComponent(result!.code!).split(RegExp(r'[#/=]')).last;
      Artwork? foundArtwork;
      foundArtwork = modelCache.artworkList.values.toList().firstWhereOrNull(
          (element) => element.identifier.toLowerCase() == query.toLowerCase());

      if (foundArtwork != null) {
        this.controller!.pauseCamera();
        Navigator.of(context).pushNamed(
            RouteGenerator.audioGuideScreenRouteName,
            // .then() runs, when the pushed detail screen is disposed
            arguments: {
              "artwork": foundArtwork
            }).then((value) => this.controller!.resumeCamera());
      } else {
        showSnackBar();
      }
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  showSnackBar() async {
    if (_snackBarVisible) return;
    _snackBarVisible = true;
    await ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(
          content: Text(
            AppLocalizations.of(context)!.artworkNotFound,
            textAlign: TextAlign.center,
          ),
          duration: Duration(milliseconds: 1500),
          behavior: SnackBarBehavior.floating,
        ))
        .closed;
    _snackBarVisible = false;
  }

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller?.pauseCamera();
    }
    controller?.resumeCamera();
  }
}
