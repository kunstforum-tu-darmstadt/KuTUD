import 'dart:convert';
import 'dart:core';
import 'dart:async';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/display_refresh_screen.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/settings/favorite_settings.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/widgets.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';
import 'package:kunstforum_tu_darmstadt/config/route_generator.dart';
import 'package:kunstforum_tu_darmstadt/api/model_cache.dart';

/// Loading Screen Widget that updates the screen once all initializer methods
/// are called.
class LoadingScreen extends StatefulWidget {
  final bool initSettings;

  LoadingScreen({this.initSettings = true});

  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  /// ----------------------- Attributes -------------------------

  /// A  flag indicated whether the app has internet access
  bool _isConnected = false;

  /// A  flag indicated the [SchedulerBinding] callback is set
  bool _initCallBack = true;

  /// A  flag indicated whether the [AppSetting] is initialized
  bool _initSetting = true;

  /// An instance of [SharedPreferences]
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  /// An instance of [AppSetting]
  late AppSetting _appSetting;

  /// ----------------------- Main Widget -------------------------
  @override
  void initState() {
    super.initState();
    checkConnectivity().then((e) => _runInitTasks());
  }

  @override
  Widget build(BuildContext context) {
    _appSetting = Provider.of<AppSetting>(context);
    if (_initSetting && widget.initSettings) {
      _prefs.then((SharedPreferences prefs) {
        bool settingSaved = prefs.getBool("settingSaved") ?? false;
        if (settingSaved) {
          _appSetting.fromJson(jsonDecode(prefs.getString("appSetting")!));
        } else {
          _appSetting.initAppSetting(context);
        }
        FavoritesHandler.loadFavorites(context);
      });
      _initSetting = false;
    }
    return FutureBuilder(
      future: checkConnectivity(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Container(
              color: Theme.of(context).brightness != Brightness.dark
                  ? Colors.white
                  : Colors.black);
        }
        return _isConnected
            ? DisplayLoadingScreen()
            : DisplayRefreshScreen(onReload: () {
                setState(() {});
              });
      },
    );
  }

  /// ----------------------- Assisting Widgets -------------------------

  // This method checks wether the device is connected with the internet or not
  Future<void> checkConnectivity() async {
    _isConnected =
        (await Connectivity().checkConnectivity()) != ConnectivityResult.none;
  }

  /// ----------------------- Assisting functions -------------------------

  /// This method calls the initializers and once they complete redirects to
  /// the widget provided in navigateAfterInit
  Future<void> _runInitTasks() async {
    bool success = false;

    /// Run each initializer method sequentially
    Future.microtask(() async {
      final modelCache = Provider.of<ModelCache>(context, listen: false);
      if (_isConnected) {
        try {
          await modelCache.getAll(context).then((value) {
            _initCallBack = false;
            success = true;
          });
        } catch (e) {
          if (!success) setState(() {});
        }
      }
    }).whenComplete(() {
      // When all the initializers has been called and terminated their
      // execution. The screen is navigated to the home screen
      if (_isConnected && success) {
        Navigator.of(context)
            .pushReplacementNamed(RouteGenerator.frameScreenRouteName);
      }
    });
  }
}

/// The actual widget which is shown while the tasks of _runInitTasks are running.
class DisplayLoadingScreen extends StatelessWidget {
  const DisplayLoadingScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).brightness != Brightness.dark
          ? Colors.white
          : Colors.black,
      child: Column(
        children: [
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Image.asset(
                Theme.of(context).brightness != Brightness.dark
                    ? 'assets/images/introduction/kutud.jpg'
                    : 'assets/images/introduction/kutud_white.jpg',
                fit: BoxFit.contain,
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: CircularProgressIndicator(
                color: Theme.of(context).brightness == Brightness.dark
                    ? Colors.white
                    : Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
