import 'dart:async';
import 'dart:math';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_cluster/flutter_map_marker_cluster.dart';
import 'package:kunstforum_tu_darmstadt/config/themes.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/custom_app_bar.dart';
import 'package:provider/provider.dart';
import 'package:latlong2/latlong.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:map_launcher/map_launcher.dart' as maplauncher;
import 'package:kunstforum_tu_darmstadt/config/kunstforum_icons.dart';
import 'package:kunstforum_tu_darmstadt/models/artwork.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/artwork_card.dart';
import 'package:kunstforum_tu_darmstadt/api/model_cache.dart';
import 'package:flutter_map_location_marker/flutter_map_location_marker.dart';
import 'package:location/location.dart';

import '../../lang/l10n.dart'; // is only used to handle location permissions and services

enum MapStatus {
  initial,
  blocked,
  follow,
  unfollow,
}

/// A [StatefulWidget] that displays an openstreetmap
/// with the positions of artworks and exhibitions
class MapScreen extends StatefulWidget {
  final Artwork? initArtwork;

  final double? initZoom;

  MapScreen({this.initArtwork, this.initZoom});

  @override
  _MapScreenState createState() => _MapScreenState(initArtwork, initZoom);
}

/// A State for [MapScreen] changing the displayed page based on
/// the current position of the user and the artworks/exhibitions
class _MapScreenState extends State<MapScreen> {
  /// An instance of [AppSetting]
  AppSetting? appSetting;

  /// The centering position (Darmstadt Mitte)
  LatLng _centeringPosition;

  final double _minZoom = 11.85;
  final double _maxZoom = 18.47;

  /// The starting zoom
  final double _startingZoom;

  /// A Controller to programmatically interact with FlutterMap
  final MapController _mapController = MapController();

  PersistentBottomSheetController<dynamic>? _modalController;

  /// Markers for showing the artworks/exhibitions
  final Map<Marker, List<Artwork>> _markers = <Marker, List<Artwork>>{};

  /// The [Marker] of the [initArtwork]
  Marker? _initMarker;

  /// The initial artwork passed by the Navigator as argument
  Artwork? initArtwork;

  /// The orientation of the map for cluster markers
  double angle = 0;

  /// Indicates the location status of the map
  final ValueNotifier<MapStatus> _status =
      ValueNotifier<MapStatus>(MapStatus.initial);

  /// Indicates that the user has moved the map while in follow mode
  final ValueNotifier<bool> _mapMoved = ValueNotifier<bool>(false);

  _MapScreenState._(
      this.initArtwork, this._centeringPosition, this._startingZoom);

  factory _MapScreenState(Artwork? artwork, double? initZoom) {
    LatLng initPosition = LatLng(49.875, 8.658);
    double startZoom = 15;
    if (artwork != null &&
        artwork.latitude != null &&
        artwork.longitude != null) {
      initPosition = LatLng(artwork.latitude!, artwork.longitude!);
    }
    if (initZoom != null) {
      startZoom = initZoom;
    }
    return _MapScreenState._(artwork, initPosition, startZoom);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _modalController?.close();
    });
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var localization = AppLocalizations.of(context)!;
    appSetting = Provider.of<AppSetting>(context);
    final modelCache = Provider.of<ModelCache>(context);
    getPositions(modelCache.artworkList.values
        .toList()); // retrieve the artwork's positions
    return Scaffold(
      body: Stack(children: [
        Container(
            child: ValueListenableBuilder<MapStatus>(
          valueListenable: _status,
          builder: (context, value, child) {
            return getMap(value);
          },
        )),
        Positioned(
          child: _MapButton(_status, _mapMoved),
          top: 85,
          right: 20,
        ),
        Positioned(
          child: FloatingActionButton(
              // This heroTag fixes an exception that is thrown,
              // if two objects in the widget tree have the same heroTag.
              // Setting the heroTag here manually prevents this.
              heroTag: "compassButton",
              backgroundColor: Provider.of<AppSetting>(context).primaryColor,
              onPressed: () {
                safeRotate(rotation: 0);
              },
              child: StreamBuilder<MapEventRotate>(
                stream: Stream.castFrom(_mapController.mapEventStream
                    .where((event) => event is MapEventRotate)),
                builder: (BuildContext context,
                    AsyncSnapshot<MapEventRotate> snapshot) {
                  double angle = 0;
                  if (snapshot.hasData && snapshot.data != null) {
                    angle = snapshot.data!.currentRotation / 180 * pi;
                  }
                  return Transform.rotate(
                      angle: angle,
                      child: Align(
                        child: Icon(
                          Kunstforum.northarrow,
                          size: 40,
                          color:
                              appSetting!.primaryColor.computeLuminance() < 0.6
                                  ? Colors.white
                                  : Colors.black,
                        ),
                        alignment: Alignment.center,
                      ));
                },
              )),
          top: 20,
          right: 20,
        ),
      ]),
    );
  }

  /// creates the [FlutterMap] for the screen
  FlutterMap getMap(MapStatus status) {
    // Adds location layer and plugin if needed
    List<MapPlugin> plugins = [MarkerClusterPlugin()];
    LayerOptions locationLayer = MarkerLayerOptions(
      markers: [],
    );
    if (status == MapStatus.follow || status == MapStatus.unfollow) {
      plugins.add(LocationMarkerPlugin(
        centerOnLocationUpdate: status == MapStatus.follow
            ? CenterOnLocationUpdate.always
            : CenterOnLocationUpdate.never,
      ));
      locationLayer = LocationMarkerLayerOptions(
          markerSize: const Size(20, 20),
          accuracyCircleColor: appSetting!.primaryColor.withOpacity(0.1),
          headingSectorColor: appSetting!.primaryColor.withOpacity(0.8),
          headingSectorRadius: 120,
          marker: DefaultLocationMarker(color: appSetting!.primaryColor));
    }
    return FlutterMap(
      mapController: _mapController,
      options: MapOptions(
          onPositionChanged: (_, userOriginated) {
            // unfollows the user, if the map was moved manually
            if (userOriginated && _status.value == MapStatus.follow) {
              _status.value = MapStatus.unfollow;
              _mapMoved.value = !_mapMoved.value;
            }
          },
          center: _centeringPosition,
          enableMultiFingerGestureRace: true,
          rotationThreshold: 11.5,
          pinchZoomThreshold: 0.3,
          minZoom: _minZoom,
          maxZoom: _maxZoom,
          zoom: _startingZoom,
          plugins: plugins),
      layers: [
        TileLayerOptions(
            urlTemplate:
                "https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png",
            subdomains: ['a', 'b']),
        locationLayer,
        MarkerClusterLayerOptions(
          rotateAlignment: AlignmentDirectional.center,
          maxClusterRadius: 50,
          size: Size(40, 40),
          anchor: AnchorPos.align(AnchorAlign.center),
          fitBoundsOptions: FitBoundsOptions(
            padding: EdgeInsets.all(50),
          ),
          showPolygon: false,
          markers: _markers.keys.toList(),
          builder: (context, markers) {
            if (initArtwork != null &&
                !markers.contains(_markers.keys.firstWhere(
                    (element) => _markers[element]!.contains(initArtwork)))) {
              return customizedCluster(
                  markers, status, appSetting!.primaryColor.shade100);
            } else {
              return customizedCluster(
                  markers, status, appSetting!.primaryColor);
            }
          },
        )
      ],
    );
  }

  Widget customizedCluster(
      List<Marker> markers, MapStatus status, Color color) {
    return FloatingActionButton(
      heroTag: "clusterButton",
      child: Text(getArtworksForMarkers(markers).length.toString(),
          style: Theme.of(context).textTheme.button!.copyWith(
              color: appSetting!.primaryColor.computeLuminance() < 0.6
                  ? Colors.white
                  : Colors.black,
              fontWeight: FontWeight.bold),
          maxLines: 1),
      onPressed: () {
        onCustomClusterButtonClick(this, status, markers);
      },
      foregroundColor: appSetting!.primaryColor.computeLuminance() < 0.6
          ? Colors.white
          : Colors.black,
      backgroundColor: color,
    );
  }

  List<Artwork> getArtworksForMarkers(List<Marker> cluster) {
    List<Artwork> artworks = [];
    for (var marker in cluster) {
      if (_markers[marker] != null) {
        for (var artwork in _markers[marker]!) {
          artworks.add(artwork);
        }
      }
    }
    return artworks;
  }

  void onCustomClusterButtonClick(
      _MapScreenState sender, MapStatus status, List<Marker> cluster) {
    const int clusterBreakdownThreshold = 5;

    if (getArtworksForMarkers(cluster).length > clusterBreakdownThreshold &&
        _mapController.zoom < _maxZoom) {
      // zoom in and refine cluster partition
      _mapController.move(
          calculateMarkerPosAverage(cluster), _mapController.zoom * 1.125);
    } else {
      // show list modal with containing artworks
      final entries = generateClusterEntries(cluster);
      final listViewSize =
          appSetting!.cardSize * min<double>(entries.length.toDouble(), 5);
      _modalController = showBottomSheet(
          backgroundColor: Colors.transparent,
          context: context,
          builder: (builder) {
            return Stack(
              children: [
                Container(
                    height: listViewSize.height,
                    color: Colors.transparent,
                    child: ListView(
                      children: entries,
                    )),
                Container(
                  width: 50,
                  height: 50,
                  margin: EdgeInsets.only(
                      right: 5, bottom: listViewSize.height + 5),
                  child: FittedBox(
                    child: FloatingActionButton(
                      elevation: 5,
                      onPressed: () {
                        _modalController?.close();
                      },
                      backgroundColor: Colors.black12,
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.white, width: 5),
                          borderRadius: BorderRadius.circular(100)),
                      child: Icon(
                        Icons.close,
                        size: 40,
                        color: Colors.white,
                      ),
                    ),
                  ),
                )
              ],
              alignment: Alignment.bottomRight,
            );
          });
    }
  }

  List<Widget> generateClusterEntries(List<Marker> cluster) {
    final List<Widget> result = <Widget>[];

    for (var marker in cluster) {
      for (var art in _markers[marker]!) {
        result.add(generateArtCardContainer(marker.point, art));
      }
    }

    return result;
  }

  List<Widget> generateArtworkEntries(LatLng position, List<Artwork> artworks) {
    final List<Widget> result = <Widget>[];

    for (var artwork in artworks) {
      result.add(generateArtCardContainer(position, artwork));
    }
    return result;
  }

  /// Orientates the map to the given position
  /// if no rotation is set, it will stay in position
  void safeRotate({double? rotation}) {
    // No MapEventRotate will be inserted into the MapEvent stream,
    // if the argument of .rotate is the same as the current rotation.
    // To resolve this, two rotations have to be called.
    _mapController.onReady.then((value) {
      _mapController
        ..rotate((rotation ?? _mapController.rotation) + 1)
        ..rotate((rotation ?? _mapController.rotation - 1));
    });
  }

  /// Create markers for artwork positions
  /// based on the position and artwork name
  Marker createMarker(LatLng position, List<Artwork> art) {
    Color markerColor = appSetting!.primaryColor;
    Marker marker;
    if (initArtwork != null) {
      if (!art.contains(initArtwork)) {
        markerColor = appSetting!.primaryColor.shade100;
      }
    }
    if (art.length == 1) {
      Artwork artwork = art[0];
      marker = Marker(
        rotate: true,
        rotateAlignment: AlignmentDirectional.bottomCenter,
        width: 50.0,
        height: 50.0,
        point: position,
        builder: (ctx) => Container(
            child: IconButton(
                iconSize: appSetting!.mapMarkerIconSize,
                icon: Icon(
                  Kunstforum.pin,
                  color: markerColor,
                ),
                onPressed: () => artworkMarkerClick(position, art))),
      );
      if (initArtwork != null && artwork.id == initArtwork!.id) {
        _initMarker = marker;
      }
    } else {
      marker = Marker(
          rotate: true,
          rotateAlignment: AlignmentDirectional.bottomCenter,
          width: 40.0,
          height: 40.0,
          point: position,
          builder: (ctx) => Container(
                  child: FloatingActionButton(
                heroTag: "markerList",
                child: Text(art.length.toString(),
                    style: Theme.of(context).textTheme.button!.copyWith(
                        color: appSetting!.primaryColor.computeLuminance() < 0.6
                            ? Colors.white
                            : Colors.black,
                        fontWeight: FontWeight.bold),
                    maxLines: 1),
                foregroundColor:
                    appSetting!.primaryColor.computeLuminance() < 0.6
                        ? Colors.white
                        : Colors.black,
                backgroundColor: markerColor,
                onPressed: () => artworkMarkerClick(position, art),
              )));
    }
    return marker;
  }

  Container generateArtCardContainer(LatLng position, Artwork artwork) {
    AppSetting appSetting = Provider.of<AppSetting>(context, listen: false);
    String languageCode = appSetting.language ?? L10n.defaultLang;
    final children = [
      ArtworkCard(
        artwork,
      ),
      Container(
          height: 40,
          width: 40,
          margin: EdgeInsets.only(right: 30),
          child: FittedBox(
              child: FloatingActionButton(
            heroTag: artwork.name,
            elevation: 2,
            onPressed: () {
              getMaps(position, artwork.getFieldValue("name", languageCode));
            },
            shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.white, width: 4),
                borderRadius: BorderRadius.circular(40)),
            child: Icon(
              Icons.directions_sharp,
              color: Colors.white,
              size: 28,
            ),
            backgroundColor: Colors.transparent,
          ))),
    ];
    return Container(
        height: appSetting.cardSize.height,
        child: Stack(
          children: children,
          alignment: Alignment.centerRight,
        ));
  }

  /// handles clicking on artwork markers
  void artworkMarkerClick(LatLng position, List<Artwork> artworks) {
    final entries = generateArtworkEntries(position, artworks);
    const double maxListHeight = 400;
    final listViewSize =
        (appSetting!.cardSize * min<double>(entries.length.toDouble(), 5));
    print(listViewSize);

    _modalController = showBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (builder) {
          return Stack(
            children: [
              Container(
                constraints: BoxConstraints(
                    maxHeight: min(maxListHeight, listViewSize.height)),
                child: ListView(
                  children: entries,
                ),
              ),
              Container(
                width: 40,
                height: 40,
                margin: EdgeInsets.only(
                    right: 30,
                    bottom: min(maxListHeight, listViewSize.height) + 5),
                child: FittedBox(
                  child: FloatingActionButton(
                    heroTag: artworks[0].identifier,
                    elevation: 5,
                    onPressed: () {
                      _modalController?.close();
                    },
                    backgroundColor: Colors.black12,
                    shape: RoundedRectangleBorder(
                        side: BorderSide(color: Colors.white, width: 5),
                        borderRadius: BorderRadius.circular(100)),
                    child: Icon(
                      Icons.close,
                      size: 40,
                      color: Colors.white,
                    ),
                  ),
                ),
              )
            ],
            alignment: Alignment.bottomRight,
          );
        });
  }

  /// Extract position and name from list of artworks
  /// and create markers of the positions
  void getPositions(List<Artwork> artworkList) {
    _markers.clear();
    Map<LatLng, List<Artwork>> uniquePositions = {};
    for (Artwork artwork in artworkList) {
      if (artwork.latitude != null && artwork.longitude != null) {
        LatLng position = LatLng(artwork.latitude!, artwork.longitude!);
        if (uniquePositions[position] == null) {
          uniquePositions[position] = [artwork];
        } else {
          uniquePositions[position]!.add(artwork);
        }
      }
    }
    for (LatLng position in uniquePositions.keys) {
      _markers[createMarker(position, uniquePositions[position]!)] =
          uniquePositions[position]!;
    }
  }

  /// Get a list of all map applications from the user's device
  /// and show the artworks' location in the first map application
  void getMaps(LatLng position, String name) async {
    final availableMaps = await maplauncher.MapLauncher.installedMaps;
    await availableMaps.first.showMarker(
      coords: maplauncher.Coords(position.latitude, position.longitude),
      title: name,
    );
  }

  LatLng calculateMarkerPosAverage(List<Marker> marker) {
    double lat = 0;
    double long = 0;
    for (var m in marker) {
      lat += m.point.latitude;
      long += m.point.longitude;
    }

    long /= marker.length;
    lat /= marker.length;
    return LatLng(lat, long);
  }
}

/// Map Button that turns on user location and toggles following
class _MapButton extends StatefulWidget {
  /// Notifies the map, if the location needs to be shown
  final ValueNotifier<MapStatus> _status;

  /// Gets notified, if the user moved the map
  final ValueNotifier<bool> _mapMoved;

  @override
  State<StatefulWidget> createState() => _MapButtonState();

  _MapButton(this._status, this._mapMoved);
}

class _MapButtonState extends State<_MapButton> with WidgetsBindingObserver {
  @override
  Widget build(BuildContext context) {
    final appSetting = Provider.of<AppSetting>(context);
    return ValueListenableBuilder<bool>(
      valueListenable: widget._mapMoved,
      builder: (context, value, _) {
        return FloatingActionButton(
          // This heroTag fixes an exception that is thrown,
          // if two objects in the widget tree have the same heroTag.
          // Setting the heroTag here manually prevents this.
          heroTag: "mapButton",
          onPressed: () {
            setState(() {});
            var status = widget._status.value;
            if (status == MapStatus.initial) {
              permissionHandler(context).then((value) {
                widget._status.value =
                    value ? MapStatus.follow : MapStatus.blocked;
                setState(() {});
              });
            } else if (status != MapStatus.blocked) {
              if (status != MapStatus.follow) permissionHandler(context);
              widget._status.value = status != MapStatus.follow
                  ? MapStatus.follow
                  : MapStatus.unfollow;
              setState(() {});
            } else {
              permissionHandler(context);
            }
          },
          child: Icon(
            Kunstforum.location,
            size: appSetting.iconSize,
            color: appSetting.primaryColor.computeLuminance() < 0.6
                ? Colors.white
                : Colors.black,
          ),
          backgroundColor: widget._status.value == MapStatus.follow
              ? appSetting.primaryColor
              : Colors.grey,
        );
      },
    );
  }

  /// Resets MapStatus to initial if the app was put into background and then resumed
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed &&
        widget._status.value == MapStatus.blocked) {
      setState(() {
        widget._status.value = MapStatus.initial;
      });
    }
  }

  /// Checks all necessary permissions to locate the user on the map
  Future<bool> permissionHandler(BuildContext context) async {
    Location location = Location();

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        ScaffoldMessenger.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(SnackBar(
            content: Text(
              AppLocalizations.of(context)!.locationService,
              textAlign: TextAlign.center,
            ),
            duration: Duration(milliseconds: 1500),
            behavior: SnackBarBehavior.floating,
          )).closed;
        return false;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        ScaffoldMessenger.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(SnackBar(
            content: Text(
              AppLocalizations.of(context)!.locationPermission,
              textAlign: TextAlign.center,
            ),
            duration: Duration(milliseconds: 1500),
            behavior: SnackBarBehavior.floating,
          )).closed;
        return false;
      }
    }

    return true;
  }

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
}
