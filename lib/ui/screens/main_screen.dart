import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/carousel_slider_campus.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/carousel_slider_exhibition.dart';
import 'package:kunstforum_tu_darmstadt/api/model_cache.dart';

/// A [StatelessWidget] that displays the main page of the app.
/// The Screen displays also in its body two [CarouselSliderBox]
class MainScreen extends StatelessWidget {
  /// ----------------------- Main Widget -------------------------

  @override
  Widget build(BuildContext context) {
    final modelCache = Provider.of<ModelCache>(context);
    return OrientationBuilder(
      builder: (context, orientation) {
        return LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            Size boxSize = Size(
                constraints.maxWidth,
                orientation == Orientation.portrait
                    ? constraints.maxHeight / 2
                    : constraints.maxHeight);
            return Column(
              children: [
                CarouselSliderCampus(
                  boxSize: boxSize,
                ),
                CarouselSliderExhibition(
                  exhibitionList: modelCache.exhibitionList.values.toList(),
                  boxSize: boxSize,
                )
              ],
            );
          },
        );
      },
    );
  }
}
