import 'package:flutter/material.dart';
import 'package:kunstforum_tu_darmstadt/models/artwork.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/map_screen.dart';
import 'package:provider/provider.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/custom_app_bar.dart';

/// A [StatelessWidget] that displays a [Scaffold] for the [MapScreen]
/// to be used as a screen that is pushed onto the current screen. erase
class MapPopUpScreen extends StatelessWidget {
  final Artwork? initArtwork;

  const MapPopUpScreen({Key? key, this.initArtwork}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppLocalizations localization = AppLocalizations.of(context)!;
    AppSetting? appSetting = Provider.of<AppSetting>(context);
    return Scaffold(
      appBar: CustomAppBar(
        appSetting: appSetting,
        appBarTitle: localization.location,
      ),
      body: MapScreen(initArtwork: initArtwork, initZoom: 18.47),
    );
  }
}
