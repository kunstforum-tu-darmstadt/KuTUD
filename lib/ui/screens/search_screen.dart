import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/exhibition_artwork_list.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/qr_scan.dart';
import 'package:kunstforum_tu_darmstadt/api/model_cache.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';
import 'package:kunstforum_tu_darmstadt/models/artwork.dart';
import 'package:kunstforum_tu_darmstadt/models/exhibition.dart';

import '../../lang/l10n.dart';

/// A [StatefulWidget] to be able to search for [Artwork]s or [Exhibition]s
/// depending on from where you navigate to it.
class Search extends StatefulWidget {
  final bool searchArtworks;

  const Search({Key? key, this.searchArtworks = true}) : super(key: key);

  @override
  _SearchState createState() => _SearchState();
}

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toUpperCase(),
      selection: newValue.selection,
    );
  }
}

/// A State for [Search] changing the displayed page based on
/// the current query and search Method (via number, via name or via QR-Code)
class _SearchState extends State<Search> with SingleTickerProviderStateMixin {
  /// This variable always contains the current query entered by the user
  String query = '';

  /// If this flag is set, artwork is searched for. Otherwise it is searched for exhibitions.
  late bool searchArtworks;

  /// To get the current index of the TabBar
  late TabController _tabController;

  /// To request the focus on the [TextField] in the [AppBar]
  late FocusNode _focusNode;

  /// To get access to the text that was entered in the [TextField]
  final TextEditingController _textController = TextEditingController();

  @override
  void initState() {
    super.initState();
    searchArtworks = widget.searchArtworks;
    _tabController = TabController(
        length: 3, vsync: this, initialIndex: TabIndices.textSearch);
    _tabController.addListener(_handleTabSelection);
    _focusNode = FocusNode();
  }

  @override
  void dispose() {
    _tabController.dispose();
    _textController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  /// A Listener, which is called whenever the tab is changed.
  /// Note: This method is called twice per tab change. While start switching and when finishing switching
  _handleTabSelection() async {
    if (_tabController.indexIsChanging) return; // To not execute the code twice
    deleteQuery();
    _focusNode.unfocus(); // unfocus the [TextField] to change the keyboard
    setState(() {
      if (currentTabIs(TabIndices.idSearch)) searchArtworks = true;
    }); // updates the state to select the new keyboardType (text vs. number)
  }

  /// This method resets the query and also the text of the [TextField] to the empty string und updates the state
  void deleteQuery() {
    setState(() {
      _textController.text = query = '';
    });
  }

  /// This method returns true if you are int the tab with the given index.
  bool currentTabIs(int compareTab) {
    return compareTab == _tabController.index;
  }

  @override
  Widget build(BuildContext context) {
    final appSetting = Provider.of<AppSetting>(context);

    return Scaffold(
        appBar: AppBar(
          centerTitle: false,
          backgroundColor: Theme.of(context).cardColor,
          title: searchTextField(appSetting, context),
          elevation: 0,
          actions: [
            if (currentTabIs(TabIndices.textSearch))
              IconButton(
                  onPressed: !currentTabIs(TabIndices.qrSearch)
                      ? () {
                          showDialog(
                              context: context,
                              builder: (_) => filterDialog(appSetting));
                        }
                      : null,
                  icon: Icon(
                    Icons.tune,
                    color: Theme.of(context).iconTheme.color,
                  ))
          ],
        ),
        body: GestureDetector(
          onTap: () {
            if (_focusNode.hasFocus) {
              _focusNode.unfocus();
            } else {
              if (!currentTabIs(TabIndices.qrSearch)) {
                _focusNode.requestFocus();
              }
            }
          },
          child: Scaffold(
              backgroundColor: Theme.of(context).cardColor,
              appBar: TabBar(
                indicator: BoxDecoration(color: appSetting.primaryColor),
                controller: _tabController,
                labelColor: appSetting.primaryColor.computeLuminance() < 0.6
                    ? Colors.white
                    : Colors.black,
                unselectedLabelColor:
                    Theme.of(context).textTheme.bodyText1!.color,
                tabs: [
                  Tab(text: AppLocalizations.of(context)!.exhibitNr),
                  Tab(text: AppLocalizations.of(context)!.exhibitInfo),
                  Tab(text: AppLocalizations.of(context)!.scanQR)
                ],
              ),
              body: Container(
                color: Theme.of(context).scaffoldBackgroundColor,
                child: TabBarView(controller: _tabController, children: [
                  body(_tabController.index, appSetting),
                  body(_tabController.index, appSetting),
                  QRScan()
                ]),
              )),
        ));
  }

  /// The [TextField] for the search of artworks or exhibitions
  TextField searchTextField(AppSetting appSetting, BuildContext context) {
    return TextField(
      inputFormatters: [
        if (currentTabIs(TabIndices.idSearch)) UpperCaseTextFormatter(),
        if (currentTabIs(TabIndices.idSearch))
          FilteringTextInputFormatter.allow(
              RegExp(r'^(([A-Z]?[A-Z]?)(([A-F0-9]){0,4}))?$'),
              replacementString: this.query),
      ],
      onTap: () {
        if (currentTabIs(TabIndices.qrSearch)) {
          setState(() {
            _tabController.index = TabIndices.textSearch;
          });
        }
      },
      style: Theme.of(context).textTheme.headline2,
      autofocus: true,
      focusNode: _focusNode,
      cursorColor: appSetting.primaryColor,
      controller: _textController,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: appSetting.primaryColor)),
          hintText: searchArtworks
              ? AppLocalizations.of(context)!.searchForArtworks
              : AppLocalizations.of(context)!.searchForExhibitions,
          suffixIcon: query.isNotEmpty
              ? IconButton(
                  // only show Button if query is not empty
                  // To delete the current query in the TextField
                  onPressed: deleteQuery,
                  icon: Icon(
                    Icons.delete,
                    size: appSetting.iconSize,
                    color: Theme.of(context).iconTheme.color,
                  ))
              : null),
      onChanged: (value) => setState(() {
        query = value;
      }),
    );
  }

  /// This method returns either the list of the artworks or the exhibitions filtered by the query depending on the parameter [searchArtworks]
  Widget body(int index, AppSetting appSetting) {
    ModelCache modelCache = Provider.of<ModelCache>(context);
    // To not load all artworks / exhibitions to take care of the device's performance
    if (query.isEmpty) {
      return infoText(AppLocalizations.of(context)!.emptyQuery, appSetting);
    }
    List<dynamic> filteredList = getFilteredList(modelCache, index);
    if (filteredList.isEmpty) {
      return infoText(AppLocalizations.of(context)!.noMatch, appSetting);
    }
    return searchArtworks
        ? Padding(
            padding: const EdgeInsets.only(top: 10),
            child: ArtworkList(filteredList as List<Artwork>),
          )
        : Padding(
            padding: const EdgeInsets.only(top: 10),
            child: ExhibitionList(filteredList as List<Exhibition>),
          );
  }

  /// This method returns a [Widget] to show the given information text
  Widget infoText(String text, AppSetting appSetting) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: AutoSizeText(text,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline2),
      ),
    );
  }

  /// This method returns the dialog in which you can choose wether you want to search for [Artwork]s or [Exhibition]s
  Widget filterDialog(AppSetting appSetting) {
    return AlertDialog(
      title: Text(
        AppLocalizations.of(context)!.searchFor,
        style: Theme.of(context).textTheme.headline2,
      ),
      content: Card(
        elevation: 0,
        color: Colors.transparent,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            RadioListTile<bool>(
                title: Text(AppLocalizations.of(context)!.artwork,
                    style: Theme.of(context).textTheme.headline2),
                activeColor: appSetting.primaryColor,
                value: true,
                groupValue: searchArtworks,
                onChanged: (_) => changeFilter(context, true)),
            RadioListTile<bool>(
              title: Text(AppLocalizations.of(context)!.exhibition,
                  style: Theme.of(context).textTheme.headline2),
              activeColor: appSetting.primaryColor,
              value: false,
              groupValue: searchArtworks,
              onChanged: !currentTabIs(TabIndices.idSearch)
                  ? (_) => changeFilter(context, false)
                  : null,
            )
          ],
        ),
      ),
    );
  }

  /// Sets the property [changeTo] to the given value und pops the filter dialog
  void changeFilter(BuildContext context, bool changeTo) {
    setState(() {
      searchArtworks = changeTo;
    });
    Navigator.pop(context);
  }

  /// This method filters the list
  List<dynamic> getFilteredList(ModelCache modelCache, int index) {
    AppSetting appSetting = Provider.of<AppSetting>(context);
    String languageCode = appSetting.language ?? L10n.defaultLang;
    String queryInUpperCase = query.toUpperCase();
    if (index == TabIndices.idSearch) {
      return modelCache.artworkList.values
          .where((element) => query.isNotEmpty
              ? element.identifier.toUpperCase().contains(queryInUpperCase)
              : true)
          .toList();
    } else {
      if (searchArtworks) {
        return modelCache.artworkList.values
            .where((element) =>
                element
                    .getFieldValue("name", languageCode)
                    .toUpperCase()
                    .contains(queryInUpperCase) ||
                modelCache.artistList[element.artistId]!.name
                    .toUpperCase()
                    .contains(queryInUpperCase))
            .toList();
      } else {
        return modelCache.exhibitionList.values
            .where((element) => element
                .getFieldValue("title", languageCode)
                .toUpperCase()
                .contains(queryInUpperCase))
            .toList();
      }
    }
  }
}

class TabIndices {
  static const int idSearch = 0;
  static const int textSearch = 1;
  static const int qrSearch = 2;
}
