import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:kunstforum_tu_darmstadt/api/model_cache.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';
import 'package:kunstforum_tu_darmstadt/models/exhibition.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/custom_app_bar.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/artwork_card.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/image_text_overlay.dart';

import '../../lang/l10n.dart';

/// A [StatelessWidget] that displays the exhibition [title] (and [exhibitionImage] if available) and the [artworksList]
class ExhibitionScreen extends StatelessWidget {
  //  Attributes

  /// The displayed [Exhibition]
  final Exhibition exhibition;

  ExhibitionScreen({required this.exhibition});

  @override
  Widget build(BuildContext context) {
    var localization = AppLocalizations.of(context)!;
    final ModelCache modelCache = Provider.of<ModelCache>(context);
    final appSetting = Provider.of<AppSetting>(context);
    // Dynamic variable, adjust to actual screen height
    final coverImageHeight = MediaQuery.of(context).size.height * (7 / 17);

    return Scaffold(
      appBar: CustomAppBar(
        appSetting: appSetting,
        appBarTitle: localization.exhibition,
      ),
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            expandedHeight: coverImageHeight,
            automaticallyImplyLeading: false,
            flexibleSpace: FlexibleSpaceBar(
              background: addExhibitionCover(modelCache, context),
            ),
          ),
          bottomBox(appSetting, modelCache, context),
        ],
      ),
    );
  }

  /// ----------------------- Assisting Widgets -------------------------

  /// Creates [exhibition] image with a title
  Widget addExhibitionCover(ModelCache modelCache, BuildContext context) {
    // String of coverImage of Exhibition as key for appropriate entry in mediaList to get the url String of it
    AppSetting appSetting = Provider.of<AppSetting>(context);
    String languageCode = appSetting.language ?? L10n.defaultLang;
    return ImageTextOverlay(
      mediaUUID: exhibition.coverImage,
      title: exhibition.getFieldValue("title", languageCode),
    );
  }

  /// Builds the bottomBox with [ListView] in a SliverToBoxAdapter to be compatible with the parent widget
  Widget bottomBox(
      AppSetting appSetting, ModelCache modelCache, BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        padding: EdgeInsets.only(top: 10, bottom: 10),
        child: buildListView(modelCache),
      ),
    );
  }

  /// Builds a [ListView] with [ArtworkCard] as element for each [filteredList] elements.
  buildListView(ModelCache modelCache) {
    return ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        primary: false,
        itemCount: exhibition.artworkList.length,
        itemBuilder: (BuildContext context, int i) {
          return ArtworkCard(
            modelCache.artworkList[exhibition.artworkList[i]]!,
          );
        });
  }
}
