import 'package:flutter/material.dart';
import 'package:kunstforum_tu_darmstadt/config/kunstforum_icons.dart';
import 'package:kunstforum_tu_darmstadt/models/exhibition.dart';
import 'package:provider/provider.dart';
import 'package:kunstforum_tu_darmstadt/config/route_generator.dart';
import 'package:kunstforum_tu_darmstadt/api/model_cache.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/image_text_overlay.dart';

import '../../config/app_setting.dart';
import '../../lang/l10n.dart';

/// A [StatelessWidget] that displays the exhibitions page of the app.
/// The Screen displays the list of all exhibitions fetched from backend
class ExhibitionsScreen extends StatefulWidget {
  @override
  State<ExhibitionsScreen> createState() => _ExhibitionsScreenState();
}

class _ExhibitionsScreenState extends State<ExhibitionsScreen> {
  final ScrollController scrollController = ScrollController();
  bool reachedTop = true;
  bool reachedEnd = false;

  @override
  void initState() {
    super.initState();
    scrollController.addListener(() {
      if (scrollController.position.pixels <= 0) {
        reachedTop = true;
      } else if (scrollController.position.pixels >=
          scrollController.position.maxScrollExtent) {
        reachedEnd = true;
      } else if (reachedTop || reachedEnd) {
        reachedTop = reachedEnd = false;
      }
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    final modelCache = Provider.of<ModelCache>(context);
    List<Exhibition> exhibitionList = modelCache.exhibitionList.values.toList();
    AppSetting appSetting = Provider.of<AppSetting>(context);
    String languageCode = appSetting.language ?? L10n.defaultLang;
    exhibitionList.sort((a, b) => b.endDate.compareTo(a.endDate));
    return Stack(children: [
      LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return ListView.builder(
              controller: scrollController,
              scrollDirection: Axis.vertical,
              itemCount: modelCache.exhibitionList.length,
              itemBuilder: (BuildContext context, int i) {
                return ImageTextOverlay(
                    onTap: () {
                      Navigator.of(context).pushNamed(
                        RouteGenerator.exhibitionScreenRouteName,
                        arguments: {
                          "exhibition": exhibitionList[i],
                        },
                      );
                    },
                    height: constraints.maxHeight / 2,
                    width: constraints.maxWidth,
                    mediaUUID: exhibitionList[i].coverImage,
                    title:
                        exhibitionList[i].getFieldValue("title", languageCode));
              });
        },
      ),
      if (!reachedTop) upDownArrow(true),
      if (!reachedEnd) upDownArrow(false)
    ]);
  }

  /// ----------------------- Assisting Widgets -------------------------

  /// Returns the icon to show that you can scroll up and down.
  /// If the given parameter is true, the up icon is chosen, otherwise the down icon.
  Widget upDownArrow(bool up) {
    return Positioned(
        bottom: up ? null : 12,
        top: up ? 12 : null,
        right: 12,
        child: IconButton(
            icon:
                Icon(up ? Kunstforum.up : Kunstforum.down, color: Colors.white),
            iconSize: 40,
            onPressed: () => scrollController.animateTo(
                up ? 0 : scrollController.position.maxScrollExtent,
                duration: Duration(milliseconds: 500),
                curve: Curves.easeInOutExpo)));
  }
}
