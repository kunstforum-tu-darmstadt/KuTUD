import 'package:flutter/material.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/connection_snackbar.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/exhibitions_screen.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/map_screen.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/search_screen.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/settings_screen.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/custom_app_bar.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/navigation_bar.dart'
    as navbar;
import 'package:kunstforum_tu_darmstadt/ui/screens/main_screen.dart';

/// the frame that shows the [CustomAppBar] and the [NavigationBar].
/// the screen it shows depends on the selected index of the [NavigationBar]
class ScreenFrame extends StatefulWidget {
  @override
  State<ScreenFrame> createState() => _ScreenFrameState();
}

class _ScreenFrameState extends State<ScreenFrame> {
  bool showArtworkInMap = false;
  int index = 0; //indicator for the screen
  bool? hasInternet;

  // onTap these attributes gain the new values in selectScreenAndCostumAppBar,
  // default values when started are main screen and KuTUD logo, since the App first opens the main screen
  String? appBarTitle;
  Widget selectedScreen = MainScreen();

  @override
  void initState() {
    super.initState();

    InternetConnectionChecker().onStatusChange.listen((event) {
      final hasInternet = event == InternetConnectionStatus.connected;
      if (this.hasInternet != null) {
        ScaffoldMessenger.of(context)
            .showSnackBar(ConnectionSnackbar.getSnackbar(hasInternet, context));
      }
      setState(() => this.hasInternet = hasInternet);
    });
  }

  // ----------------------- Main Widget  -------------------------

  @override
  Widget build(BuildContext context) {
    final appSetting = Provider.of<AppSetting>(context);
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(appSetting.appBarHeight),
        child: CustomAppBar(
          appSetting: appSetting,
          appBarTitle: appBarTitle,
          showBackButton: false,
        ),
      ),
      body: selectedScreen,
      bottomNavigationBar: navbar.NavigationBar(
        tabIndexCallback: (tabIndex) => setState(
          () {
            index = tabIndex;
            selectScreenAndCostumAppBar();
          },
        ),
      ),
    );
  }

  // ----------------------- Combination Selection  -------------------------
  // select the appropriate screen and appBar resulting from the index

  ///Select an appropriate Screen and [CostumAppBar] matching the selected [index] representing the TabBar Button
  Widget selectScreenAndCostumAppBar() {
    var localization = AppLocalizations.of(context)!;
    switch (index) {
      case 0:
        appBarTitle = null;
        return selectedScreen = MainScreen();
      case 1:
        appBarTitle = localization.campusArt;
        return selectedScreen = MapScreen();
      case 2:
        appBarTitle = localization.search;
        return selectedScreen = Search();
      case 3:
        appBarTitle = localization.exhibition;
        return selectedScreen = ExhibitionsScreen();
      case 4:
        appBarTitle = localization.settings;
        return selectedScreen = SettingsScreen();
      default:
        throw Exception(
            "Missing index in BottomNavigationBar. Got $index but valid index should be in range [0,4].");
    }
  }
}
