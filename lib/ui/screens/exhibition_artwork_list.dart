import 'package:flutter/material.dart';
import 'package:kunstforum_tu_darmstadt/models/artwork.dart';
import 'package:kunstforum_tu_darmstadt/models/exhibition.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/artwork_card.dart';

/// Builds a [ListView] with [ArtCard] as element for each [filteredList] elements.
/// It shows the given items in a List of Cards
abstract class ArtList extends StatelessWidget {
  final List<Artwork>? filteredArtworkList;
  final List<Exhibition>? filteredExhibitionList;
  const ArtList(
      {Key? key, this.filteredArtworkList, this.filteredExhibitionList})
      : assert(filteredArtworkList != null ||
            filteredExhibitionList !=
                null), // either the artwork or the exhibition list is set
        assert(filteredArtworkList == null ||
            filteredExhibitionList ==
                null), // either the artwork or the exhibition list isn't set;
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: filteredArtworkList != null
            ? filteredArtworkList!.length
            : filteredExhibitionList!.length,
        itemBuilder: (BuildContext context, int i) {
          return filteredArtworkList != null
              ? ArtworkCard(filteredArtworkList![i])
              : ExhibitionCard(filteredExhibitionList![i]);
        });
  }
}

/// Builds a [ListView] with [ArtworkCard] as element for each [filteredList] elements.
/// It shows the given [Artwork]s in a List of Cards
class ArtworkList extends ArtList {
  ArtworkList(List<Artwork> artworkList)
      : super(filteredArtworkList: artworkList);
}

/// Builds a [ListView] with [ArtworkCard] as element for each [filteredList] elements.
/// It shows the given [Exhibition]s in a List of Cards
class ExhibitionList extends ArtList {
  ExhibitionList(List<Exhibition> exhibitionList)
      : super(filteredExhibitionList: exhibitionList);
}
