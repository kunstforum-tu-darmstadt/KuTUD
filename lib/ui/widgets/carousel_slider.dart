import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:kunstforum_tu_darmstadt/api/model_cache.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';
import 'package:kunstforum_tu_darmstadt/models/media.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/image_text_overlay.dart';
import 'package:provider/provider.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

/// A enum for choosing if and how image names should be displayed.
enum ImageNameStyle {
  // the name of the current image is not displayed
  none,
  // the name of the current image is displayed as a title at the top of the image
  title,
  // the name of the current image is displayed as a subtitle under a fixed title
  subtitle,
  // the name of the current image is displayed as a subtitle under a fixed title aswell as a banner showing the currentStatus (new, updated)
  icon,
}

class CarouselSliderBox extends StatefulWidget {
  /// The [BoxFit] of the displayed images in the carousel.
  /// If not set, it defaults to [BoxFit.contain].
  final BoxFit imageFit;

  /// The size of the box
  final Size boxSize;

  /// The uuids of the medias of the images that shall be displayed
  final List<String> mediaUUIDs;

  /// The titles/subtitles of the images that shall be displayed
  /// The length of this List has to be the same length of the [imageUrls] List
  final List<String>? imageTexts;

  /// A function which will be called if a image has been tapped by a user.
  /// The parameter [index] is the number of the currently displayed image and
  /// is guaranteed to be 0 <= index < elementCount.
  final void Function(int index)? onTap;

  /// The title of the slider.
  /// if a title is provided the imageNameStyle must not be set to ImageNameStyle.title
  final String? title;

  /// The icon which displays the change status (new = was added in the last 30 days
  /// updated = was changed in the last 30 days)
  final List<String>? changedStatus;

  /// The style in which the image names provided by [provideText] should be displayed
  /// if it is set to [ImageNameStyle.none], the name of the current image is not displayed
  /// if it is set to [ImageNameStyle.title], the name of the current image is displayed as a title at the top of the image
  /// if it is set to [ImageNameStyle.subtitle], the name of the current image is displayed as a subtitle under the given fixed [title]. The title must be set in this case
  /// If not set, it defaults to [ImageNameStyle.title].
  final ImageNameStyle imageNameStyle;

  /// Determines if current page should be larger then the side images,
  /// creating a feeling of depth in the carousel.
  /// If not set, it defaults to false.
  final bool enlargeCenterPage;

  CarouselSliderBox({
    Key? key,
    required this.boxSize,
    this.imageFit = BoxFit.contain,
    this.imageTexts,
    this.onTap,
    this.title,
    this.changedStatus,
    this.imageNameStyle = ImageNameStyle.title,
    this.enlargeCenterPage = true,
    required this.mediaUUIDs,
  })  : assert(title == null ||
            imageNameStyle !=
                ImageNameStyle
                    .title), // if the image name is displayed as the title, no title must be specified
        assert(imageTexts == null ||
            imageNameStyle !=
                ImageNameStyle
                    .none), // if names for the images are given, these names should also be displayed
        assert(imageNameStyle != ImageNameStyle.subtitle ||
            title !=
                null), // if the image name is displayed as the subtitle, there has to be a fixed title for the slider
        assert(imageTexts == null || imageTexts.length == mediaUUIDs.length),
        assert(changedStatus == null ||
            imageNameStyle !=
                ImageNameStyle
                    .icon), // if the images have texts the number of texts must be equal to the number of images
        super(key: key);

  @override
  _CarouselSliderBoxState createState() => _CarouselSliderBoxState();
}

class _CarouselSliderBoxState extends State<CarouselSliderBox> {
  late final int elementCount = widget.mediaUUIDs.length;

  /// The index of the image which is currently shown.
  int position = 0;

  /// Used to control the the CarouselSlider for swiping to the next page with the [AnimatedSmoothIndicator]
  CarouselController carouselController = CarouselController();

  late ModelCache modelCache = Provider.of<ModelCache>(context);

  @override
  void didChangeDependencies() {
    // Run the precache method after the widget is built
    _precacheNeighbours(position);
    super.didChangeDependencies();
  }

  /// Loads the previous and the next image into the cache.
  /// The method is called when the widget is initialized and
  /// if the user swipes to change the image.
  void _precacheNeighbours(int index) async {
    // Precaches the next image in the carousel if there at least two images
    if (elementCount >= 2) {
      final nextIndex = (index + 1) % elementCount;
      Media media = await modelCache.getMediaCached(
          widget.mediaUUIDs[nextIndex], context);
      DefaultCacheManager().downloadFile(media.url);
    }

    // Precaches the previous image in the carousel if there at least three images
    if (elementCount >= 3) {
      final previousIndex = (index - 1) % elementCount;
      Media media = await modelCache.getMediaCached(
          widget.mediaUUIDs[previousIndex], context);
      DefaultCacheManager().downloadFile(media.url);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.boxSize.width,
      height: widget.boxSize.height,
      child: _buildCarouselSlider(),
    );
  }

  /// Builds a [CarouselSlider] widget containing the images.
  /// The images shown are built using the [_buildImage] method.
  Widget _buildCarouselSlider() {
    return Stack(
      fit: StackFit.expand,
      children: [
        CarouselSlider.builder(
          carouselController: carouselController,
          options: CarouselOptions(
            enlargeCenterPage: widget.enlargeCenterPage,
            aspectRatio: 1,
            // Only enable infinite scrolling when at least 2 artworks are to be shown
            enableInfiniteScroll: elementCount > 1,
            viewportFraction: 1,
            onPageChanged: (index, reason) {
              setState(() {
                position = index;
              });
              _precacheNeighbours(index);
            },
          ),
          itemCount: elementCount,
          itemBuilder: (context, index, realIndex) => _buildImage(index),
        ),
        if (widget.title != null)
          Positioned(
            top: 20,
            left: 20,
            child: Text(widget.title!,
                style: Theme.of(context).textTheme.headline1!.copyWith(
                      fontWeight: FontWeight.w900,
                      color: Colors.white,
                    )),
          ),
        if (elementCount >= 2)
          Positioned(
            right: 20,
            bottom: 20,
            child: _PageIndicator(
              position: position,
              elementCount: elementCount,
              onTap: (index) => carouselController.animateToPage(index),
            ),
          ),
      ],
    );
  }

  /// Builds the correct image widget for the [widget.type].
  Widget _buildImage(int index) {
    return ImageTextOverlay(
        onTap: () => widget.onTap != null ? widget.onTap!(index) : null,
        mediaUUID: widget.mediaUUIDs[index],
        width: widget.boxSize.width,
        height: widget.boxSize.height,
        title: widget.imageTexts != null &&
                widget.imageNameStyle == ImageNameStyle.title
            ? widget.imageTexts![index]
            : null,
        subtitle: widget.imageTexts != null &&
                widget.imageNameStyle == ImageNameStyle.subtitle
            ? widget.imageTexts![index]
            : null,
        changedStatus:
            widget.changedStatus != null ? widget.changedStatus![index] : null);
  }
}

/// A [StatelessWidget] showing a page indicator.
/// [position] is current current position of the indicator.
/// [elementCount] the maximum number of elements.
class _PageIndicator extends StatelessWidget {
  const _PageIndicator({
    Key? key,
    required this.position,
    required this.elementCount,
    required this.onTap,
  }) : super(key: key);

  /// The position of the pageIndicator
  final int position;

  /// The number of elements which are represented by the dots
  final int elementCount;

  /// The function that is called when you tap on a dot
  final Function(int) onTap;

  @override
  Widget build(BuildContext context) {
    final appSetting = Provider.of<AppSetting>(context);
    final double dotScale = MediaQuery.of(context).size.width * 0.025;

    return AnimatedSmoothIndicator(
      activeIndex: position,
      count: elementCount,
      onDotClicked: onTap,
      effect: ScrollingDotsEffect(
        activeDotColor: appSetting.primaryColor,
        dotColor: Colors.white,
        dotWidth: dotScale,
        dotHeight: dotScale,
        spacing: 17,
      ),
    );
  }
}
