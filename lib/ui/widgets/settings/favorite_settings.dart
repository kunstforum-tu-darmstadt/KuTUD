import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/artwork_card.dart';
import 'package:kunstforum_tu_darmstadt/api/model_cache.dart';
import 'package:shared_preferences/shared_preferences.dart';

///
/// A [StatefulWidget] that displays a [SimpleDialog] with
/// the user's favorite artworks
///
class FavoriteSettings extends StatefulWidget {
  @override
  _FavoriteSettingsState createState() => _FavoriteSettingsState();
}

///
/// A State for [FavoriteSettings] that manages the artworks marked
/// as favorite by the user in a [ListView] with [ArtworkCard]s
///
class _FavoriteSettingsState extends State<FavoriteSettings> {
  /// Class attributes

  /// A list of favorites from [modelCache.favoriteArtworkList] which is manipulated locally.
  /// The changes are only saved in [modelCache.favoriteArtworkList] when the checkmark button is pressed
  List<int> currentFavorites = [];

  @override
  Widget build(BuildContext context) {
    AppSetting appSetting = Provider.of<AppSetting>(context);
    ModelCache modelCache = Provider.of<ModelCache>(context);
    currentFavorites = List.from(modelCache.favoriteArtworkList);

    return SimpleDialog(
        contentPadding: EdgeInsets.all(3.0),
        titlePadding: EdgeInsets.all(6.0),
        title: Text(AppLocalizations.of(context)!.favorites,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline2),
        children: <Widget>[
          Container(
            color: Theme.of(context).dialogBackgroundColor,
            height: appSetting.cardSize.height * 4,
            child: Container(
              width: appSetting.cardSize.width * 4,
              child: artworkCard(modelCache),
            ),
          ),
        ]);
  }

  /// Helpful functions

  /// Builds a [ListView] with [ArtworkCard] as element for each [filteredList] elements.
  Widget artworkCard(ModelCache modelCache) {
    if (currentFavorites.isEmpty) {
      return Padding(
        padding: const EdgeInsets.all(16.0),
        child: Center(
            child: Text(
          AppLocalizations.of(context)!.noFavorites,
          textAlign: TextAlign.center,
        )),
      );
    }
    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: currentFavorites.length,
      itemBuilder: (BuildContext context, int index) {
        final item = currentFavorites[index];
        return Dismissible(
          // Each Dismissible must contain a Key. Keys allow Flutter to
          // uniquely identify widgets.
          key: Key(modelCache.artworkList[item]!.identifier),
          // Show a red background as the item is swiped away.
          background: Container(color: Colors.red),
          // Provide a function that tells the app
          // what to do after an item has been swiped away.
          onDismissed: (direction) {
            FavoritesHandler.removeFavorite(item, context);
            setState(() {});
          },
          child: ArtworkCard(
            modelCache.artworkList[item]!,
          ),
        );
      },
    );
  }
}

class FavoritesHandler {
  static final Future<SharedPreferences> _prefs =
      SharedPreferences.getInstance();

  static final String preferencesName = 'favorites';

  static void loadFavorites(BuildContext context) {
    ModelCache modelCache = Provider.of<ModelCache>(context, listen: false);
    _prefs.then((SharedPreferences prefs) {
      List<String> favoritesStrings =
          (prefs.getStringList(preferencesName) ?? []);
      modelCache.favoriteArtworkList
          .addAll(favoritesStrings.map((i) => int.parse(i)).toList());
    });
  }

  static void addFavorite(int artworkId, BuildContext context) {
    ModelCache modelCache = Provider.of<ModelCache>(context, listen: false);
    _prefs.then((SharedPreferences prefs) {
      if (modelCache.favoriteArtworkList.contains(artworkId)) return;
      modelCache.favoriteArtworkList.add(artworkId);
      prefs.setStringList(preferencesName,
          modelCache.favoriteArtworkList.map((e) => e.toString()).toList());
    });
  }

  static void removeFavorite(int artworkId, BuildContext context) {
    ModelCache modelCache = Provider.of<ModelCache>(context, listen: false);
    _prefs.then((SharedPreferences prefs) {
      if (modelCache.favoriteArtworkList.remove(artworkId)) {
        prefs.setStringList(preferencesName,
            modelCache.favoriteArtworkList.map((e) => e.toString()).toList());
      }
    });
  }
}
