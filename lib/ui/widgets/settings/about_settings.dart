import 'package:flutter/material.dart';

///
/// A [StatelessWidget] that displays a [SimpleDialog] with a text
///
class AboutSettings extends StatelessWidget {
  /// Class attributes

  // The headline of the textBox
  final String about;

  // The content of the textBox
  final String text;

  /// Constructor

  AboutSettings({required this.about, required this.text});

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: Text(about,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline2),
      children: <Widget>[
        Container(
          color: Theme.of(context).cardColor,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child:
                      Text(text, style: Theme.of(context).textTheme.bodyText1!),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
