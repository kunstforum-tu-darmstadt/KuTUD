import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';
import 'package:kunstforum_tu_darmstadt/lang/l10n.dart';
import 'package:kunstforum_tu_darmstadt/lang/locale_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

///
/// A [StatelessWidget] that displays a [SimpleDialog] with language options.
///
class LanguageSettings extends StatelessWidget {
  /// Class attributes

  /// An instance of [SharedPreferences]
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  // A list of [Locale] for each language
  final List<Locale> _langCodes = [];

  @override
  Widget build(BuildContext context) {
    final AppSetting appSetting = Provider.of<AppSetting>(context);
    final provider = Provider.of<LocaleProvider>(context);
    Locale? _pickedLocale = provider.locale;
    return SimpleDialog(
        title: Text(AppLocalizations.of(context)!.chooseLang,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline2),
        children: <Widget>[
          Container(
              width: 300,
              height: 70,
              child: CupertinoPicker(
                itemExtent: Theme.of(context).textTheme.button!.fontSize! * 1.8,
                looping: false,
                onSelectedItemChanged: (int value) {
                  _pickedLocale = _langCodes[value];
                },
                children: getLanguageWidgets(
                    AppLocalizations.of(context)!.language, context),
              )),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            IconButton(
              onPressed: () async {
                provider.setLocale(_pickedLocale!);
                changePrefs(context, _pickedLocale!);
                Navigator.of(context, rootNavigator: true).pop('dialog');
              },
              icon: Icon(CupertinoIcons.checkmark, size: appSetting.iconSize),
            ),
            IconButton(
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop('dialog');
              },
              icon: Icon(CupertinoIcons.clear, size: appSetting.iconSize),
            )
          ])
        ]);
  }

  /// Helpful functions

  // Create a widget of text items for each language,
  // the text item with the current language comes first
  List<Widget> getLanguageWidgets(String currentLang, BuildContext context) {
    List<Widget> widgets = [
      Center(
        child: Text(currentLang,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.button),
      )
    ];

    for (int i = 0; i < L10n.allLangString.length; i++) {
      if (L10n.allLangString[i] != currentLang) {
        widgets.add(Center(
          child: Text(L10n.allLangString[i],
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.button),
        ));

        // Fill the langCodes with the corresponding order
        _langCodes.add(L10n.all[i]);
      } else {
        _langCodes.insert(0, L10n.all[i]);
      }
    }
    return widgets;
  }

  /// Changes settings and saves changes
  void changePrefs(BuildContext context, Locale _pickedLocale) {
    AppSetting appSetting = Provider.of<AppSetting>(context, listen: false);
    appSetting.language = _pickedLocale.toLanguageTag();
    _prefs.then((SharedPreferences prefs) {
      prefs.setString("appSetting", jsonEncode(appSetting.toJson()));
      prefs.setBool("settingSaved", true);
    });
  }
}
