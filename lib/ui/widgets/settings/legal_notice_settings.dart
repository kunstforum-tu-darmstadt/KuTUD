import 'package:flutter/material.dart';
import 'package:kunstforum_tu_darmstadt/api/api_util.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';
import 'package:provider/provider.dart';

import '../../../api/model_cache.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

/// abstract class for Listitems used in a Listview
abstract class ListItem {
  /// Title of the ListItem
  Widget buildTitle(BuildContext context);

  /// Used for Trailing Icons
  Widget? buildIcon(BuildContext context, int index, int offset);

  /// Padding of the ListItem
  EdgeInsetsGeometry buildPadding(BuildContext context);

  void buildOnTap(BuildContext context, int index, int offset);

  Color? buildColor(BuildContext context);

  bool buildEnabled(BuildContext context, int index, int offset);
}

/// Listitem for the Imprint of the app_information
class ImprintListItem implements ListItem {
  final String text;

  ImprintListItem(this.text);

  @override
  EdgeInsetsGeometry buildPadding(BuildContext context) {
    return EdgeInsets.fromLTRB(20, 20, 20, 20);
  }

  @override
  Widget buildTitle(BuildContext context) {
    return Text(text, style: Theme.of(context).textTheme.bodyText1!);
  }

  @override
  Color? buildColor(BuildContext context) {
    return null;
  }

  @override
  bool buildEnabled(BuildContext context, int index, int offset) {
    return false;
  }

  @override
  Widget? buildIcon(BuildContext context, int index, int offset) {
    return null;
  }

  @override
  void buildOnTap(BuildContext context, int index, int offset) {}
}

/// Listitem for the copyright notices
class CopyrightListItem implements ListItem {
  final String text;

  CopyrightListItem(this.text);

  @override
  EdgeInsetsGeometry buildPadding(BuildContext context) {
    return EdgeInsets.fromLTRB(20, 0, 20, 0);
  }

  @override
  Widget buildTitle(BuildContext context) {
    return Text(text, style: Theme.of(context).textTheme.bodyText1!);
  }

  @override
  Color? buildColor(BuildContext context) {
    return null;
  }

  @override
  bool buildEnabled(BuildContext context, int index, int offset) {
    ModelCache modelCache = Provider.of<ModelCache>(context, listen: false);
    final copyrightList = modelCache.copyrightList.values.toList();
    if (copyrightList[index - offset].copyrightUrl != null &&
        copyrightList[index - offset].copyrightUrl != '') {
      return true;
    }
    return false;
  }

  @override
  Widget? buildIcon(BuildContext context, int index, int offset) {
    ModelCache modelCache = Provider.of<ModelCache>(context, listen: false);
    AppSetting appSetting = Provider.of<AppSetting>(context);
    final copyrightList = modelCache.copyrightList.values.toList();
    if (copyrightList[index - offset].copyrightUrl != null &&
        copyrightList[index - offset].copyrightUrl != '') {
      return SizedBox(
        child: ImageIcon(AssetImage("assets/images/Icon_Link.png")),
      );
    }
    return null;
  }

  @override
  void buildOnTap(BuildContext context, int index, int offset) {
    ModelCache modelCache = Provider.of<ModelCache>(context, listen: false);
    final copyrightList = modelCache.copyrightList.values.toList();
    if (copyrightList[index - offset].copyrightUrl != null &&
        copyrightList[index - offset].copyrightUrl != '') {
      ApiUtil.launchURL(copyrightList[index - offset].copyrightUrl!);
    }
  }
}

/// ListItem for Seperators
class SeparatorListItem implements ListItem {
  final String text;

  SeparatorListItem(this.text);

  @override
  EdgeInsetsGeometry buildPadding(BuildContext context) {
    return EdgeInsets.fromLTRB(20, 5, 20, 5);
  }

  @override
  Widget buildTitle(BuildContext context) {
    return Text(text,
        style: Theme.of(context)
            .textTheme
            .headline2!
            .copyWith(fontWeight: FontWeight.bold));
  }

  @override
  Color? buildColor(BuildContext context) {
    return Theme.of(context).brightness == Brightness.dark
        ? Color(0xFF818284)
        : Color(0xFFF7F7F7);
  }

  @override
  bool buildEnabled(BuildContext context, int index, int offset) {
    return false;
  }

  @override
  Widget? buildIcon(BuildContext context, int index, int offset) {
    return null;
  }

  @override
  void buildOnTap(BuildContext context, int index, int offset) {}
}

///
/// A [StatelessWidget] that displays a [SimpleDialog] with a text
///
class LegalNoticeSettings extends StatelessWidget {
  /// Class attributes
  // The headline of the textBox
  final String about;

  // The content of the textBox
  final String text;

  // This number of elements which are not retrieved from the Copyrights
  final int offset = 2;

  /// Constructor

  LegalNoticeSettings({required this.about, required this.text});

  /// Returns a List for the ListView to display
  /// First Item is the Legal Notice
  /// Second Item headline as a seperator
  /// followed by all relevant copyright used in the app
  List<ListItem> getItemList(BuildContext context) {
    ModelCache modelCache = Provider.of<ModelCache>(context);
    AppLocalizations localizations = AppLocalizations.of(context)!;
    final copyrightList = modelCache.copyrightList.values.toList();

    return List<ListItem>.generate(
      copyrightList.length + offset,
      (i) => i == 0
          ? ImprintListItem(text)
          : i == 1
              ? SeparatorListItem(localizations.photographers)
              : CopyrightListItem(copyrightList[i - offset].copyright),
    );
  }

  @override
  Widget build(BuildContext context) {
    ModelCache modelCache = Provider.of<ModelCache>(context);
    final copyrightList = modelCache.copyrightList.values.toList();

    /// creates the displayed list of items imprint first followed by copyright values
    final items = getItemList(context);
    return SimpleDialog(
      title: Text(about,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline2),
      children: <Widget>[
        Container(
            height: 500,
            width: 300,
            child: Card(
              child: ListView.builder(
                itemCount: copyrightList.length + offset,
                itemBuilder: (context, index) {
                  final item = items[index];
                  return ListTile(
                    title: item.buildTitle(context),
                    trailing: item.buildIcon(context, index, offset),
                    tileColor: item.buildColor(context),
                    enabled: item.buildEnabled(context, index, offset),
                    dense: true,
                    contentPadding: item.buildPadding(context),
                    onTap: () {
                      item.buildOnTap(context, index, offset);
                    },
                  );
                },
              ),
            )),
      ],
    );
  }
}
