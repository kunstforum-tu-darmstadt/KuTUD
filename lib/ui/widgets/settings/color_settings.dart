import 'dart:convert';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flex_color_picker/flex_color_picker.dart';
import 'package:kunstforum_tu_darmstadt/config/themes.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';

///
/// A [StatefulWidget] that displays a [SimpleDialog] with options
/// for the [ColorPicker].
///
class ColorSettings extends StatefulWidget {
  @override
  _ColorSettingsState createState() => _ColorSettingsState();
}

///
/// A State for [ColorSettings] that computes the colors for
/// displaying the [SimpleDialog] and the application theme
///
class _ColorSettingsState extends State<ColorSettings> {
  /// Class attributes

  /// An instance of [SharedPreferences]
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  /// The primary color shown in a Circle Box
  Color? primaryColor;

  // List of the available theme modes
  late List<ThemeMode> themeModes;

  // List of booleans, where the element at the selected theme mode index is true and the other elements are false
  late List<bool> _themeModeSelected;

  @override
  Widget build(BuildContext context) {
    AppSetting appSetting = Provider.of<AppSetting>(context);

    /// Initialize colors if they are null
    primaryColor = primaryColor ?? appSetting.primaryColor;

    return Consumer<ThemeNotifier>(builder: (_, notifier, __) {
      themeModes = notifier.themeModes;
      _themeModeSelected = List.generate(
          themeModes.length, (index) => notifier.themeMode.index == index);
      return LayoutBuilder(
        builder: (context, constraint) {
          return SimpleDialog(
              title: Text(AppLocalizations.of(context)!.whichColor,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline2),
              contentPadding: EdgeInsets.fromLTRB(12, 12, 12, 16),
              insetPadding: EdgeInsets.symmetric(horizontal: 10),
              children: <Widget>[
                SimpleDialogOption(
                  padding: EdgeInsets.symmetric(vertical: 8),
                  child: buildDialogButton(
                    width: constraint.maxWidth * 0.6,
                    height: constraint.smallest.shortestSide * 0.1,
                    buttonColor: primaryColor!,
                    buttonText: AppLocalizations.of(context)!.primaryColor,
                    isPrimary: true,
                  ),
                ),
                SimpleDialogOption(
                  padding: EdgeInsets.symmetric(vertical: 8),
                  child: buildDialogButton(
                    width: constraint.maxWidth * 0.6,
                    height: constraint.smallest.shortestSide * 0.1,
                    buttonColor: Theme.of(context).cardColor,
                    buttonText: AppLocalizations.of(context)!.resetColor,
                    isPrimary: false,
                    onPressed: () {
                      setState(() {
                        primaryColor = Color(0xffff1c8c);
                      });
                    },
                  ),
                ),
                Divider(thickness: 2),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 12),
                  child: Container(
                    height: 40,
                    child: Consumer<ThemeNotifier>(builder: (_, notifier, __) {
                      return ToggleButtons(
                        constraints: BoxConstraints.expand(
                            width: (constraint.maxWidth - 48) /
                                _themeModeSelected.length),
                        borderRadius: BorderRadius.all(
                          Radius.circular(10),
                        ),
                        selectedColor: primaryColor!.computeLuminance() < 0.6
                            ? Colors.white
                            : Colors.black,
                        fillColor: primaryColor,
                        children: <Widget>[
                          Text(AppLocalizations.of(context)!.system),
                          Text(AppLocalizations.of(context)!.light),
                          Text(AppLocalizations.of(context)!.dark),
                        ],
                        onPressed: (index) => selectThemeMode(index, notifier),
                        isSelected: _themeModeSelected,
                      );
                    }),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                        onPressed: () {
                          changePrefs();
                          Navigator.of(context, rootNavigator: true)
                              .pop('dialog');
                        },
                        icon: Icon(CupertinoIcons.checkmark,
                            size: appSetting.iconSize)),
                    IconButton(
                      onPressed: () {
                        Navigator.of(context, rootNavigator: true)
                            .pop('dialog');
                      },
                      icon: Icon(
                        CupertinoIcons.clear,
                        size: appSetting.iconSize,
                      ),
                    )
                  ],
                )
              ]);
        },
      );
    });
  }

  /// Helpful functions

  void selectThemeMode(index, ThemeNotifier notifier) {
    notifier.changeTheme(index);
    setState(() {
      _themeModeSelected = List.generate(
          themeModes.length, (index) => notifier.themeMode.index == index);
    });
  }

  /// Builds an [ElevatedButton] having passed [buttonColor] und [buttonText]  with respect to [width] and [height].
  Widget buildDialogButton(
      {required double width,
      required double height,
      required Color buttonColor,
      required String buttonText,
      required bool isPrimary,
      Function()? onPressed}) {
    final appSetting = Provider.of<AppSetting>(context);
    return ElevatedButton(
      onPressed: onPressed ??
          () {
            openColorPicker(context);
          },
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>(buttonColor),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          SizedBox(
            width: width,
            height: height,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: AutoSizeText(buttonText,
                  maxLines: 2,
                  style: Theme.of(context).textTheme.button!.copyWith(
                        color: buttonColor.computeLuminance() < 0.6
                            ? Colors.white
                            : Colors.black,
                      )),
            ),
          ),
          Icon(
            Icons.arrow_forward,
            size: appSetting.iconSize,
            color: buttonColor.computeLuminance() < 0.6
                ? Colors.white
                : Colors.black,
          )
        ],
      ),
    );
  }

  /// Shows the color picker based on the color option (Primary, Secondary)
  void openColorPicker(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SizedBox(
              width: double.infinity,
              child: LayoutBuilder(builder: (context, constraint) {
                return Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: SingleChildScrollView(
                      child: Card(
                        elevation: 2,
                        child: Column(children: [
                          ColorPicker(
                            // Use the primary or secondary color as start color.
                            color: primaryColor!,
                            // Update the screenPickerColor using the callback.
                            onColorChanged: (Color color) {
                              setState(() {
                                primaryColor = color;
                              });
                            },
                            width: constraint.smallest.shortestSide * 0.1,
                            height: constraint.smallest.shortestSide * 0.1,
                            borderRadius: 22,
                            heading: AutoSizeText(
                              AppLocalizations.of(context)!.primaryColor,
                              maxLines: 2,
                              style: Theme.of(context).textTheme.headline2,
                            ),
                            subheading: AutoSizeText(
                              AppLocalizations.of(context)!.chooseColorShade,
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                            actionButtons: const ColorPickerActionButtons(
                              okButton: false,
                              closeButton: false,
                              dialogActionButtons: false,
                            ),
                            pickersEnabled: <ColorPickerType, bool>{
                              ColorPickerType.both: true,
                              ColorPickerType.accent: false,
                              ColorPickerType.primary: false,
                            },
                          ),
                          IconButton(
                            icon: Icon(Icons.keyboard_backspace),
                            onPressed: () {
                              Navigator.of(context).pop(true);
                            },
                          ),
                        ]),
                      ),
                    ));
              }));
        });
  }

  /// Changes settings and saves changes
  void changePrefs() {
    AppSetting appSetting = Provider.of<AppSetting>(context, listen: false);
    appSetting.primaryColor = getColor(primaryColor!);
    _prefs.then((SharedPreferences prefs) {
      prefs.setString("appSetting", jsonEncode(appSetting.toJson()));
      prefs.setBool("settingSaved", true);
    });
  }

  /// Casts [Color] to [MaterialColor]
  MaterialColor getColor(Color color) {
    return MaterialColor(color.value, {
      50: Color.fromRGBO(color.red, color.green, color.blue, .1),
      100: Color.fromRGBO(color.red, color.green, color.blue, .2),
      200: Color.fromRGBO(color.red, color.green, color.blue, .3),
      300: Color.fromRGBO(color.red, color.green, color.blue, .4),
      400: Color.fromRGBO(color.red, color.green, color.blue, .5),
      500: Color.fromRGBO(color.red, color.green, color.blue, .6),
      600: Color.fromRGBO(color.red, color.green, color.blue, .7),
      700: Color.fromRGBO(color.red, color.green, color.blue, .8),
      800: Color.fromRGBO(color.red, color.green, color.blue, .9),
      900: Color.fromRGBO(color.red, color.green, color.blue, 1),
    });
  }
}
