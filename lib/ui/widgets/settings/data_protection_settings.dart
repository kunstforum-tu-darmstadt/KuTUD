import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/introduction_screen.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

///
/// A [StatelessWidget] that displays a [SimpleDialog] with a text aswell as a Revoke Button
///
class DataProtectionSettings extends StatelessWidget {
  /// Class attributes

  // The headline of the textBox
  final String about;

  // The content of the textBox
  final String text;

  /// Constructor

  DataProtectionSettings({required this.about, required this.text});

  @override
  Widget build(BuildContext context) {
    AppSetting appSetting = Provider.of<AppSetting>(context);
    return Dialog(
        child: Container(
      margin: EdgeInsets.symmetric(horizontal: 2.0),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 24.0),
            child: Text(about,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline2),
          ),
          Expanded(
              flex: 5,
              child: Container(
                decoration: BoxDecoration(
                  color: Theme.of(context).cardColor,
                  border: Border.all(
                    color: Theme.of(context).cardColor,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                ),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Text(text,
                            style: Theme.of(context).textTheme.bodyText1!),
                      ),
                    ],
                  ),
                ),
              )),
          Container(
            child: Padding(
              padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              child: Text(AppLocalizations.of(context)!.dataProtectionRevoke,
                  style: Theme.of(context).textTheme.bodyText1!),
            ),
          ),
          Container(
              child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                          flex: 1,
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateColor.resolveWith(
                                          (states) => appSetting.primaryColor)),
                              onPressed: () {
                                final prefs = SharedPreferences.getInstance();
                                prefs.then((_prefs) =>
                                    _prefs.setBool("agreementRequired", true));
                                Navigator.of(context).pushAndRemoveUntil(
                                    MaterialPageRoute(
                                        builder: (_) => IntroScreen()),
                                    (route) => false);
                              },
                              child: Text(
                                  AppLocalizations.of(context)!
                                      .revoke
                                      .toUpperCase(),
                                  textAlign: TextAlign.center,
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1
                                      ?.copyWith(
                                        color: appSetting!.primaryColor
                                                    .computeLuminance() <
                                                0.6
                                            ? Colors.white
                                            : Colors.black,
                                      )),
                            ),
                          )),
                      Expanded(
                          flex: 1,
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateColor.resolveWith(
                                          (states) => appSetting.primaryColor)),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text(
                                  AppLocalizations.of(context)!
                                      .back
                                      .toUpperCase(),
                                  textAlign: TextAlign.center,
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1
                                      ?.copyWith(
                                        color: appSetting!.primaryColor
                                                    .computeLuminance() <
                                                0.6
                                            ? Colors.white
                                            : Colors.black,
                                      )),
                            ),
                          )),
                    ],
                  ))),
        ],
      ),
    ));
  }
}
