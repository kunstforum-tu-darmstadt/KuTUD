import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';

///
/// A [StatelessWidget] that displays a [SimpleDialog] with text size options.
///
class SizeSettings extends StatelessWidget {
  /// Class attributes

  /// An instance of [SharedPreferences]
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  // The list of possible sizes
  final List<int> _sizes = [
    SizeSetting.small,
    SizeSetting.medium,
    SizeSetting.large
  ];

  @override
  Widget build(BuildContext context) {
    final AppSetting appSetting = Provider.of<AppSetting>(context);
    int _pickedSize = appSetting.sizeSetting;
    return SimpleDialog(
        title: Text(AppLocalizations.of(context)!.chooseSizes,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline2),
        children: <Widget>[
          Container(
              width: 300,
              height: 80,
              child: CupertinoPicker(
                itemExtent: 32.0,
                looping: false,
                magnification: 1.3,
                onSelectedItemChanged: (int value) {
                  /// Set pickedSize to the picked item
                  _pickedSize = _sizes[value];
                },
                scrollController: FixedExtentScrollController(
                    initialItem: _sizes.indexOf(_pickedSize)),
                children: getSizeWidgets(context),
              )),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            IconButton(
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop('dialog');

                /// Set font sizes for the application
                appSetting.sizeSetting = _pickedSize;

                /// Update the preferences
                _prefs.then((SharedPreferences prefs) {
                  prefs.setString(
                      "appSetting", jsonEncode(appSetting.toJson()));
                  prefs.setBool("settingSaved", true);
                });
              },
              icon: Icon(CupertinoIcons.checkmark),
            ),
            IconButton(
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop('dialog');
              },
              icon: Icon(CupertinoIcons.clear),
            )
          ])
        ]);
  }

  /// Helpful functions

  /// Create a [Widget] of text items for each size,
  List<Widget> getSizeWidgets(BuildContext context) {
    return [
      sizeWidget(AppLocalizations.of(context)!.small, 10, context),
      sizeWidget(AppLocalizations.of(context)!.medium, 14, context),
      sizeWidget(AppLocalizations.of(context)!.large, 18, context)
    ];
  }

  /// Creates a [Widget] for the size setting with the given [text] and [fontSize]
  Widget sizeWidget(String text, double fontSize, BuildContext context) {
    return Center(
      child: Text(
        text,
        style: TextStyle(
          fontSize: fontSize,
          color: Theme.of(context).textTheme.bodyText1!.color,
        ),
      ),
    );
  }
}
