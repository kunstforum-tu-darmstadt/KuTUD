import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:kunstforum_tu_darmstadt/api/model_cache.dart';
import 'package:kunstforum_tu_darmstadt/models/media.dart';
import 'package:provider/provider.dart';

/// This widget gets a uuid of a media object from which the image shall be displayed
/// It downloads the [Media] JSON file if it is not already cached and after that
/// it loads and caches the image with the [CachedNetworkImage]
class FutureImage extends StatelessWidget {
  const FutureImage(
    this.uuid, {
    Key? key,
    this.printCopyright = true,
    this.width,
    this.height,
    this.fit,
  }) : super(key: key);

  final bool? printCopyright;

  /// The uuid of the media file from which the image should be displayed
  final String uuid;

  /// If non-null, require the image to have this width.
  /// If null, the image will pick a size that best preserves its intrinsic
  /// aspect ratio.
  final double? width;

  /// If non-null, require the image to have this height.
  /// If null, the image will pick a size that best preserves its intrinsic
  /// aspect ratio.
  final double? height;

  /// How to inscribe the image into the space allocated during layout.
  /// The default varies based on the other fields. See the discussion at
  /// [paintImage].
  final BoxFit? fit;

  @override
  Widget build(BuildContext context) {
    final modelCache = Provider.of<ModelCache>(context);
    Media? media = modelCache.mediaList[uuid];
    if (media == null) {
      return SizedBox(
        height: height,
        width: width,
        child: FutureBuilder<Media>(
          future: modelCache.getMediaCached(uuid, context),
          builder: (context, snapshot) {
            if (snapshot.connectionState != ConnectionState.done) {
              return Center(child: CircularProgressIndicator());
            } else {
              return _FutureImageByMedia(
                  printCopyright: printCopyright!,
                  height: height,
                  width: width,
                  fit: fit,
                  media: snapshot.data!);
            }
          },
        ),
      );
    } else {
      return _FutureImageByMedia(
          printCopyright: printCopyright!,
          height: height,
          width: width,
          fit: fit,
          media: media);
    }
  }
}

/// This widget gets the media object of the image that shall be displayed
/// it loads and caches the image with the [CachedNetworkImage]

class _FutureImageByMedia extends StatelessWidget {
  const _FutureImageByMedia({
    Key? key,
    required this.printCopyright,
    this.height,
    this.width,
    this.fit,
    required this.media,
  }) : super(key: key);

  final bool printCopyright;
  final double? height;
  final double? width;
  final BoxFit? fit;
  final Media media;

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      CachedNetworkImage(
        progressIndicatorBuilder: (context, url, progress) =>
            Center(child: CircularProgressIndicator(value: progress.progress)),
        errorWidget: (context, url, error) => Container(
          color: Colors.black12,
          child: Icon(Icons.error, color: Colors.red),
        ),
        imageUrl: media.url,
        height: height,
        width: width,
        fit: fit,
      ),
      if (printCopyright && media.copyright != '')
        LayoutBuilder(builder: (context, constraints) {
          return Padding(
            padding: EdgeInsets.only(
                left: 20,
                bottom: 25,
                top: (height ?? constraints.maxHeight) * 2 / 5),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: RotatedBox(
                  quarterTurns: -1,
                  child: Text(
                    "© " + media.copyright,
                    style: TextStyle(shadows: <Shadow>[
                      Shadow(blurRadius: 20.0, color: Colors.black)
                    ], fontSize: 12, color: Colors.white),
                  )),
            ),
          );
        })
    ]);
  }
}
