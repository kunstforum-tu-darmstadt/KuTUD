import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kunstforum_tu_darmstadt/config/route_generator.dart';
import 'package:kunstforum_tu_darmstadt/models/exhibition.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/carousel_slider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

import '../../config/app_setting.dart';
import '../../lang/l10n.dart';

/// Builds a carousel slider for displaying the first images for each exhibition.
/// The exhibitions are provided by the [exhibitionList].
/// The page indicator and a text is shown on top of the image.
class CarouselSliderExhibition extends StatelessWidget {
  /// A list [Exhibition]s.
  /// From each exhibition the first image is used.
  /// If a user taps on a image, the page for the exhibition is opened.
  final List<Exhibition> exhibitionList;

  /// The [Size] of the box.
  final Size boxSize;

  CarouselSliderExhibition({
    Key? key,
    required this.exhibitionList,
    required this.boxSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppSetting appSetting = Provider.of<AppSetting>(context);
    String languageCode = appSetting.language ?? L10n.defaultLang;
    exhibitionList.sort((a, b) => b.endDate.compareTo(a.endDate));
    return CarouselSliderBox(
      title: AppLocalizations.of(context)!.exhibition.toUpperCase(),
      imageNameStyle: ImageNameStyle.subtitle,
      imageFit: BoxFit.cover,
      boxSize: boxSize,
      mediaUUIDs: exhibitionList.map((e) => e.coverImage).toList(),
      imageTexts: exhibitionList
          .map((e) => e.getFieldValue("title", languageCode))
          .toList(),
      onTap: (index) {
        Navigator.of(context).pushNamed(
          RouteGenerator.exhibitionScreenRouteName,
          arguments: {
            "exhibition": exhibitionList[index],
          },
        );
      },
    );
  }
}
