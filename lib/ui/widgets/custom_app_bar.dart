import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';
import 'package:kunstforum_tu_darmstadt/config/kunstforum_icons.dart';

/// A [StatelessWidget] that displays an [AppBar] with the
/// Kunstforum Logo and a search button, based on [hasSearch].
class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  // ----------------------- Class Attributes  -------------------------

  /// An instance of [AppSetting]
  final AppSetting appSetting;

  /// A [bool] indicating whether the back icons should be displayed
  final bool showBackButton;

  /// A [string] representing the title displayed in the appBar, if appBarTitle is null, the KuTUD Logo is shown
  final String? appBarTitle;

  @override
  Size get preferredSize => Size.fromHeight(appSetting.appBarHeight);

  CustomAppBar(
      {required this.appSetting, this.showBackButton = true, this.appBarTitle});
  // ----------------------- Main Widget  -------------------------

  @override
  Widget build(BuildContext context) {
    double height = appSetting.appBarHeight;
    return AppBar(
      toolbarHeight: height,
      leading: showBackButton
          ? IconButton(
              padding: EdgeInsets.all(0),
              iconSize: appSetting.iconSize * 1.5,
              icon: Icon(
                Kunstforum.back,
                color: Theme.of(context).brightness == Brightness.dark
                    ? Colors.white
                    : Colors.black,
              ),
              onPressed: () => Navigator.of(context).pop(),
            )
          : null,
      // if: title is null (no title) -> the logo is displayed, else: title is not null -> the appropriate title is displayed
      title: appBarTitle == null
          ? appBarLogoDisplay(context)
          : appBarTitleDisplay(context),
      //toolbarHeight: height,
      iconTheme: IconThemeData(color: Colors.grey),
      centerTitle: true,
      //clipBehavior: Clip.none,
    );
  }

  // ----------------------- Assisting Widget  -------------------------

  /// Display the KuTUD Logo in the Appbar with [logoWidth] and [logoHeight] extracted from class [appSetting]
  Widget appBarLogoDisplay(BuildContext context) {
    return Container(
      child: Image.asset(
        Theme.of(context).brightness == Brightness.light
            ? 'assets/images/logo_black.png'
            : 'assets/images/logo_white.png',
        // fixed format with constant results in constant header without glitching
        width: appSetting.logoWidth,
        height: appSetting.appBarHeight * 0.8,
        fit: BoxFit.scaleDown,
      ),
    );
  }

  /// Display the [appBarTitle] of the screen in the appBar
  Widget appBarTitleDisplay(BuildContext context) {
    // the amount of padding added to the appBarTitle from below
    return Text(
      appBarTitle!
          .toUpperCase(), //when selected, the argument appBarTitle is not null
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Theme.of(context).textTheme.bodyText1!.color,
        fontFamily: 'Avenir',
        fontWeight: FontWeight.bold,
        fontSize: appSetting.appBarTitleSize,
      ),
    );
  }
}
