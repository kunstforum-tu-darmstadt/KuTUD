import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:kunstforum_tu_darmstadt/config/kunstforum_icons.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';

///
/// A [StatefulWidget] that displays a [BottomNavigationBar] with five tabs
/// that are index starting with 0.
/// First tab: MainScreen, second tab: MapScreen, third tab: SearchScreen, fourth tab: ExhibitionScreen, fifth tab SettingsScreen
///
class NavigationBar extends StatefulWidget {
  /// Class attributes

  final ValueChanged<int> tabIndexCallback;

  /// Constructor

  const NavigationBar({Key? key, required this.tabIndexCallback})
      : super(key: key);

  @override
  State<NavigationBar> createState() => _NavigationBarState();
}

class _NavigationBarState extends State<NavigationBar> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    final appSetting = Provider.of<AppSetting>(context);
    return BottomNavigationBar(
      unselectedIconTheme: Theme.of(context).iconTheme,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            icon: Icon(Kunstforum.home),
            label: AppLocalizations.of(context)!.home),
        BottomNavigationBarItem(
            icon: Icon(Kunstforum.circleMap),
            label: AppLocalizations.of(context)!.map),
        BottomNavigationBarItem(
            icon: Icon(Kunstforum.circleSearch),
            label: AppLocalizations.of(context)!.search),
        BottomNavigationBarItem(
            icon: Icon(Kunstforum.circleAthene),
            label: AppLocalizations.of(context)!.exhibition),
        BottomNavigationBarItem(
            icon: Icon(Kunstforum.circleSettings),
            label: AppLocalizations.of(context)!.settings),
      ],
      currentIndex: index,
      type: BottomNavigationBarType.fixed,
      onTap: onTap,
      unselectedFontSize: 0.0,
      selectedFontSize: 0.0,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      iconSize: appSetting.iconSize * 1.4,
      selectedItemColor: appSetting.primaryColor,
      unselectedItemColor: Colors.grey,
    );
  }

  ///Changes the screen based on [index]
  void onTap(int index) {
    widget.tabIndexCallback(index);
    setState(() {
      this.index = index;
    });
  }
}
