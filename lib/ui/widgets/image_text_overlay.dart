import 'package:flutter/material.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/future_image.dart';
import 'package:provider/provider.dart';

import '../../config/app_setting.dart';
import '../../lang/l10n.dart';

/// A [StatelessWidget] displaying the image of the given url.
/// In the foreground it displays the [title] and [subtitle] if given on a black gradient.
/// Either the [title] or the [subtitle] is required.
class ImageTextOverlay extends StatelessWidget {
  const ImageTextOverlay(
      {Key? key,
      required this.mediaUUID,
      this.title,
      this.subtitle,
      this.height,
      this.width,
      this.onTap,
      this.changedStatus})
      : super(key: key);

  /// The title that should be displayed.
  final String? title;

  /// The subtitle that should be displayed. It will be displayed below the title in a smaller font size
  final String? subtitle;

  /// The height of the image
  final double? height;

  /// The width of the image
  final double? width;

  /// The uuid of the media of the image
  final String mediaUUID;

  /// The icon which displays the change status (new = was added in the last 30 days
  /// updated = was changed in the last 30 days)
  final String? changedStatus;

  /// The function that is called when you click on the image
  final GestureTapCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: LayoutBuilder(builder: (context, constraints) {
        return GestureDetector(
            onTap: onTap,
            child: Container(
              height: height,
              width: width,
              child: Stack(
                children: [
                  FutureImage(
                    mediaUUID,
                    height: height ?? constraints.maxHeight,
                    width: width ?? constraints.maxWidth,
                    fit: BoxFit.cover,
                  ),

                  /// The title and or subtitle with a gradient
                  if (title != null || subtitle != null)
                    Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            Colors.black.withOpacity(0.6),
                            Colors.black.withOpacity(0.0),
                          ],
                          stops: [0, 0.5],
                        ),
                      ),
                    ),
                  if (title != null)
                    Padding(
                      padding: EdgeInsets.fromLTRB(20, 20, 12, 12),
                      child: Text(title!,
                          maxLines: subtitle != null ? 1 : null,
                          style: Theme.of(context)
                              .textTheme
                              .headline1!
                              .copyWith(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w900)),
                    ),
                  if (subtitle != null)
                    Padding(
                      padding: EdgeInsets.fromLTRB(20, 46, 12, 12),
                      child: RichText(
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        text: TextSpan(
                            text: subtitle!,
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700))!,
                      ),
                    ),
                  if (changedStatus != null && changedStatus != "old")
                    Padding(
                        padding: EdgeInsets.fromLTRB(20, 67, 12, 12),
                        child: buildChangedIcon(context, changedStatus!)),
                ],
              ),
            ));
      }),
    );
  }

  Widget buildChangedIcon(BuildContext context, String changedStatus) {
    AppSetting appSetting = Provider.of<AppSetting>(context);
    String languageCode = appSetting.language ?? L10n.defaultLang;
    Color textColor = appSetting.primaryColor.computeLuminance() < 0.6
        ? Colors.white
        : Colors.black;
    return Container(
      margin: EdgeInsets.all(0),
      padding: EdgeInsets.all(0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: appSetting.primaryColor,
          border: Border()),
      child: changedStatus == "update"
          ? Image.asset(
              "assets/images/Icon_Update.png",
              scale: 19,
              color: textColor,
            )
          : languageCode == "en"
              ? Image.asset(
                  "assets/images/Icon_New.png",
                  scale: 30,
                  color: textColor,
                )
              : Image.asset(
                  "assets/images/Icon_Neu.png",
                  scale: 30,
                  color: textColor,
                ),
    );
  }
}
