import 'package:flutter/material.dart';
import 'package:kunstforum_tu_darmstadt/api/model_cache.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/future_image.dart';
import 'package:provider/provider.dart';
import 'package:kunstforum_tu_darmstadt/models/artwork.dart';
import 'package:kunstforum_tu_darmstadt/models/exhibition.dart';
import 'package:kunstforum_tu_darmstadt/models/artist.dart';
import 'package:kunstforum_tu_darmstadt/config/route_generator.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';

import '../../lang/l10n.dart';

///
/// A [StatelessWidget] that displays a [Card] with two container displaying
/// the [artwork]/[exhibition]'s coverImage and title.
///
abstract class _ArtCard extends StatelessWidget {
  // Class Attributes

  /// The artwork model
  final Artwork? artwork;

  /// The Exhibition of this artwork
  final Exhibition? exhibition;

  // Constructor

  _ArtCard({this.artwork, this.exhibition})
      : assert(artwork != null ||
            exhibition != null), // either the artwork or the exhibition is set
        assert(artwork == null ||
            exhibition ==
                null); // either the artwork or the exhibition isn't set

  // Main Widget

  @override
  Widget build(BuildContext context) {
    final appSetting = Provider.of<AppSetting>(context);
    String languageCode = appSetting.language ?? L10n.defaultLang;
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 2, 8, 2),
      child: Card(
        elevation: 0,
        color: Theme.of(context).cardColor,
        shape: Border(),
        child: InkWell(
          onTap: () => navigate(context),
          child: IntrinsicHeight(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  flex: 1,
                  child: Container(
                    width: appSetting.cardSize.width,
                    child: FittedBox(
                      fit: BoxFit.cover,
                      clipBehavior: Clip.hardEdge,
                      child: FutureImage(
                        artwork != null
                            ? artwork!.coverImage
                            : exhibition!.coverImage,
                        printCopyright: false,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: Container(
                    width: appSetting.cardSize.width,
                    child: Padding(
                      padding: EdgeInsets.all(15.0),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          margin: EdgeInsets.only(right: 55),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: artwork != null
                                  ? displayArtworkText(context, artwork!)
                                  : displayExhibitionText(
                                      context,
                                      exhibition!.getFieldValue(
                                          "title", languageCode))),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

// Helpful Functions

  /// Displays the [Artist] or the [title] of the [Artwork] or [Exhibition]
  List<Text> displayArtworkText(BuildContext context, Artwork artwork) {
    Artist artist =
        Provider.of<ModelCache>(context).artistList[artwork.artistId]!;
    AppSetting appSetting = Provider.of<AppSetting>(context);
    String languageCode = appSetting.language ?? L10n.defaultLang;
    final theme = Theme.of(context).textTheme.bodyText1;
    return [
      Text(artist.name,
          textAlign: TextAlign.left,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: theme!),
      Text(artwork.getFieldValue("name", languageCode),
          textAlign: TextAlign.left,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: theme.copyWith(fontWeight: FontWeight.bold)),
    ];
  }

  /// Displays the [Artist] or the [title] of the [Artwork] or [Exhibition]
  List<Text> displayExhibitionText(
      BuildContext context, String exhibitionTitle) {
    return [
      Text(
        exhibitionTitle,
        textAlign: TextAlign.left,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: Theme.of(context)
            .textTheme
            .bodyText1!
            .copyWith(fontWeight: FontWeight.bold),
      )
    ];
  }

  /// Navigates the app in AudioGuideScreen if showArtworkInfo is true
  /// else in ExhibitionsScreen
  void navigate(BuildContext context) {
    if (artwork != null) {
      Navigator.of(context).pushNamed(RouteGenerator.audioGuideScreenRouteName,
          arguments: {"artwork": artwork});
    } else {
      Navigator.of(context)
          .pushNamed(RouteGenerator.exhibitionScreenRouteName, arguments: {
        "exhibition": exhibition,
      });
    }
  }
}

///
/// A [StatelessWidget] that displays a [Card] with two container displaying
/// the [artwork]'s coverImage and title.
///
class ArtworkCard extends _ArtCard {
  ArtworkCard(Artwork artwork) : super(artwork: artwork);
}

///
/// A [StatelessWidget] that displays a [Card] with two container displaying
/// the [exhibition]'s coverImage and title.
///
class ExhibitionCard extends _ArtCard {
  ExhibitionCard(Exhibition exhibition) : super(exhibition: exhibition);
}
