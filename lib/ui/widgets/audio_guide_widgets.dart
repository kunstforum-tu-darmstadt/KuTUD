import 'dart:math';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:provider/provider.dart';
import 'package:kunstforum_tu_darmstadt/config/kunstforum_icons.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';

final Duration jumpLength = Duration(seconds: 10);

///
/// A [StatelessWidget] that displays a the control button of audio player.
///
class ControlButtons extends StatelessWidget {
  /// Class attributes

  /// An instance of [AudioPlayer].
  final AudioPlayer player;

  final bool displayPausePlay;

  /// Constructor

  ControlButtons({required this.player, required this.displayPausePlay});

  @override
  Widget build(BuildContext context) {
    final appSetting = Provider.of<AppSetting>(context);
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Expanded(child: SizedBox.shrink()),
        Expanded(
          flex: 1,
          child: StreamBuilder<SequenceState?>(
            stream: player.sequenceStateStream,
            builder: (context, snapshot) => IconButton(
              iconSize: appSetting.iconSize,
              icon: Icon(Kunstforum.backward),
              onPressed: () {
                player.position > jumpLength
                    ? player.seek(player.position - jumpLength)
                    : player.seek(Duration.zero);
              },
            ),
          ),
        ),
        displayPausePlay
            ? Expanded(
                flex: 1,
                child: StreamBuilder<PlayerState>(
                  stream: player.playerStateStream,
                  builder: (context, snapshot) {
                    final playerState = snapshot.data;
                    final processingState = playerState?.processingState;
                    final playing = playerState?.playing;
                    if (processingState == ProcessingState.loading ||
                        processingState == ProcessingState.buffering) {
                      return Center(
                        child: Container(
                          margin: EdgeInsets.all(8.0),
                          width: appSetting.iconSize,
                          height: appSetting.iconSize,
                          child: CircularProgressIndicator(),
                        ),
                      );
                    } else if (playing != true) {
                      return IconButton(
                        icon: Icon(Kunstforum.play),
                        color: appSetting.primaryColor,
                        iconSize: appSetting.iconSize,
                        onPressed: player.play,
                      );
                    } else if (processingState != ProcessingState.completed) {
                      return IconButton(
                        icon: Icon(Kunstforum.pause),
                        color: appSetting.primaryColor,
                        iconSize: appSetting.iconSize,
                        onPressed: player.pause,
                      );
                    } else {
                      return IconButton(
                        icon: Icon(Kunstforum.restart),
                        iconSize: appSetting.iconSize,
                        color: appSetting.primaryColor,
                        onPressed: () => player.seek(Duration.zero,
                            index: player.effectiveIndices!.first),
                      );
                    }
                  },
                ),
              )
            : Spacer(),
        Expanded(
          flex: 1,
          child: StreamBuilder<SequenceState?>(
            stream: player.sequenceStateStream,
            builder: (context, snapshot) => IconButton(
              icon: Icon(Kunstforum.forward),
              onPressed: () {
                player.position > player.duration! - jumpLength
                    ? player.seek(player.duration)
                    : player.seek(player.position + jumpLength);
              },
              iconSize: appSetting.iconSize,
            ),
          ),
        ),
        Expanded(child: SizedBox.shrink())
      ],
    );
  }
}

///
/// A [StatefulWidget] that displays a seek bar.
///
class SeekBar extends StatefulWidget {
  /// Class attributes

  /// The [Duration] of completed SeekBar
  final Duration duration;

  /// The [Duration] of current SeekBar's position
  final Duration position;

  /// The [Duration] of current SeekBar's buffered position
  final Duration bufferedPosition;

  /// Result of the function onChanged
  final ValueChanged<Duration>? onChanged;

  /// Result of the function onChangeEnd
  final ValueChanged<Duration>? onChangeEnd;

  /// Constructor

  SeekBar({
    required this.duration,
    required this.position,
    required this.bufferedPosition,
    this.onChanged,
    this.onChangeEnd,
  });

  @override
  _SeekBarState createState() => _SeekBarState();
}

///
/// A State for [SeekBar] changing the displayed [SeekBar]
/// based on [_dragValue].
///
class _SeekBarState extends State<SeekBar> {
  /// Class attributes
  /// A value for drag size
  double? _dragValue;

  /// An instance of SliderThemeData
  late SliderThemeData _sliderThemeData;

  /// The remaining [Duration]
  Duration get _remaining => widget.duration - widget.position;

  @override
  Widget build(BuildContext context) {
    final appSetting = Provider.of<AppSetting>(context);
    return Stack(
      children: [
        SliderTheme(
          // This Slider is to show what part of the video is buffered
          data: _sliderThemeData.copyWith(
            thumbShape: HiddenThumbComponentShape(),
            activeTrackColor: Colors.red.shade100,
            inactiveTrackColor: Colors.grey.shade300,
          ),
          child: ExcludeSemantics(
            child: Slider(
              min: 0.0,
              max: widget.duration.inMilliseconds.toDouble(),
              value: widget.bufferedPosition.inMilliseconds.toDouble(),
              activeColor: appSetting.primaryColor,
              inactiveColor: appSetting.primaryColor.withOpacity(0.5),
              onChanged: (value) {
                setState(() {
                  _dragValue = value;
                });
                if (widget.onChanged != null) {
                  widget.onChanged!(Duration(milliseconds: value.round()));
                }
              },
              onChangeEnd: (value) {
                if (widget.onChangeEnd != null) {
                  widget.onChangeEnd!(Duration(milliseconds: value.round()));
                }
                _dragValue = null;
              },
            ),
          ),
        ),
        SliderTheme(
          // This Slider is to change the current position of the audio
          data: _sliderThemeData.copyWith(
            inactiveTrackColor: Colors.transparent,
          ),
          child: Slider(
            min: 0.0,
            max: widget.duration.inMilliseconds.toDouble(),
            activeColor: appSetting.primaryColor,
            inactiveColor: appSetting.primaryColor.withOpacity(0.5),
            value: min(_dragValue ?? widget.position.inMilliseconds.toDouble(),
                widget.duration.inMilliseconds.toDouble()),
            onChanged: (value) {
              setState(() {
                _dragValue = value;
              });
              if (widget.onChanged != null) {
                widget.onChanged!(Duration(milliseconds: value.round()));
              }
            },
            onChangeEnd: (value) {
              if (widget.onChangeEnd != null) {
                widget.onChangeEnd!(Duration(milliseconds: value.round()));
              }
              _dragValue = null;
            },
          ),
        ),
        Positioned(
          right: 16.0,
          bottom: -4.0,
          child: Text('-' + durationToString(_remaining),
              style: Theme.of(context).textTheme.bodyText1!),
        ),
        Positioned(
          left: 16.0,
          bottom: -4.0,
          child: Text(durationToString(widget.position),
              style: Theme.of(context).textTheme.bodyText1!),
        )
      ],
    );
  }

  /// Helpful functions

  String durationToString(Duration duration) {
    int frontend = 2;
    if (duration.inHours != 0) frontend = 0;
    return duration.toString().substring(frontend, 7);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _sliderThemeData = SliderTheme.of(context).copyWith(
      trackHeight: 2.0,
    );
  }
}

/// A custom [SliderComponentShape] for displaying a hidden thumb component
class HiddenThumbComponentShape extends SliderComponentShape {
  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) => Size.zero;

  @override
  void paint(
    PaintingContext context,
    Offset center, {
    required Animation<double> activationAnimation,
    required Animation<double> enableAnimation,
    required bool isDiscrete,
    required TextPainter labelPainter,
    required RenderBox parentBox,
    required SliderThemeData sliderTheme,
    required TextDirection textDirection,
    required double value,
    required double textScaleFactor,
    required Size sizeWithOverflow,
  }) {}
}

/// A class to handle SeekBar position information.
class PositionData {
  /// Class attributes

  /// The current seek bar position
  final Duration position;

  /// The current buffered seek bar position
  final Duration bufferedPosition;

  /// Constructor

  PositionData(this.position, this.bufferedPosition);
}
