import 'package:flutter/material.dart';
import 'package:kunstforum_tu_darmstadt/api/model_cache.dart';
import 'package:kunstforum_tu_darmstadt/config/route_generator.dart';
import 'package:kunstforum_tu_darmstadt/models/artwork.dart';
import 'package:kunstforum_tu_darmstadt/models/exhibition.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/carousel_slider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

import '../../config/app_setting.dart';
import '../../lang/l10n.dart';

/// Builds a carousel slider for displaying images provided by [campusExhibition].
/// The page indicator and a text is shown on top of the image.
class CarouselSliderCampus extends StatelessWidget {
  /// All images from the [Exhibition] are shown in the carousel.
  /// If a user taps on an image, the respective image is opened in the exhibition.

  /// The [Size] of the box.
  final Size boxSize;

  CarouselSliderCampus({
    Key? key,
    required this.boxSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final modelCache = Provider.of<ModelCache>(context);
    final Exhibition campusExhibition = modelCache.campusExhibition;
    AppSetting appSetting = Provider.of<AppSetting>(context);
    String languageCode = appSetting.language ?? L10n.defaultLang;

    List<Artwork> artworks = campusExhibition.artworkList
        .map((e) => modelCache.artworkList[e]!)
        .toList();
    artworks.sort((a, b) => b.updatedAt.compareTo(a.updatedAt));

    return CarouselSliderBox(
      title: AppLocalizations.of(context)!.campusArt.toUpperCase(),
      imageFit: BoxFit.cover,
      boxSize: boxSize,
      mediaUUIDs: artworks.map((e) => e.coverImage).toList(),
      imageNameStyle: ImageNameStyle.subtitle,
      imageTexts:
          artworks.map((e) => e.getFieldValue("name", languageCode)).toList(),
      changedStatus: artworks.map((e) => e.createdStatus!).toList(),
      onTap: (index) {
        Navigator.of(context).pushNamed(
          RouteGenerator.audioGuideScreenRouteName,
          arguments: {"artwork": artworks[index]},
        );
      },
    );
  }
}
