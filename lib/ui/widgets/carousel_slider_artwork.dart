import 'package:flutter/material.dart';
import 'package:kunstforum_tu_darmstadt/ui/widgets/carousel_slider.dart';

/// Builds a carousel slider for displaying the [imageLinks].
/// The page indicator is shown centered below the image.
class CarouselSliderArtwork extends StatelessWidget {
  /// A list of image URLs.
  final List<String> mediaUUIDs;

  /// The [Size] of the box.
  final Size boxSize;

  CarouselSliderArtwork({
    Key? key,
    required this.mediaUUIDs,
    required this.boxSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CarouselSliderBox(
      boxSize: boxSize,
      mediaUUIDs: mediaUUIDs,
      imageNameStyle: ImageNameStyle.none,
      imageFit: BoxFit.cover,
    );
  }
}
