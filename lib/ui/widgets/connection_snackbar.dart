import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ConnectionSnackbar {
  static SnackBar getSnackbar(bool connected, BuildContext context) {
    if (connected) {
      return SnackBar(
          content: Text(AppLocalizations.of(context)!.gotInternet),
          backgroundColor: Colors.green);
    } else {
      return SnackBar(
          content: Text(AppLocalizations.of(context)!.noInternet),
          backgroundColor: Colors.red);
    }
  }
}
