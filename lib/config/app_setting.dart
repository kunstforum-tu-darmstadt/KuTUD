import 'package:flutter/material.dart';

/// A [ChangeNotifier] providing globally access to [MediaQueryData] and
/// important device specific information.
class AppSetting extends ChangeNotifier {
  /// ----------------------- Attributes -------------------------

  /// The [MediaQueryData?] containing all important device info
  MediaQueryData? _mediaQuery;

  /// Widget Colors

  /// The preferred app's primary [MaterialColor]
  MaterialColor _primaryColor = MaterialColor(0xffff1c8c, {
    50: Color.fromRGBO(255, 28, 140, .1),
    100: Color.fromRGBO(255, 28, 140, .2),
    200: Color.fromRGBO(255, 28, 140, .3),
    300: Color.fromRGBO(255, 28, 140, .4),
    400: Color.fromRGBO(255, 28, 140, .5),
    500: Color.fromRGBO(255, 28, 140, .6),
    600: Color.fromRGBO(255, 28, 140, .7),
    700: Color.fromRGBO(255, 28, 140, .8),
    800: Color.fromRGBO(255, 28, 140, .9),
    900: Color.fromRGBO(255, 28, 140, 1),
  });

  /// Widget Sizes

  /// The preferred height of the custom app bar
  double _appBarHeight = 40;

  /// The preferred height of the custom app bar
  Size _cardSize = Size(40, 40);

  /// The preferred height of the IntroScreen dots
  final Size _dotSize = Size(10.0, 10.0);

  /// The preferred size of icons
  double _iconSize = 15;

  /// The preferred size of icons in map
  double mapMarkerIconSize = 30;

  /// The preferred width of the app logo in the app bar
  double _logoWidth = 200;

  /// The preferred height of the app logo in the app bar
  double _logoHeight = 40;

  /// The preferred font size for the title in the app bar
  double _appBarTitleSize = 20;

  /// The preferred font size setting
  int _sizeSetting = SizeSetting.medium;

  /// The preferred language
  String? _language;

  /// ----------------------- Getters for Widget Attributes -------------------------

  /// The [MediaQueryData?] containing all important device info.
  MediaQueryData? get mediaQuery => _mediaQuery;

  /// Widget Colors

  /// The preferred app's primary [MaterialColor]
  MaterialColor get primaryColor => _primaryColor;

  /// Widget Sizes

  /// The preferred height of the custom app bar
  double get appBarHeight => _appBarHeight;

  /// The preferred height of the custom app bar
  Size get cardSize => _cardSize;

  /// The preferred height of the IntroScreen dots
  Size get dotSize => _dotSize;

  /// The preferred height of the custom app bar
  double get iconSize => _iconSize;

  /// The preferred width of the app logo in the app bar
  double get logoWidth => _logoWidth;

  /// The preferred height of the app logo in the app bar
  double get logoHeight => _logoHeight;

  /// Font Sizes

  /// The preferred font size for the title in the app bar
  double get appBarTitleSize => _appBarTitleSize;

  /// The preferred font size setting
  int get sizeSetting => _sizeSetting;

  /// Language

  /// The preferred language
  String? get language => _language;

  /// ----------------------- Setters for Widget Attributes -------------------------

  /// Widget Colors

  /// Sets [color] to [_primaryColor] and notify listeners to render new changes.
  set primaryColor(MaterialColor color) {
    _primaryColor = color;
    notifyListeners();
  }

  /// Widget Sizes

  /// Sets [size] to [_cardSize] and notify listeners to render new changes.
  set cardSize(Size size) {
    if (size.height < _mediaQuery!.size.height * 0.4 &&
        size.width < _mediaQuery!.size.width) {
      _cardSize = size;
      notifyListeners();
    }
  }

  /// Sets [size] to [iconSize] and notify listeners to render new changes.
  set iconSize(double size) {
    if (size < 100) {
      _iconSize = size;
      notifyListeners();
    }
  }

  /// Font Sizes

  /// Sets [_sizeSetting] to the given value and notifies listeners to render new changes
  set sizeSetting(int sizeSetting) {
    _sizeSetting = sizeSetting;
    notifyListeners();
  }

  /// Language

  /// Sets [_language] to the given value and notifies listeners to render new changes
  set language(String? language) {
    _language = language;
    notifyListeners();
  }

  /// ----------------------- Initialize Data -------------------------

  /// Initialize [DeviceInfo] based on [context]
  void initAppSetting(BuildContext context) {
    _mediaQuery = MediaQuery.of(context);
    // set the height and width dynamically in dependence of screen size
    // it will take 5% of the smartphones screen height
    _logoHeight = _mediaQuery!.size.height / 20;
    // the logo has a fixed format of 1246x273 pixels, resulting in a factor of 22/100:
    // to get the optimal width to set for the logo in the app bar it is scaled by this factor in dependence of the resulting height from before
    _logoWidth = _logoHeight * (100 / 22);
    // the appBar in total takes 6,25% of the screen
    _appBarHeight = _mediaQuery!.size.height / 16;
    // the title in total takes a 3rd of the appBar
    _appBarTitleSize = _mediaQuery!.size.height / (16 * (2.5));
    _iconSize = 25;
    _cardSize = Size(80, 80);
    mapMarkerIconSize = 40;
  }

  /// Deserializes AppString from JSON and initialize AppString
  void fromJson(Map<String, dynamic> json) {
    int _primaryColorValue = json['_primaryColorValue'];
    _primaryColor = MaterialColor(_primaryColorValue, {
      50: Color.fromRGBO(json['_primaryColorRed'], json['_primaryColorGreen'],
          json['_primaryColorBlue'], .1),
      100: Color.fromRGBO(json['_primaryColorRed'], json['_primaryColorGreen'],
          json['_primaryColorBlue'], .2),
      200: Color.fromRGBO(json['_primaryColorRed'], json['_primaryColorGreen'],
          json['_primaryColorBlue'], .3),
      300: Color.fromRGBO(json['_primaryColorRed'], json['_primaryColorGreen'],
          json['_primaryColorBlue'], .4),
      400: Color.fromRGBO(json['_primaryColorRed'], json['_primaryColorGreen'],
          json['_primaryColorBlue'], .5),
      500: Color.fromRGBO(json['_primaryColorRed'], json['_primaryColorGreen'],
          json['_primaryColorBlue'], .6),
      600: Color.fromRGBO(json['_primaryColorRed'], json['_primaryColorGreen'],
          json['_primaryColorBlue'], .7),
      700: Color.fromRGBO(json['_primaryColorRed'], json['_primaryColorGreen'],
          json['_primaryColorBlue'], .8),
      800: Color.fromRGBO(json['_primaryColorRed'], json['_primaryColorGreen'],
          json['_primaryColorBlue'], .9),
      900: Color.fromRGBO(json['_primaryColorRed'], json['_primaryColorGreen'],
          json['_primaryColorBlue'], 1),
    });

    _appBarHeight = json['_appBarHeight'];
    _cardSize = Size(json['_cardSizeWidth'], json['_cardSizeHeight']);
    _iconSize = json['_iconSize'];
    mapMarkerIconSize = json['mapMarkerIconSize'];
    _mediaQuery = json['_mediaQuery'];
    _sizeSetting = json['_sizeSetting'];
    _language = json['_language'];
    notifyListeners();
  }

  /// Serializes AppString in to JSON
  Map<String, dynamic> toJson() => {
        "_primaryColorValue": _primaryColor.value,
        "_primaryColorRed": _primaryColor.red,
        "_primaryColorGreen": _primaryColor.green,
        "_primaryColorBlue": _primaryColor.blue,
        "_appBarHeight": _appBarHeight,
        "_cardSizeHeight": _cardSize.height,
        "_cardSizeWidth": _cardSize.width,
        "_iconSize": _iconSize,
        "mapMarkerIconSize": mapMarkerIconSize,
        "_sizeSetting": _sizeSetting,
        "_language": _language
      };
}

/// To safe the size setting in a JSON file, the three possible values
/// are mapped to integers.
class SizeSetting {
  static const int small = 0;
  static const int medium = 1;
  static const int large = 2;
}
