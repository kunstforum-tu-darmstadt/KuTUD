import 'package:flutter/material.dart';
import 'package:kunstforum_tu_darmstadt/config/app_setting.dart';
import 'package:shared_preferences/shared_preferences.dart';

TextTheme _textTheme(int size) {
  final double fontSize;
  switch (size) {
    case SizeSetting.small:
      fontSize = 10;
      break;
    case SizeSetting.large:
      fontSize = 18;
      break;
    default:
      fontSize = 14;
  }
  return TextTheme(
          headline1: TextStyle(fontSize: fontSize + 8),
          headline2: TextStyle(fontSize: fontSize + 4),
          bodyText1: TextStyle(fontSize: fontSize),
          bodyText2:
              TextStyle(fontSize: fontSize + 2), // Body texts the intro screen
          headline3: TextStyle(
              fontSize: fontSize + 6,
              fontWeight: FontWeight.w700), // Headlines the intro screen
          button: TextStyle(fontSize: fontSize + 4))
      .apply(fontFamily: 'Avenir');
}

ThemeData lightTheme(int size) {
  final ThemeData base = ThemeData.light();
  final primaryColor = Color(0xFFFF1C8C);

  return base.copyWith(
      appBarTheme: AppBarTheme(color: Colors.white),
      bottomNavigationBarTheme:
          BottomNavigationBarThemeData(backgroundColor: Colors.white),
      cardColor: Color(0xFFF1F2F2),
      inputDecorationTheme: InputDecorationTheme(fillColor: Color(0xFFF1F2F2)),
      textTheme: _textTheme(size).apply(
        bodyColor: Colors.black,
        displayColor: Colors.black,
      ),
      tabBarTheme: TabBarTheme(
        unselectedLabelColor: Colors.black,
        indicator: BoxDecoration(color: primaryColor),
      ),
      snackBarTheme: SnackBarThemeData(
          backgroundColor: Colors.white,
          contentTextStyle: TextStyle(color: Colors.black)),
      iconTheme: IconThemeData(color: Color(0xFF818284)),
      scaffoldBackgroundColor: Color(0xFFA7A9AB),
      dialogBackgroundColor: Colors.white);
}

ThemeData darkTheme(int size) {
  final ThemeData base = ThemeData.dark();
  final primaryColor = Color(0xFFFF1C8C);

  return base.copyWith(
      appBarTheme: AppBarTheme(color: Colors.black),
      bottomNavigationBarTheme:
          BottomNavigationBarThemeData(backgroundColor: Colors.black),
      cardColor: Color(0xFF6D6E71),
      inputDecorationTheme: InputDecorationTheme(fillColor: Color(0xFF6D6E71)),
      tabBarTheme: TabBarTheme(
        unselectedLabelColor: Colors.white,
        indicator: BoxDecoration(color: primaryColor),
      ),
      textTheme: _textTheme(size).apply(
        bodyColor: Colors.white,
        displayColor: Colors.white,
      ),
      snackBarTheme: SnackBarThemeData(
          backgroundColor: Color(0xFF6D6E71),
          contentTextStyle: TextStyle(color: Colors.white)),
      iconTheme: IconThemeData(color: Color(0xFFF7F7F7)),
      scaffoldBackgroundColor: Color(0xFF221F20),
      dialogBackgroundColor: Colors.grey[800]);
}

/// This class is required to change the theme dynamically in the app settings
/// It gets requests if the user wants to change the theme mode,
/// changes the new setting in the SharePreferences and notifies the app to change the theme mode
class ThemeNotifier extends ChangeNotifier {
  /// The name of the SharePreferences file where the current theme mode setting should be saved
  final String key = "theme";

  /// An instance of [SharedPreferences] to access the saved settings
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  /// List of the available theme modes
  List<ThemeMode> themeModes = [
    ThemeMode.system,
    ThemeMode.light,
    ThemeMode.dark
  ];

  /// The currently selected [ThemeMode]. Initially ThemeMode.system is selected
  ThemeMode themeMode = ThemeMode.system;

  /// When a new instance is created, the saved preferences are loaded
  ThemeNotifier() {
    _loadFromPreferences();
  }

  /// Saves the currently selected theme mode in the SharedPreferences
  _savePreferences() async {
    _prefs.then((SharedPreferences prefs) {
      prefs.setInt(key, themeModes.indexOf(themeMode));
    });
  }

  /// Saves the in the SharedPreferences saved theme mode in field themeMode
  _loadFromPreferences() async {
    _prefs.then((SharedPreferences prefs) {
      themeMode = themeModes[prefs.getInt(key) ?? 0];
      prefs.setInt(key, themeModes.indexOf(themeMode));
    });
    notifyListeners();
  }

  /// Changes the the to the given theme and saves it in the SharedPreferences
  changeTheme(int newThemeMode) {
    themeMode = themeModes[newThemeMode];
    _savePreferences();
    notifyListeners();
  }
}
