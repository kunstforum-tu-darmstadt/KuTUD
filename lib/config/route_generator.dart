import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/detail_screen.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/display_refresh_screen.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/exhibition_screen.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/introduction_screen.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/loading_screen.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/search_screen.dart';
import 'package:kunstforum_tu_darmstadt/ui/screens/screen_frame.dart';

/// A class which used to handle route hierarchy.
class RouteGenerator {
  /// ----------------------- Attributes -------------------------

  /// A [String] representing route Name of audio guide screen
  static const String audioGuideScreenRouteName = '/audio';

  /// A [String] representing route Name of exhibition screen
  static const String exhibitionScreenRouteName = '/exhibition';

  /// A [String] representing route Name of loading screen
  static const String loadingScreenRouteName = '/loading';

  /// A [String] representing route Name of loading screen
  static const String introScreenRouteName = '/intro';

  /// A [String] representing route Name of frame screen
  static const String frameScreenRouteName = '/home';

  /// A [String] representing route Name of map screen
  static const String mapScreenRouteName = '/map';

  /// A [String] representing route Name of search screen
  static const String searchRouteName = '/search';

  /// A [String] representing route Name of search screen
  static const String displayRefreshScreen = '/refresh';

  /// ----------------------- Methods -------------------------

  /// Returns a [MaterialPageRoute] for the give [RouteSettings].
  /// It calls depending to [settings.name] the constructor of the specific screen.
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case loadingScreenRouteName:
        return MaterialPageRoute(builder: (context) => LoadingScreen());
      case audioGuideScreenRouteName:
        // Getting arguments passed in while calling Navigator.pushNamed
        final Map<String, dynamic> args =
            settings.arguments as Map<String, dynamic>;
        return MaterialPageRoute(
            builder: (context) => AudioGuideScreen(artwork: args['artwork']));
      case frameScreenRouteName:
        return MaterialPageRoute(builder: (context) => ScreenFrame());
      case exhibitionScreenRouteName:
        // Getting arguments passed in while calling Navigator.pushNamed
        final Map<String, dynamic> args =
            settings.arguments as Map<String, dynamic>;
        return MaterialPageRoute(
            builder: (context) => ExhibitionScreen(
                  exhibition: args['exhibition'],
                ));
      case introScreenRouteName:
        return MaterialPageRoute(builder: (context) => IntroScreen());
      case displayRefreshScreen:
        return MaterialPageRoute(
            builder: (context) => DisplayRefreshScreen(onReload: () async {
                  var connectivityResult =
                      await (Connectivity().checkConnectivity());
                  if (connectivityResult != ConnectivityResult.none) {
                    DisplayRefreshScreen.opened = false;
                    Navigator.of(context).pop();
                  }
                }));
      case searchRouteName:
        final Map<String, dynamic> args =
            settings.arguments as Map<String, dynamic>;
        return MaterialPageRoute(
            builder: (context) => Search(
                  searchArtworks: args['searchArtworks'],
                ));
      default:
        // If there is no such named route in the switch statement
        return _errorRoute();
    }
  }

  /// Returns a [MaterialPageRoute] to an error screen.
  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
