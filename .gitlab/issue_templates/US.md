## Titel

[UI]/[UX]/[UI/UX] Titel

## Beschreibung

Als Nutzer*in …


## Akzeptanzkriterium

- kriterium 1
- kriterium 2

## Bemerkungen


/weight SP
/label ~open
/assign @dev
/assign_reviewer @tester
/iteration *iteration:"iteration name"
/estimate 1mo 2w 3d 4h 5m
